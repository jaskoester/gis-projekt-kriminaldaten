import UrlUtils from "./UrlUtils";

export default class GeoServerLayerTemplate {

    
    public static getTemplate() {
        return fetch(UrlUtils.getCurrentURLWithContext('/assets/geoservertemplate.xml'));
    }

}