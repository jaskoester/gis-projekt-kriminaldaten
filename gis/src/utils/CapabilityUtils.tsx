import WMSCapabilities from "ol/format/WMSCapabilities";

export default class CapabilityUtils {
  static CAPABILITY_REQUEST: string = "http://%host:%port/geoserver/%workspace/wms?service=WMS&request=GetCapabilities";

  /**
   * Gibt die Layerinformationen aller verfuegbaren Layers zurueck
   * @param hostname 
   * @param port 
   * @param workspace 
   */
  public static async getLayerCapabilities(hostname, port, workspace) {
    const url = this.CAPABILITY_REQUEST.replace("%host", hostname)
      .replace("%port", port)
      .replace("%workspace", workspace);
    const parser = new WMSCapabilities();

    const layers = await fetch(url)
      .then(function(response) {
        return response.text();
      })
      .then(function(text) {
        const capabilities = parser.read(text).Capability;
        const layers = capabilities.Layer.Layer;

        return layers;
      });

    return layers;
  }

  /**
   * Gibt alle verfuegbaren Jahre von Kriminalitaetsdaten zurueck 
   * @param layers 
   */
  public static getAvailableYears(layers) {

    const availableYears = [];

    layers.forEach(element => {
        if (element.Name.includes('vg_bka_')) {
            availableYears.push((element.Title as string).split(' ')[1]);
        }
    });

    return availableYears; 
  }
}
