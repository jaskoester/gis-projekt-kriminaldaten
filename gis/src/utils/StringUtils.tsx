export default class StringUtils{

    public static replaceAll(string, search, replace) {
        let newString = string;
        while (newString.includes(search)) {
          newString = newString.replace(search, replace);
        }
        return newString;
      }

}