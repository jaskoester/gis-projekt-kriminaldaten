import GeoJSON from "ol/format/GeoJSON";
import { Feature } from "ol";
import * as ss from "simple-statistics";

export interface Cluster{
  min: number,
  max: number,
  color: any    
}

export default class FeatureUtils {

  /**
   * Ruft den Geoserver auf und gibt die Features zurueck
   * @param wfsServiceNamespace 
   * @param layer 
   * @param propertyKey 
   * @param propertyValue 
   */
  public static getFeaturesByProperty(wfsServiceNamespace, layer, propertyKey, propertyValue) : Promise<Feature[]> {
    const url =
      "http://localhost:8080/geoserver/Kriminalstatistiken/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=" + wfsServiceNamespace 
        + "%3A" + layer + "&maxFeatures=99999&outputFormat=application%2Fjson&srsname=EPSG:3857&,EPSG:3857&CQL_FILTER=" + propertyKey + "='" + propertyValue + "'";

      return fetch(url)
      .then(function(response) {
        return response.json();
      })
      .then(function(text) {
        const result = new GeoJSON().readFeatures(text);
        return result;
      });
  }

  /**
   * Gibt eine Liste von allen Werten in der Featurelist für einen bestimmten Key zurueck
   * @param featureList 
   * @param propertyKey 
   * @param normalize 
   */
  public static getAllValuesForFeatureListProperty(featureList : Feature[], propertyKey, normalize: boolean): number[]{

    let valueList = new Array<number>();

    featureList.forEach((feature) => {
      if(feature.get(propertyKey) === null || feature.get(propertyKey) === undefined || isNaN(Number.parseFloat(feature.get(propertyKey)))){
        console.log("undefined = "+ propertyKey)
        console.log(feature)
      }else{
      let propertyValue = Number.parseFloat((feature.get(propertyKey) as string).toString().replace(',',''));

      if(normalize){
        const bevoelkerung = Number.parseFloat(feature.get("BevoelkerungInsgesamt")) /1000;
        if(bevoelkerung){
          propertyValue = propertyValue / bevoelkerung;
        }else{
          console.log("Error in getAllValues");
        }
      }
      if (propertyValue !== null && propertyValue !== undefined && !isNaN(propertyValue)) {
        valueList.push(propertyValue);
      }}
    });

    return valueList;
  }

  /**
   * Erstelt 10 Cluster aus der Values in der Liste und ordnet einen Farbverlauf zu
   * @param colorGood 
   * @param colorBad 
   * @param valueList 
   */
  public static getCluster(colorGood, colorBad, valueList: number[]){
    const distanceR = colorGood.r - colorBad.r;
    const distanceG = colorGood.g - colorBad.g;
    const distanceB = colorGood.b - colorBad.b;

    const clusterCount = 10;
    const breaks = ss.ckmeans(valueList, clusterCount);

    let cluster: Array<Cluster> = [];
    breaks.forEach((breakValues, index) => {
      breakValues.sort((a, b) => a - b);
      let faktor = 1 / (clusterCount -1);
      faktor = faktor * index;
      const colorCluster = "rgba(" + (colorGood.r - distanceR * faktor) + "," + (colorGood.g - distanceG * faktor) + "," + (colorGood.b - distanceB * faktor) + ", 0.8)"
      cluster = [...cluster, {min:breakValues[0], max: breakValues[breakValues.length-1], color: colorCluster}];

    });

    cluster.forEach((clusterelement, index) => {
      if(index === 0){
        clusterelement.min = 0;
      }else{
        const middle = (cluster[index-1].max + clusterelement.min)/2;
        cluster[index-1].max = Number.parseFloat(middle.toFixed(3));
        clusterelement.min = Number.parseFloat(middle.toFixed(3)) + 0.001;
      }   
      if(index === cluster.length-1){
        clusterelement.max = Number.parseFloat(clusterelement.max.toFixed(3)) + 0.001;
      }   
    });
    return cluster;
  }

  /**
   * Gibt die Farbe fuer einen Wert zurueck (abhaengig vom Cluster)
   * @param cluster 
   * @param value 
   */
  public static getRelativeColorgradedStyle(cluster, value: number){
    let color;
    value = Number.parseFloat(value.toFixed(3));
    cluster.forEach(element => {
      
      if(element.min === value || (element.min < value && element.max >= value)){
        color = element.color;
      }
    });
    return color;
  }
}

