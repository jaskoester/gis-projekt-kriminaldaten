export default class UrlUtils {


    public static getCurrentURL() {
        return window.location.origin;
    }

    public static getCurrentURLWithContext(context) {
        return window.location.origin + context;
    }

    public static createURLForCurrentHost(port: Number, context: String) {
        return window.location.protocol + "//" + window.location.hostname + ":" + port.toString() + context;
    }

}