import * as React from "react";
import { TabMenu } from "primereact/tabmenu";
import { PropStructure } from "./PropStructure";

export default class Nav extends React.Component<PropStructure> {
  navlist = {
    items: [
      {
        label: "Kartenansicht",
        command: event => {
          window.location.hash = "/map";
        }
      },
      {
        label: "Import",
        command: event => {
          window.location.hash = "/import";
        }
      },
      {
        label: "Datenquellen",
        command: event => {
          window.location.hash = "/source";
        }
      }
    ]
  };

  constructor(props) {
    super(props);
    this.props.onChange(
      Object.assign({}, this.props.data.nav, {
        navlist: this.navlist,
        activeItem: this.navlist.items.filter(
          item => item.label === "Kartenansicht"
        )[0]
      })
    );
  }

  componentDidUpdate() {
    this.props.data.nav.activeItem.command(null);
  }

  private switchView(viewName) {
    this.props.onChange(
      Object.assign({}, this.props.data.nav, { activeItem: viewName })
    );
  }
  public render() {
    return (
      <div className="p-grid" > 
        <div className="p-col-12">
        <TabMenu
          model={this.props.data.nav.navlist.items}
          activeItem={this.props.data.nav.activeItem}
          onTabChange={e => {
            this.switchView(e.value);
          }}
        />
        </div>
      </div>
    );
  }
}
