import * as React from "react";
import Map from "ol/Map";
import OSM from "ol/source/OSM";
import TileWMS from "ol/source/TileWMS";
import View from "ol/View";
import TileLayer from "ol/layer/Tile";
import Projection from "ol/proj/Projection";
import GeoJSON from "ol/format/GeoJSON";
import { Dropdown } from "primereact/dropdown";
import { Slider } from "primereact/slider";
import { RadioButton } from "primereact/radiobutton";
import Style from "ol/style/Style";
import Fill from "ol/style/Fill";
import Stroke from "ol/style/Stroke";
import VectorSource from "ol/source/Vector";
import VectorLayer from "ol/layer/Vector";
import FeatureUtils from "../utils/FeatureUtils";
import LayerGroup from "ol/layer/Group";
import { ColorPicker } from "primereact/colorpicker";
import { Button } from "primereact/button";
import Popup from "ol-popup/src/ol-popup";
import Select from "ol/interaction/Select";
import { StyleLike } from "ol/style/Style";
import { Checkbox } from "primereact/checkbox";
import { TabMenu } from "primereact/tabmenu";
import { SelectButton } from "primereact/selectbutton";
import { transform } from "ol/proj";
import * as controls from "ol/control";
import "./OpenLayerView.css";
import CapabilityUtils from "../utils/CapabilityUtils";

export default class OpenLayerView extends React.Component<{},
  {
    evaluations: Array<any>;
    activatedEvaluation: any;
    activeItem: boolean;
    secondEvaluationActivated: boolean;
    artKrimiOptions: Array<any>;
    faktorKrimiOptions: Array<any>;
    sonstigeOptions: Array<any>;
  }
> {
  state = {
    activatedEvaluation: null,
    activeItem: true,
    secondEvaluationActivated: false,
    artKrimiOptions: [],
    faktorKrimiOptions: [],
    sonstigeOptions: [],
    evaluations: [
      {
        art: "Straftaten insgesamt",
        year: 2018,
        faktor: "erfasste Faelle",
        higherbetter: false,
        colorBad: { r: 255, g: 0, b: 0 },
        colorGood: { r: 43, g: 214, b: 40 },
        normalize: true,
        datentyp: null,
        map: null,
        vectorTiles: null,
        hasData: true,
        infotext: ""
      },
      {
        art: "Straftaten insgesamt",
        year: 2018,
        faktor: "erfasste Faelle",
        higherbetter: false,
        colorBad: { r: 255, g: 0, b: 0 },
        colorGood: { r: 43, g: 214, b: 40 },
        normalize: false,
        datentyp: null,
        map: null,
        vectorTiles: null,
        hasData: true,
        infotext: ""
      }
    ]
  };

  constructor(props) {
    super(props);
    this.items = [{ label: "1. Auswertung" }, { label: "2.Auswertung" }];
    this.datentypen = [{ label: "Kriminalitätsdaten" }, { label: "Sonstige Daten" }];
    this.state.activeItem = true;
    this.state.activatedEvaluation = this.state.evaluations[0];
    this.state.activatedEvaluation.datentyp = this.datentypen[0];
    this.state.evaluations[1].datentyp = this.datentypen[0];
  }

  baseLayer: TileLayer;
  osm: TileLayer;
  items: Array<any>;
  datentypen: Array<any>;
  view: View;

  /**
   * Funktion setzt die Liste fuer das Dropdown von Straftaten aus einer JSON-Response der Features
   */
  private setArtOptions = response => {
    let keylist = [];
    JSON.parse(response).features.forEach(element => {
      keylist = [...keylist, element.properties.Straftat];
    });
    let options = this.getOptions(keylist);
    this.setState({ artKrimiOptions: options });
  };

  /**
   * Erstellt einen neuen VectorLayer anhand der uebergebenen Parameter wird dieser Layer beim geoserver angefordert
   * 
   * @param geoWorkspace 
   * @param geoDataSet 
   * @param propertyKey 
   * @param propertyValue 
   */
  private createNewVectorLayer(geoWorkspace: string, geoDataSet: string, propertyKey: string, propertyValue: string): VectorLayer {
    const vectorLayer = new VectorLayer({
      visible: true,
      source: new VectorSource({
        url:
          "http://localhost:8080/geoserver/Kriminalstatistiken/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=" +
          geoWorkspace +
          "%3A" +
          geoDataSet +
          "&maxFeatures=99999&outputFormat=application%2Fjson&srsname=EPSG:3857&,EPSG:3857&CQL_FILTER=" +
          propertyKey +
          "='" +
          propertyValue +
          "'",
        format: new GeoJSON({
          featureProjection: "EPSG:3857"
        })

        // Countries have transparency, so do not fade tiles:
      })
    });

    return vectorLayer;
  }

  /**
   * Setzt einen neuen Feature VectorLayer in der LayerGroup der Map
   * @param map 
   * @param geoWorkspace 
   * @param geoDataSet 
   * @param propertyKey 
   * @param propertyValue 
   */
  private setFeatureVectorLayer(map: Map, geoWorkspace: string, geoDataSet: string, propertyKey: string, propertyValue: string) {
    let newVectorLayer;

    newVectorLayer = this.createNewVectorLayer(geoWorkspace, geoDataSet, propertyKey, propertyValue);

    map.setLayerGroup(
      new LayerGroup({
        layers: [this.osm, this.baseLayer, newVectorLayer]
      })
    );
    return newVectorLayer;
  }

  /**
   * Setzt die Stylefunction vom Vector, um die Klassifizierten Werten in der richtigen Farbe anzuzeigen
   */
  private setValueBasedColorGrading = (
    vector: VectorLayer,
    geoWorkspace: string,
    geoDataSet: string,
    propertyKey: string,
    propertyValue: string,
    isFirstMap
  ) => {
    this.getStyle(vector, geoWorkspace, geoDataSet, propertyKey, propertyValue, false, isFirstMap).then(style => {
      vector.setStyle(style);
    });
  };

  /**
   * Gibt die sortierten Optionen im Format einer Dropdownliste für eine Liste von Keys zurueck
   * @param keys 
   */
  private getOptions(keys) {
    let options = [];

    keys.forEach(element => {
      if (options.filter(opt => opt.value === element).length === 0) {
        options = [
          ...options,
          {
            label: element,
            value: element
          }
        ];
      }
    });
    options = options.sort(function(a, b) {
      if (a.label < b.label) {
        return -1;
      }
      if (a.label > b.label) {
        return 1;
      }
      return 0;
    });
    return options;
  }

  /**
   * Setzt den Inhalt aller Dropdownlisten
   */
  private setDropdownlists() {
    var xhr = new XMLHttpRequest();
    xhr.addEventListener("load", () => {
      this.setArtOptions(xhr.responseText);
    });
    xhr.open(
      "GET",
      "http://localhost:8080/geoserver/Kriminalstatistiken/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=Kriminalstatistiken%3Avg_bka_" +
        this.state.activatedEvaluation.year +
        "&maxFeatures=99999&outputFormat=application%2Fjson&srsname=EPSG:3857&,EPSG:3857&PROPERTYNAME=Straftat"
    );
    xhr.send();
    const urlWertungsOptions =
      "http://localhost:8080/geoserver/Kriminalstatistiken/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=Kriminalstatistiken%3Avg_bka_" +
      this.state.activatedEvaluation.year +
      "&maxFeatures=1&outputFormat=application%2Fjson&srsname=EPSG:3857&,EPSG:3857";
    const notincludedProperties = [
      "id",
      "Schluessel",
      "Straftat",
      "Gemeindeschluessel",
      "Stadt-/Landkreis",
      "Kreisart",
      "id_pol",
      "ade",
      "rs",
      "rs_0",
      "gen",
      "geometry",
      "BevoelkerungInsgesamt",
      "maennlich",
      "weiblich",
      "Arbeitslosenquote bez. abh. zivile Erwerbspers.",
      "Arbeitslosenquote bez. auf alle zivile Erwerbsp.",
      "Arbeitslosenquote - Maenner",
      "Arbeitslosenquote - Frauen",
      "Arbeitslosenquote - 15 bis unter 25 Jahre",
      "Bevoelkerungsdichte EW/qkm"
    ];
    const includedSonstige = [
      "BevoelkerungInsgesamt",
      "Arbeitslosenquote bez. abh. zivile Erwerbspers.",
      "Arbeitslosenquote bez. auf alle zivile Erwerbsp.",
      "Arbeitslosenquote - Maenner",
      "Arbeitslosenquote - Frauen",
      "Arbeitslosenquote - 15 bis unter 25 Jahre",
      "Bevoelkerungsdichte EW/qkm"
    ];
    fetch(urlWertungsOptions)
      .then(response => {
        return response.json();
      })
      .then(json => {
        const result = new GeoJSON().readFeatures(json);
        const properties = result[0].getProperties();
        let propertiesKeys = Object.keys(properties);
        var matchingKeys = propertiesKeys.filter(function(key) {
          return !notincludedProperties.includes(key);
        });
        var matchingKeysSonstige = propertiesKeys.filter(function(key) {
          return includedSonstige.includes(key);
        });
        this.setState({ faktorKrimiOptions: this.getOptions(matchingKeys), sonstigeOptions: this.getOptions(matchingKeysSonstige) });
      });
  }

  /**
   * Erstellt eine Map
   * @param evaluation die Einstellungen fuer die darzustellenden Daten
   * @param ref Referenz in der die Karte angezeigt werden soll
   * @param isFirstMap gibt an, ob es die erste Karte/Auswertung ist
   */
  private createMap(evaluation, ref, isFirstMap): { map: Map; vectorTiles: VectorLayer } {
    let vectorTiles = new VectorLayer({
      visible: true,
      source: new VectorSource({
        url:
          "http://localhost:8080/geoserver/Kriminalstatistiken/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=Kriminalstatistiken%3Avg_bka_" +
          this.state.activatedEvaluation.year +
          "&maxFeatures=99999&outputFormat=application%2Fjson&srsname=EPSG:3857&,EPSG:3857&CQL_FILTER=Straftat='Straftaten insgesamt'",
        format: new GeoJSON({
          featureProjection: "EPSG:3857"
        })

        // Countries have transparency, so do not fade tiles:
      })
    });
    if (!this.view) {
      this.view = new View({
        projection: "EPSG:3857", //HERE IS THE VIEW PROJECTION
        center: [0, 0],
        zoom: 2
      });
    }

    let map = new Map({
      target: ref as HTMLElement,

      view: this.view,
      layers: [this.osm, this.baseLayer, vectorTiles],
      controls: controls.defaults().extend([new controls.ScaleLine(), new controls.ZoomSlider()])
    });

    const bounds = [5.8659988134023715, 47.270362326546476, 15.037743331944311, 55.05737470056596];
    const source = new Projection({ code: "EPSG:4326" });
    const dest = new Projection({ code: "EPSG:3857" });
    const newBounds1 = transform([bounds[0], bounds[1]], source, dest);
    const newBounds2 = transform([bounds[2], bounds[3]], source, dest);
    const newBounds = newBounds1.concat(newBounds2);

    this.setValueBasedColorGrading(vectorTiles, "Kriminalstatistiken", "vg_bka_" + evaluation.year, "Straftat", evaluation.art, isFirstMap);

    map.getView().fit(newBounds, { size: map.getSize() });
    return { map: map, vectorTiles: vectorTiles };
  }

  /**
   * Setzt zusaetzliche Einstellungen für die Karten:
   * - setzt Style  & Text bei Klick auf einen Landkreis
   * @param evaluation 
   * @param isFirstMap 
   */
  private setAdditonalSetupMaps(evaluation, isFirstMap) {
    this.getStyle(evaluation.vectorTiles, "Kriminalstatistiken", "vg_bka_" + evaluation.year, "Straftat", evaluation.art, true, isFirstMap).then(
      style => {
        const opt = { style: style };
        (evaluation.map as any).Select = new Select(opt);
        evaluation.map.addInteraction((evaluation.map as any).Select);
      }
    );

    let stateLocale = () => {
      return this.state;
    };

    evaluation.map.on("singleclick", function(evt) {
      let vectorTiles = (evt.map as Map)
        .getLayers()
        .getArray()
        .filter(layer => layer.getType() === "VECTOR")[0];
      let featVectors = (vectorTiles as VectorLayer).getSource().getFeaturesAtCoordinate(evt.coordinate);
      let vectorText = "";
      let morermationsPopuptext = "";

      featVectors.forEach(feat => {
        if (!vectorText.includes(feat.getProperties()["Stadt-/Landkreis"])) {
          vectorText = vectorText + feat.getProperties()["Stadt-/Landkreis"];
          if (
            stateLocale().evaluations[(evt.map as any).number].faktor !== null &&
            stateLocale().evaluations[(evt.map as any).number].faktor !== undefined  &&
            feat.getProperties()[stateLocale().evaluations[(evt.map as any).number].faktor] !== null && 
            feat.getProperties()[stateLocale().evaluations[(evt.map as any).number].faktor] !== undefined
          ) {
            morermationsPopuptext =
              "<b>" +
              stateLocale().evaluations[(evt.map as any).number].faktor +
              ":</b> </br>" +
              feat.getProperties()[stateLocale().evaluations[(evt.map as any).number].faktor];
            morermationsPopuptext += "</br><b>Bevölkerung:</b> </br>" + feat.getProperties()["BevoelkerungInsgesamt"];
          }
        }
      });
      if (!!featVectors && featVectors.length > 0) {
        ((evt.map as Map).getOverlays().getArray()[0] as Popup).show(
          evt.coordinate,
          '<p style="text-align: left"> <b>Kreis:</b> </br>' + vectorText + "</br></br>" + morermationsPopuptext + "</p>"
        );
      } else {
        ((evt.map as Map).getOverlays().getArray()[0] as Popup).hide();
      }
    });
  }

  componentDidMount() {
    var format = "image/png";

    this.baseLayer = new TileLayer({
      visible: true,
      source: new TileWMS({
        projection: "EPSG:3857",
        url: "http://localhost:8080/geoserver/Kriminalstatistiken/wms",
        params: {
          FORMAT: format,
          VERSION: "1.1.1",
          tiled: true,
          LAYERS: "Kriminalstatistiken:vg2500_krs",
          exceptions: "application/vnd.ogc.se_inimage",
          tilesOrigin: 5.82013988494873 + "," + 47.2314224243164
        }
      })
    });

    this.osm = new TileLayer({
      source: new OSM()
    });

    let evaluations = this.state.evaluations;

    let mapResponse1 = this.createMap(evaluations[0], this.refs.mapContainer, true);
    evaluations[0].map = mapResponse1.map;
    evaluations[0].vectorTiles = mapResponse1.vectorTiles;
    evaluations[0].map.addOverlay(new Popup());
    (evaluations[0].map as any).number = 0;

    let mapResponse2 = this.createMap(evaluations[1], this.refs.mapContainer2, false);
    evaluations[1].map = mapResponse2.map;
    evaluations[1].vectorTiles = mapResponse2.vectorTiles;
    evaluations[1].map.addOverlay(new Popup());
    (evaluations[1].map as any).number = 1;

    this.setState({ evaluations: evaluations });

    this.setDropdownlists();

    this.setAdditonalSetupMaps(this.state.evaluations[0], true);
    this.setAdditonalSetupMaps(this.state.evaluations[1], false);
  }
  /**
   * Schraffiert ein Feature
   * @param featValue 
   * @param cluster 
   */
  private makePattern(featValue, cluster) {
    var cnv = document.createElement("canvas");
    var ctx = cnv.getContext("2d");
    cnv.width = 6;
    cnv.height = 6;
    ctx.fillStyle = FeatureUtils.getRelativeColorgradedStyle(cluster, featValue);
    ctx.fillRect(0, 0, cnv.width, cnv.height);
    ctx.fillStyle = "rgb(255, 255, 255)";

    for (var i = 0; i < 6; ++i) {
      ctx.fillRect(i, i, 1, 1);
    }

    return ctx.createPattern(cnv, "repeat");
  }

  /**
   * Erstellt eine Stylefunction aus VectorTiles, wenn vorhanden. Ansonsten werden die Features neu geladen.
   * @param vectorTiles 
   * @param geoWorkspace 
   * @param geoDataSet 
   * @param propertyKey 
   * @param propertyValue 
   * @param isSelected 
   * @param isFirstMaps 
   */
  private async getStyle(vectorTiles, geoWorkspace, geoDataSet, propertyKey, propertyValue, isSelected, isFirstMaps) {
    const resp = vectorTiles.getSource().getFeatures();
    if (resp && resp.length < 10) {
      let response;
      if (this.state.activatedEvaluation.datentyp === this.datentypen[1]) {
        response = await FeatureUtils.getFeaturesByProperty(geoWorkspace, geoDataSet, "Straftat", "Straftaten insgesamt");
      } else {
        response = await FeatureUtils.getFeaturesByProperty(geoWorkspace, geoDataSet, propertyKey, propertyValue);
      }
      return this.getStyleByFeature(response, isSelected, isFirstMaps);
    } else {
      return this.getStyleByFeature(resp, isSelected, isFirstMaps);
    }
  }

  /**
   * Erstellt einen Style von einer Featurelist (resp)
   * @param resp 
   * @param isSelected 
   * @param isFirstMaps 
   */
  private getStyleByFeature(resp, isSelected, isFirstMaps): StyleLike {
    let faktor;
    if (this.state.activatedEvaluation.datentyp === this.datentypen[0]) {
      faktor = this.state.activatedEvaluation.faktor;
    } else {
      faktor = this.state.activatedEvaluation.art;
      this.state.activatedEvaluation.faktor = faktor;
    }
    const allValues = FeatureUtils.getAllValuesForFeatureListProperty(resp, faktor, this.state.activatedEvaluation.normalize);
    const colorGood = this.state.activatedEvaluation.higherbetter
      ? this.state.activatedEvaluation.colorBad
      : this.state.activatedEvaluation.colorGood;
    const colorBad = this.state.activatedEvaluation.higherbetter ? this.state.activatedEvaluation.colorGood : this.state.activatedEvaluation.colorBad;

    const cluster = FeatureUtils.getCluster(colorGood, colorBad, allValues);
    let legendhtml = "<p style='font-size: 14px'> Legende <p>";
    cluster.forEach(cluster => {
      legendhtml +=
        this.getColoredRectangle(cluster.color) +
        " " +
        cluster.min.toLocaleString(undefined, { maximumFractionDigits: 3 }) +
        " - " +
        cluster.max.toLocaleString(undefined, { maximumFractionDigits: 3 }) +
        "</br>";
    });
    if (isFirstMaps) {
      if (document.getElementById("info1")) {
        document.getElementById("info1").innerHTML = legendhtml;
      }
      this.state.evaluations[0].infotext = legendhtml;
    } else {
      this.state.evaluations[1].infotext = legendhtml;
      if (this.state.evaluations[1].hasData && document.getElementById("info2")) {
        document.getElementById("info2").innerHTML = legendhtml;
      }
    }
    let normalize = this.state.activatedEvaluation.normalize;
    return feature => {
      let featValue = Number.parseFloat((feature.get(faktor) as string).toString().replace(",", ""));
      if (normalize) {
        const bevoelkerung = Number.parseFloat(feature.get("BevoelkerungInsgesamt")) / 1000;
        featValue = featValue / bevoelkerung;
      }
      let st = new Style({
        fill: new Fill({
          color: isSelected ? this.makePattern(featValue, cluster) : FeatureUtils.getRelativeColorgradedStyle(cluster, featValue)
        })
      });
      if (isSelected) {
        st.setStroke(
          new Stroke({
            color: "rgba(255,255,255,1)",
            width: 2
          })
        );
      }
      return st;
    };
  }
  /**
   * Gibt den HTML-Code für ein Rechteck in der übergebenen Farbe zurueck. 
   * (wird für die Legende verwendet)
   * @param color 
   */
  private getColoredRectangle(color) {
    return "<div style='width:15px; height:15px; background:" + color + "; border-radius:3px; display: inline-block; '></div>";
  }

  /**
   * Funktion, die aufgerufen wird, wenn neue Werte analysiert und in den Karten dargestellt werden sollen.
   * Erneuert die Stylefunctions
   */
  private handleClick = evt => {
    (this.state.activatedEvaluation.map.getOverlays().getArray()[0] as Popup).hide();
    let vectorTiles = this.setFeatureVectorLayer(
      this.state.activatedEvaluation.map,
      "Kriminalstatistiken",
      "vg_bka_" + this.state.activatedEvaluation.year,
      "Straftat",
      this.state.activatedEvaluation.datentyp === this.datentypen[0] ? this.state.activatedEvaluation.art : "Straftaten insgesamt"
    );
    this.getStyle(
      vectorTiles,
      "Kriminalstatistiken",
      "vg_bka_" + this.state.activatedEvaluation.year,
      "Straftat",
      this.state.activatedEvaluation.art,
      true,
      this.state.activeItem
    ).then(style => {
      const opt = { style: style };
      this.state.activatedEvaluation.map.removeInteraction((this.state.activatedEvaluation.map as any).Select);
      (this.state.activatedEvaluation.map as any).Select = new Select(opt);
      this.state.activatedEvaluation.map.addInteraction((this.state.activatedEvaluation.map as any).Select);
    });
    this.setDropdownlists();
    this.setValueBasedColorGrading(
      vectorTiles,
      "Kriminalstatistiken",
      "vg_bka_" + this.state.activatedEvaluation.year,
      "Straftat",
      this.state.activatedEvaluation.art,
      this.state.activeItem
    );
  };

  /**
   * Setzt den Change bei der activierten Auswertung im State
   * @param name 
   * @param value 
   */
  private handleChangeEvaluations(name, value) {
    let active = this.state.activatedEvaluation;
    active[name] = value;
    this.setState({ activatedEvaluation: active }, () => {});
  }

  public render() {
    const divStyle = {
      width: "100%",
      marginTop: "20px"
    };

    return (
      <div>
        <div className="p-grid">
          <div className={this.state.secondEvaluationActivated ? "p-col-4" : "p-col-8"}>
            {!this.state.evaluations[0].hasData ? (
              <div
                style={{
                  padding: "5px",
                  width: "inherit",
                  height: "calc(100vh * 0.8)",
                  zIndex: 1010,
                  background: "rgba(200,200,200,0.9)",
                  textAlign: "center",
                  fontSize: "smaller",
                  position: "absolute"
                }}
              >
                <h4>
                  Achtung: <br /> Für dieses Jahr sind keine Kriminalitätsdaten verfügbar
                </h4>
              </div>
            ) : null}
            <div ref="mapContainer" style={{ height: "calc(100vh * 0.8)" }}>
              <div
                id="info1"
                style={{
                  padding: "5px",
                  position: "absolute",
                  zIndex: 1000,
                  background: "rgba(200,200,200,0.9)",
                  textAlign: "left",
                  fontSize: "smaller"
                }}
              >
                <p>Legende</p>
              </div>
            </div>
          </div>

          {this.state.secondEvaluationActivated ? (
            <div className="p-col-4">
              {!this.state.evaluations[1].hasData ? (
                <div
                  style={{
                    padding: "5px",
                    width: "inherit",
                    height: "calc(100vh * 0.8)",
                    zIndex: 1010,
                    background: "rgba(200,200,200,0.9)",
                    textAlign: "center",
                    fontSize: "smaller",
                    position: "absolute"
                  }}
                >
                  <h4>
                    Achtung: <br /> Für dieses Jahr sind keine Kriminalitätsdaten verfügbar
                  </h4>
                </div>
              ) : null}
              <div ref="mapContainer2" style={{ height: "calc(100vh * 0.8)" }}>
                <div
                  id="info2"
                  style={{
                    padding: "5px",
                    position: "absolute",
                    zIndex: 1000,
                    background: "rgba(200,200,200,0.9)",
                    textAlign: "left",
                    fontSize: "smaller"
                  }}
                >
                  <p>Legende</p>
                </div>

                <div id="wrapper">
                  <div id="location"></div>
                  <div id="scale2"></div>
                </div>
              </div>
            </div>
          ) : null}

          <div className="p-col-4">
            <TabMenu
              model={this.items}
              activeItem={this.state.activeItem ? this.items[0] : this.items[1]}
              onTabChange={e =>
                this.setState({
                  activeItem: e.value === this.items[0],
                  activatedEvaluation: this.state.evaluations[e.value === this.items[0] ? 0 : 1]
                })
              }
            />

            <SelectButton
              style={{ marginTop: "20px" }}
              optionLabel="label"
              value={this.state.activatedEvaluation.datentyp}
              options={this.datentypen}
              onChange={e => {
                this.handleChangeEvaluations("datentyp", e.value);
                if (e.value === this.datentypen[1]) {
                  this.handleChangeEvaluations("normalize", false);
                }
              }}
            />
            <hr></hr>

            <h3>Jahr: {this.state.activatedEvaluation.year}</h3>
            <Slider
              value={this.state.activatedEvaluation.year}
              onChange={e => {
                this.handleChangeEvaluations("year", e.value);
              }}
              onSlideEnd={async e => {
                const availableYears = CapabilityUtils.getAvailableYears(
                  await CapabilityUtils.getLayerCapabilities("localhost", 8080, "Kriminalstatistiken")
                );
                if (availableYears.includes(this.state.activatedEvaluation.year.toString())) {
                  this.state.activatedEvaluation.hasData = true;
                  this.setState({ activatedEvaluation: this.state.activatedEvaluation });
                  this.handleClick(e);
                } else {
                  this.state.activatedEvaluation.hasData = false;
                  this.setState({ activatedEvaluation: this.state.activatedEvaluation });
                }
              }}
              range={false}
              min={2013}
              max={new Date().getFullYear()}
              style={{ marginLeft: "20px", marginRight: "20px" }}
            />
            <Dropdown
              style={divStyle}
              value={this.state ? this.state.activatedEvaluation.art : "LDN"}
              options={this.state.activatedEvaluation.datentyp === this.datentypen[0] ? this.state.artKrimiOptions : this.state.sonstigeOptions}
              onChange={e => {
                this.handleChangeEvaluations("art", e.value);
              }}
              placeholder="Wähle eine Datensatz"
            />
            {this.state.activatedEvaluation.datentyp === this.datentypen[0] ? (
              <Dropdown
                style={divStyle}
                value={this.state.activatedEvaluation.faktor}
                options={this.state.faktorKrimiOptions}
                onChange={e => {
                  this.handleChangeEvaluations("faktor", e.value);
                }}
                placeholder="Wähle einen Wertungsfaktor"
              />
            ) : null}
            <div className="p-col-12" style={{ textAlign: "left" }}>
              <RadioButton
                inputId="rb2"
                value={true}
                name="city"
                onChange={e => this.handleChangeEvaluations("higherbetter", e.value)}
                checked={this.state.activatedEvaluation.higherbetter}
              />
              <label htmlFor="rb2" className="p-radiobutton-label">
                Höhere Werte sind besser
              </label>
            </div>

            <div className="p-col-12" style={{ textAlign: "left" }}>
              <RadioButton
                inputId="rb1"
                value={false}
                name="city"
                onChange={e => this.handleChangeEvaluations("higherbetter", e.value)}
                checked={!this.state.activatedEvaluation.higherbetter}
              />
              <label htmlFor="rb1" className="p-radiobutton-label">
                Niedrigere Werte sind besser
              </label>
            </div>
            <div className="p-col-12" style={{ textAlign: "left" }}>
              <h4 style={{ marginBlockEnd: "0px" }}>Farbe von:</h4>
            </div>
            <div className="p-col-12" style={{ textAlign: "left" }}>
              <ColorPicker
                format="rgb"
                inputId="cb1"
                value={this.state.activatedEvaluation.colorBad}
                onChange={e => this.handleChangeEvaluations("colorBad", e.value)}
              ></ColorPicker>
              <label htmlFor="cb1" className="p-radiobutton-label">
                schlechteren Werten
              </label>
            </div>
            <div className="p-col-12" style={{ textAlign: "left" }}>
              <ColorPicker
                format="rgb"
                inputId="cb2"
                value={this.state.activatedEvaluation.colorGood}
                onChange={e => this.handleChangeEvaluations("colorGood", e.value)}
              ></ColorPicker>
              <label htmlFor="cb2" className="p-radiobutton-label">
                besseren Werten
              </label>
            </div>
            {this.state.activatedEvaluation.datentyp === this.datentypen[0] ? (
              <div className="p-col-12" style={{ textAlign: "left" }}>
                <Checkbox
                  inputId="normalizeCB"
                  onChange={e => this.handleChangeEvaluations("normalize", e.checked)}
                  checked={this.state.activatedEvaluation.normalize}
                ></Checkbox>
                <label htmlFor="normalizeCB" className="p-checkbox-label">
                  Werte pro Tsd. Einwohner berechnen
                </label>
              </div>
            ) : null}
            {!this.state.activeItem ? (
              this.state.secondEvaluationActivated ? (
                <>
                  <div className="p-col-6" style={{ textAlign: "left" }}>
                    <Button label="Analyse" disabled={!this.state.activatedEvaluation.hasData} onClick={this.handleClick} />
                  </div>
                  <div className="p-col-6" style={{ textAlign: "left" }}>
                    <Button
                      label="Verstecken"
                      onClick={evt =>
                        this.setState({ secondEvaluationActivated: false }, () => {
                          this.state.evaluations[0].map.updateSize();
                          this.state.evaluations[1].map.updateSize();
                        })
                      }
                    />
                  </div>
                </>
              ) : (
                <div className="p-col-12" style={{ textAlign: "left" }}>
                  <Button
                    label="Anzeigen"
                    onClick={evt => {
                      this.setState({ secondEvaluationActivated: true }, () => {
                        this.state.evaluations[0].map.updateSize();
                        this.state.evaluations[1].map.setTarget(this.refs.mapContainer2 as HTMLElement);
                        if (this.state.evaluations[1].hasData && document.getElementById("info2")) {
                          document.getElementById("info2").innerHTML = this.state.evaluations[1].infotext;
                        }
                      });
                    }}
                  />
                </div>
              )
            ) : (
              <div className="p-col-12" style={{ textAlign: "left" }}>
                <Button label="Analyse" disabled={!this.state.activatedEvaluation.hasData} onClick={this.handleClick} />
              </div>
            )}
          </div>
        </div>
      </div>
    );
  }
}
