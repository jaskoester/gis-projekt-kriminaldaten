import * as React from "react";
import UrlUtils from "../utils/UrlUtils";
import { FileUpload } from "primereact/fileupload";
import { InputText } from "primereact/inputtext";
import { Password } from "primereact/password";
import "./Import.css";
import Papa from "papaparse/papaparse";
import GeoServerLayerTemplate from "../utils/GeoServerLayerTemplate";
import StringUtils from "../utils/StringUtils";
import { Messages } from "primereact/messages";

class MyUpload extends FileUpload {
  /**
   * Ueberschreiben der orginalen Funktion mit der gleichen Funktion ohne Absenden eines Requests
   */
  upload() {
    this.setState({ msgs: [] });
    let xhr = new XMLHttpRequest();
    let formData = new FormData();

    if (this.props.onBeforeUpload) {
      this.props.onBeforeUpload({
        xhr: xhr,
        formData: formData
      });
    }

    xhr.upload.addEventListener("progress", event => {
      if (event.lengthComputable) {
        this.setState({ progress: Math.round((event.loaded * 100) / event.total) });
      }

      if (this.props.onProgress) {
        this.props.onProgress({
          originalEvent: event,
          progress: this.state.progress
        });
      }
    });

    xhr.onreadystatechange = () => {
      if (xhr.readyState === 4) {
        this.setState({ progress: 0 });

        if (xhr.status >= 200 && xhr.status < 300) {
          if (this.props.onUpload) {
            this.props.onUpload({ xhr: xhr, files: this.state.files });
          }
        } else {
          if (this.props.onError) {
            this.props.onError({ xhr: xhr, files: this.state.files });
          }
        }

        (FileUpload.prototype as any).clear.call(this);
      }
    };

    if (this.props.onBeforeSend) {
      this.props.onBeforeSend({
        xhr: xhr,
        formData: formData
      });
    }
  }
}

export default class Import extends React.Component<{}> {
  state = {
    geousername: "admin",
    geopassword: "geoserver",
    dbusername: "postgres",
    dbpassword: "admin",
    context: "/geoserver/rest/workspaces/Kriminalstatistiken/datastores/kstatistiken/featuretypes/",
    year: 2019,
    file: {}
  };

  messages: Messages;

  /**
   * Liest ein CSV-File ein und legt das Ergebnis im State ab
   * @param file 
   */
  private readCSV(file) {
    const read = new FileReader();
    read.readAsText(file, "latin1");
    read.onloadend = () => {
      const result = Papa.parse(read.result);

      const header = result.data[1];
      const data = (result.data as Array<any>).slice(2);

      this.setState({ file: { header: header, data: data } });
    };
  }

  public render() {
    return (
      <div>
        <h1>Import von neuen Kriminalstatistiken</h1>
        <div className='p-grid'>
          <div className ='p-col-3'></div>
          <div className ='p-col-6' >
            <div className="BorderElement" style={{ height: "500px" }}>
              <div className="LoginForm">
                <div className="p-grid">
                  <div className="p-col-4">
                    <p>Zugriffsdaten Geoserver</p>
                    <span className="p-float-label" style={{ marginTop: "20px" }}>
                      <InputText id="geouser" value={this.state.geousername} onChange={e => this.setState({ geousername: (e as any).target.value })} />
                      <label htmlFor="geouser">Username</label>
                    </span>
                    <span className="p-float-label" style={{ marginTop: "20px" }}>
                      <Password
                        id="geopass"
                        value={this.state.geopassword}
                        onChange={e => this.setState({ geopassword: (e as any).target.value })}
                        feedback={false}
                      />
                      <label htmlFor="geopass">Passwort</label>
                    </span>
                  </div>
                  <div className="p-col-4">
                    <p>Zugriffsdaten Datenbank</p>
                    <span className="p-float-label" style={{ marginTop: "20px" }}>
                      <InputText id="dbuser" value={this.state.dbusername} onChange={e => this.setState({ dbusername: (e as any).target.value })} />
                      <label htmlFor="dbuser">Username</label>
                    </span>
                    <span className="p-float-label" style={{ marginTop: "20px" }}>
                      <Password
                        id="dbpass"
                        value={this.state.dbpassword}
                        onChange={e => this.setState({ dbpassword: (e as any).target.value })}
                        feedback={false}
                      />
                      <label htmlFor="dbpass">Passwort</label>
                    </span>
                  </div>
                  <div className="p-col-4">
                    <p>Neuer Datensatz</p>
                    <span className="p-float-label" style={{ marginTop: "20px" }}>
                      <InputText
                        id="year"
                        keyfilter="pint"
                        value={this.state.year}
                        onChange={e => this.setState({ year: (e as any).target.value })}
                      />
                      <label htmlFor="year">Jahr der Statistik</label>
                    </span>
                  </div>
                </div>
              </div>
              <hr></hr>
              <div>
                <p className="DescriptionHeadline">Laden Sie hier eine neue BKA-Kriminalstatistik hoch (Format: CSV)</p>
                <div className="FileUploadClass">
                  <MyUpload
                    onBeforeUpload={e => {
                      e.xhr.open("POST", UrlUtils.createURLForCurrentHost(8000, "/upload/" + this.state.year));
                      e.xhr.setRequestHeader(
                        "Authorization",
                        "Basic " + new Buffer(this.state.dbusername + ":" + this.state.dbpassword).toString("base64")
                      );
                      e.xhr.setRequestHeader("Content-Type", "application/json");
                      e.xhr.send(JSON.stringify(this.state.file));
                    }}
                    name="BKAPKS"
                    accept=".csv"
                    multiple={false}
                    chooseLabel="Statistik wählen"
                    cancelLabel="Abbrechen"
                    url={UrlUtils.createURLForCurrentHost(8000, "/upload/" + this.state.year)}
                    onSelect={e => {
                      this.readCSV(e.files[0]);
                    }}
                    onUpload={e => {
                      if (e.xhr.status >= 200 && e.xhr.status <= 300) {
                        const xhr = new XMLHttpRequest();
                        xhr.open("POST", UrlUtils.createURLForCurrentHost(8080, this.state.context));
                        xhr.setRequestHeader(
                          "Authorization",
                          "Basic " + new Buffer(this.state.geousername + ":" + this.state.geopassword).toString("base64")
                        );
                        xhr.setRequestHeader("Content-Type", "application/xml");
                        GeoServerLayerTemplate.getTemplate().then(resp => {
                          resp.text().then(text => {
                            const finalized = StringUtils.replaceAll(text, "%s1%", this.state.year.toString());
                            xhr.send(finalized);

                            //TODO: Uncomment, when cors-problem fixed
                            // xhr.onreadystatechange = () => {
                            //   if (xhr.readyState === 4) {
                        
                            //     if (xhr.status >= 200 && xhr.status < 300) {
                            //       this.messages.show({severity: 'success', summary: 'Erfolg', detail: 'Layer publiziert'});
                            //     } else {
                            //       console.log(xhr.status);
                            //       this.messages.show({severity: 'error', summary: 'Fehler', detail: 'Daten wurden nicht publiziert'});
                            //     }
                            //   }
                            // };
                          });
                        });
                      } else {
                        this.messages.show({severity: 'error', summary: 'Fehler', detail: 'Datei-Upload fehlgeschlagen, Daten wurden nicht übernommen'});
                      }
                    }}
                    onError={e => {
                      this.messages.show({severity: 'error', summary: 'Fehler', detail: 'Datei-Upload fehlgeschlagen, Daten wurden nicht übernommen'});
                    }}
                  ></MyUpload>
                </div>
              </div>
                <Messages ref={el => (this.messages = el)} style={{width: '90%', margin: 'auto', marginTop: '100px',}}></Messages>
            </div>
          </div>
          <div></div>
        </div>
        <div>

        </div>
      </div>
    );
  }
}
