import * as React from 'react';


export default class Sources extends React.Component<{}> {
  public render() {
    return (
      <div className="p-grid">
        <div className="p-col-12">
          <h1>Datenquellen</h1>
        </div>
        <div className="p-col-4 p-offset-2" style={{borderBottom:"solid"}}>
          <h4>Name der Daten</h4>
        </div>
        <div className="p-col-4" style={{borderBottom:"solid"}}>
          <h4>Link zur Quelle</h4>
        </div>
        <div className="p-col-4 p-offset-2" style={{borderBottom:"solid"}}>
          Kriminalitätsdaten 2013-2018:<br/>
          Grundtabelle - Kreise - ausgewählte Straftaten/-gruppen
        </div>

        <div className="p-col-4" style={{borderBottom:"solid"}}>
          <a href="https://www.bka.de/DE/AktuelleInformationen/StatistikenLagebilder/PolizeilicheKriminalstatistik/pks_node.html">Polizeiliche Kriminalstatistik (PKS)</a> 
        </div>
        <div className="p-col-4 p-offset-2" style={{borderBottom:"solid"}}>
          Bevölkerungsdaten 2017:<br/>
          Bevölkerung nach Geschlecht - Stichtag 31.12. - regionale Tiefe: Kreise und krfr.Städte
        </div>

        <div className="p-col-4" style={{borderBottom:"solid"}}>
          <a href="https://www.regionalstatistik.de/genesis/online/">Regionaldatenbank Deutschland</a> 
        </div>
        <div className="p-col-4 p-offset-2" style={{borderBottom:"solid"}}>
          Gebietsdaten 2017:<br/>
          Gebietsfläche in qkm -Stichtag 31.12. - regionale Tiefe: Kreise und krfr. Städte
        </div>
        <div className="p-col-4" style={{borderBottom:"solid"}}>
          <a href="https://www.regionalstatistik.de/genesis/online/">Regionaldatenbank Deutschland</a> 
        </div>
        <div className="p-col-4 p-offset-2" style={{borderBottom:"solid"}}>
          Arbeitslosendaten 2017:<br/>
          Arbeitslose nachausgewählten Personengruppen - Jahresdurchschnitt - regionale Tiefe: Kreise undkrfr. Städte
        </div>
        <div className="p-col-4" style={{borderBottom:"solid"}}>
          <a href="https://www.regionalstatistik.de/genesis/online/">Regionaldatenbank Deutschland</a> 
        </div>
        <div className="p-col-4 p-offset-2" style={{borderBottom:"solid"}}>
          Landkreise und kreisfreien Städten 2019:<br/>
          Shapefile vom Bundesamt für Kartographie und Geodäsie (BKG)
        </div>
        <div className="p-col-4" style={{borderBottom:"solid"}}>
          <a href="http://www.geodatenzentrum.de/auftrag1/archiv/vektor/vg2500/2019/vg2500_2019-01-01.geo84.shape.zip">Shapefile von Geodatenzentrum/BKG</a> 
        </div>
        <div className="p-col-8 p-offset-2" style={{height:"20px"}}>
        </div>
        <div className="p-col-8 p-offset-2" >
          Alle hier genannten Quellen unterliegen der "Datenlizenz Deutschland – Namensnennung – Version 2.0", siehe <a href="http://www.govdata.de/dl-de/by-2-0" >Lizenztext govdata</a>
        </div>
        
        
      </div>
    );
  }
}
