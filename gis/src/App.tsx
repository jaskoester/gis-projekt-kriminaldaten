import React from "react";
import "./App.css";
import Nav from "./components/nav";
import "primereact/resources/themes/nova-light/theme.css";
import "primeflex/primeflex.css";
import "primereact/resources/primereact.min.css";
import "primeicons/primeicons.css";
import "ol-popup/src/ol-popup.css";
import "ol/ol.css";
import { HashRouter, Switch, Route } from "react-router-dom";
import OpenLayerView from "./components/OpenLayerView";
import Import from "./components/Import";
import Sources from "./components/Sources";

class App extends React.Component<{}, { data: any; activeURL: string }> {
  constructor(props) {
    super(props);
    this.state = {
      data: { nav: { navlist: { items: [] }, activeItem: {} }},
      activeURL: ""
    };
  }

  public render() {
    return (
      <div className="App">
        <Nav
          data={this.state.data}
          activeURL={this.state.activeURL}
          onChange={value => {
            this.setState({
              data: Object.assign({}, this.state.data, { nav: value })
            });
          }}
        />

        <HashRouter>
          <React.Suspense fallback={<div>Loading...</div>}>
            <Switch>
              <Route
                path="*/map"
                render={() => (
                  <OpenLayerView/>
                )}
                exact
              />
              ,
              <Route 
                path="*/import" 
                render={() => (
                  <Import/>
                )} 
                exact />
                <Route 
                path="*/source" 
                render={() => (
                  <Sources/>
                )} 
                exact />
            </Switch>
          </React.Suspense>
        </HashRouter>
      </div>
    );
  }
}

export default App;
