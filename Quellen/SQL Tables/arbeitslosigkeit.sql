--
-- PostgreSQL database dump
--

-- Dumped from database version 11.4
-- Dumped by pg_dump version 11.4

-- Started on 2019-08-14 15:41:30

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'LATIN1';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 226 (class 1259 OID 332916)
-- Name: Arbeitslosigkeit; Type: TABLE; Schema: kriminal; Owner: postgres
--

CREATE TABLE kriminal."Arbeitslosigkeit" (
    id integer NOT NULL,
    "Jahr" integer,
    "GS" integer,
    "Gebiet" character varying,
    "Arbeitslose" character varying,
    "Arbeitslose - Ausl�nder" character varying,
    "Arbeitslose - schwerbehindert" character varying,
    "Arbeitslose - 15 bis unter 20 Jahre" character varying,
    "Arbeitslose - 15 bis unter 25 Jahre" character varying,
    "Arbeitslose - 55 bis unter 65 Jahre" character varying,
    "Arbeitslose - langzeitarbeitslos" character varying,
    "Arbeitslosenquote bez. abh. zivile Erwerbspers." character varying,
    "Arbeitslosenquote bez. auf alle zivile Erwerbsp." character varying,
    "Arbeitslosenquote - M�nner" character varying,
    "Arbeitslosenquote - Frauen" character varying,
    "Arbeitslosenquote - Ausl�nder" character varying,
    "Arbeitslosenquote - 15 bis unter 25 Jahre" character varying
);


ALTER TABLE kriminal."Arbeitslosigkeit" OWNER TO postgres;

--
-- TOC entry 225 (class 1259 OID 332914)
-- Name: Arbeitslosigkeit_id_seq; Type: SEQUENCE; Schema: kriminal; Owner: postgres
--

CREATE SEQUENCE kriminal."Arbeitslosigkeit_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE kriminal."Arbeitslosigkeit_id_seq" OWNER TO postgres;

--
-- TOC entry 4293 (class 0 OID 0)
-- Dependencies: 225
-- Name: Arbeitslosigkeit_id_seq; Type: SEQUENCE OWNED BY; Schema: kriminal; Owner: postgres
--

ALTER SEQUENCE kriminal."Arbeitslosigkeit_id_seq" OWNED BY kriminal."Arbeitslosigkeit".id;


--
-- TOC entry 4155 (class 2604 OID 332919)
-- Name: Arbeitslosigkeit id; Type: DEFAULT; Schema: kriminal; Owner: postgres
--

ALTER TABLE ONLY kriminal."Arbeitslosigkeit" ALTER COLUMN id SET DEFAULT nextval('kriminal."Arbeitslosigkeit_id_seq"'::regclass);


--
-- TOC entry 4287 (class 0 OID 332916)
-- Dependencies: 226
-- Data for Name: Arbeitslosigkeit; Type: TABLE DATA; Schema: kriminal; Owner: postgres
--

INSERT INTO kriminal."Arbeitslosigkeit" VALUES (1, 2017, 1, '  Schleswig-Holstein', '92434', '18688', '5038', '2072', '9703', '18788', '32362', '6,7', '6', '6,5', '5,5', '21,5', '5,9');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (2, 2017, 1001, '      Flensburg, Kreisfreie Stadt', '4512', '983', '268', '108', '566', '654', '1436', '9,8', '8,9', '10,2', '7,6', '.', '7,9');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (3, 2017, 1002, '      Kiel, Landeshauptstadt, Kreisfreie Stadt', '12345', '3103', '516', '220', '1100', '1842', '4661', '10', '9,1', '10,2', '7,9', '.', '6,6');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (4, 2017, 1003, '      L�beck, Hansestadt, Kreisfreie Stadt', '9692', '2050', '457', '180', '836', '1667', '3516', '9,5', '8,6', '9,3', '7,8', '.', '7');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (5, 2017, 1004, '      Neum�nster, Kreisfreie Stadt', '3836', '724', '202', '105', '440', '685', '1470', '10,3', '9,2', '9,7', '8,6', '.', '9,5');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (6, 2017, 1051, '      Dithmarschen, Landkreis', '4628', '791', '236', '147', '634', '826', '1418', '7,6', '6,8', '7,1', '6,5', '.', '7,6');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (7, 2017, 1053, '      Herzogtum Lauenburg, Landkreis', '5593', '1204', '346', '138', '637', '1249', '2010', '6,1', '5,5', '5,7', '5,2', '.', '6,4');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (8, 2017, 1054, '      Nordfriesland, Landkreis', '5654', '943', '270', '138', '619', '1442', '2326', '6,8', '6,1', '6,6', '5,7', '.', '5,4');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (9, 2017, 1055, '      Ostholstein, Landkreis', '5747', '794', '388', '135', '629', '1466', '1940', '6,3', '5,6', '6', '5,1', '.', '5,8');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (10, 2017, 1056, '      Pinneberg, Landkreis', '8595', '2325', '409', '167', '776', '1965', '2722', '5,6', '5', '5,3', '4,7', '.', '4,6');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (11, 2017, 1057, '      Pl�n, Landkreis', '3268', '449', '210', '76', '350', '845', '1070', '5,7', '5', '5,4', '4,7', '.', '5,5');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (12, 2017, 1058, '      Rendsburg-Eckernf�rde, Landkreis', '6770', '1098', '417', '157', '781', '1532', '2155', '5,5', '4,9', '5,2', '4,6', '.', '5,5');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (13, 2017, 1059, '      Schleswig-Flensburg, Landkreis', '6238', '917', '494', '137', '652', '1351', '2786', '6,6', '6', '6,5', '5,4', '.', '5,5');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (14, 2017, 1060, '      Segeberg, Landkreis', '6915', '1471', '379', '159', '714', '1434', '2211', '5,2', '4,7', '5', '4,4', '.', '4,8');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (15, 2017, 1061, '      Steinburg, Landkreis', '4278', '773', '188', '124', '541', '857', '1491', '6,9', '6,1', '6,6', '5,5', '.', '7,4');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (16, 2017, 1062, '      Stormarn, Landkreis', '4362', '1064', '258', '81', '428', '974', '1150', '3,8', '3,4', '3,6', '3,3', '.', '3,6');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (17, 2017, 2000, '  Hamburg', '69248', '21863', '3184', '1040', '5408', '11476', '21330', '7,7', '6,8', '7,3', '6,3', '16,1', '5,6');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (18, 2017, 3, '  Niedersachsen', '244260', '58458', '11478', '4940', '25649', '48532', '77185', '6,3', '5,8', '5,9', '5,5', '18,7', '5,5');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (19, 2017, 31, '    Braunschweig, Stat. Region', '50724', '11518', '2239', '995', '5212', '9842', '17925', '.', '.', '.', '.', '.', '.');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (20, 2017, 3101, '      Braunschweig, Kreisfreie Stadt', '8038', '1776', '354', '90', '531', '1474', '2859', '6,4', '5,8', '5,9', '5,7', '.', '4');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (21, 2017, 3102, '      Salzgitter, Kreisfreie Stadt', '5530', '2079', '215', '117', '642', '953', '2094', '11,6', '10,5', '10,1', '11', '.', '11,1');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (22, 2017, 3103, '      Wolfsburg, Kreisfreie Stadt', '3308', '1232', '121', '60', '377', '582', '1054', '5,4', '4,9', '4,9', '4,9', '.', '5,7');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (23, 2017, 3151, '      Gifhorn, Landkreis', '4410', '837', '193', '97', '552', '843', '1510', '5,1', '4,7', '4,6', '4,7', '.', '5,6');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (24, 2017, 3152, '      G�ttingen, Landkreis', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (25, 2017, 3153, '      Goslar, Landkreis', '4855', '844', '189', '96', '496', '1064', '1747', '7,9', '7,1', '7,7', '6,6', '.', '7,3');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (26, 2017, 3154, '      Helmstedt, Landkreis', '3393', '600', '139', '61', '346', '724', '1410', '7,6', '6,9', '7,3', '6,4', '.', '7,3');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (27, 2017, 3155, '      Northeim, Landkreis', '4064', '671', '172', '86', '448', '877', '1055', '6,4', '5,8', '6,1', '5,4', '.', '6,6');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (28, 2017, 3156, '      Osterode am Harz, Landkreis', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (29, 2017, 3157, '      Peine, Landkreis', '3727', '909', '175', '83', '417', '725', '1054', '5,8', '5,3', '5,3', '5,3', '.', '5,9');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (30, 2017, 3158, '      Wolfenb�ttel, Landkreis', '3418', '592', '150', '72', '366', '796', '1223', '6,1', '5,5', '5,9', '5,1', '.', '6,1');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (31, 2017, 3159, '      G�ttingen, Landkreis', '9980', '1977', '531', '233', '1037', '1802', '3919', '6,5', '5,9', '6,4', '5,4', '.', '5,6');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (32, 2017, 32, '    Hannover, Stat. Region', '73901', '21315', '2313', '1478', '7370', '14149', '17892', '.', '.', '.', '.', '.', '.');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (33, 2017, 3241, '      Region Hannover, Landkreis', '43570', '14321', '870', '767', '3950', '7922', '6972', '7,8', '7,1', '7,5', '6,7', '.', '6,5');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (34, 2017, 3241001, '      Hannover, Landeshauptstadt', '25162', '8913', '1165', '398', '2113', '4304', '10989', '.', '.', '.', '.', '.', '.');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (35, 2017, 3251, '      Diepholz, Landkreis', '4993', '1111', '193', '122', '620', '974', '1481', '4,6', '4,2', '4,2', '4,2', '.', '4,8');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (36, 2017, 3252, '      Hameln-Pyrmont, Landkreis', '5189', '1361', '295', '105', '518', '1063', '2031', '7,5', '6,8', '7,4', '6', '.', '6,7');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (37, 2017, 3254, '      Hildesheim, Landkreis', '9402', '2086', '452', '183', '944', '1998', '3736', '7,1', '6,4', '7', '5,8', '.', '6');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (38, 2017, 3255, '      Holzminden, Landkreis', '2391', '450', '102', '54', '254', '515', '927', '7,4', '6,7', '7,2', '6,2', '.', '6,9');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (39, 2017, 3256, '      Nienburg (Weser), Landkreis', '3532', '792', '182', '96', '468', '735', '1133', '5,9', '5,3', '5,4', '5,2', '.', '6,3');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (40, 2017, 3257, '      Schaumburg, Landkreis', '4823', '1193', '219', '151', '616', '942', '1612', '6,6', '6', '6,3', '5,7', '.', '7,9');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (41, 2017, 33, '    L�neburg, Stat. Region', '47452', '9421', '2374', '1043', '5248', '9908', '16884', '.', '.', '.', '.', '.', '.');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (42, 2017, 3351, '      Celle, Landkreis', '6408', '1262', '248', '147', '704', '1326', '2410', '7,8', '7,1', '7,5', '6,6', '.', '7,2');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (43, 2017, 3352, '      Cuxhaven, Landkreis', '5554', '997', '322', '113', '587', '1192', '2223', '6', '5,5', '5,8', '5,1', '.', '5,5');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (44, 2017, 3353, '      Harburg, Landkreis', '5503', '1123', '248', '98', '577', '1365', '1680', '4,5', '4,1', '4,2', '3,9', '.', '4,2');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (45, 2017, 3354, '      L�chow-Dannenberg, Landkreis', '1825', '197', '77', '41', '197', '390', '782', '9', '8,1', '8,6', '7,6', '.', '9,4');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (46, 2017, 3355, '      L�neburg, Landkreis', '5280', '996', '236', '123', '565', '1039', '1567', '6,1', '5,6', '6,2', '4,9', '.', '5,7');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (47, 2017, 3356, '      Osterholz, Landkreis', '2239', '492', '121', '57', '287', '451', '564', '4,1', '3,8', '3,6', '3,9', '.', '4,6');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (48, 2017, 3357, '      Rotenburg (W�mme), Landkreis', '3868', '785', '243', '104', '502', '823', '1436', '4,8', '4,3', '4,5', '4,1', '.', '4,7');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (49, 2017, 3358, '      Heidekreis, Landkreis', '4445', '816', '260', '100', '484', '880', '1761', '6,7', '6,1', '6', '6,1', '.', '5,7');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (50, 2017, 3359, '      Stade, Landkreis', '6136', '1368', '294', '152', '688', '1173', '2318', '6,1', '5,6', '5,5', '5,6', '.', '5,5');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (51, 2017, 3360, '      Uelzen, Landkreis', '2694', '317', '152', '40', '271', '620', '890', '6,4', '5,8', '6,4', '5,1', '.', '5,8');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (52, 2017, 3361, '      Verden, Landkreis', '3501', '1069', '173', '70', '386', '647', '1253', '5,3', '4,8', '5', '4,6', '.', '4,9');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (53, 2017, 34, '    Weser-Ems, Stat. Region', '72182', '16205', '4552', '1424', '7818', '14634', '24484', '.', '.', '.', '.', '.', '.');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (54, 2017, 3401, '      Delmenhorst, Kreisfreie Stadt', '4042', '1345', '202', '71', '396', '754', '1675', '11,3', '10,2', '10', '10,5', '.', '9,6');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (55, 2017, 3402, '      Emden, Kreisfreie Stadt', '2341', '447', '118', '50', '280', '390', '930', '9,5', '8,7', '8,9', '8,4', '.', '7,9');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (56, 2017, 3403, '      Oldenburg (Oldenburg), Kreisfreie Stadt', '6706', '1452', '388', '142', '728', '1152', '2559', '8', '7,3', '8,2', '6,5', '.', '7,1');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (57, 2017, 3404, '      Osnabr�ck, Kreisfreie Stadt', '6933', '2293', '351', '131', '671', '1136', '2750', '8,2', '7,5', '8,1', '7', '.', '5,8');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (58, 2017, 3405, '      Wilhelmshaven, Kreisfreie Stadt', '4344', '830', '302', '57', '385', '833', '1811', '12,5', '11,4', '12,1', '10,5', '.', '8,7');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (59, 2017, 3451, '      Ammerland, Landkreis', '2771', '537', '175', '50', '271', '591', '720', '4,7', '4,3', '4,4', '4,2', '.', '3,7');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (60, 2017, 3452, '      Aurich, Landkreis', '6934', '1049', '470', '154', '852', '1370', '2596', '7,5', '6,9', '6,9', '6,9', '.', '6,8');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (61, 2017, 3453, '      Cloppenburg, Landkreis', '4383', '951', '294', '106', '523', '983', '1293', '5,2', '4,8', '4,5', '5,1', '.', '4,1');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (62, 2017, 3454, '      Emsland, Landkreis', '5324', '1160', '267', '114', '671', '1094', '1093', '3,3', '3', '2,9', '3,1', '.', '2,9');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (63, 2017, 3455, '      Friesland, Landkreis', '2582', '371', '188', '44', '250', '626', '769', '5,8', '5,3', '5,5', '5', '.', '4,4');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (64, 2017, 3456, '      Grafschaft Bentheim, Landkreis', '2741', '756', '156', '66', '326', '466', '910', '4,1', '3,7', '3,7', '3,7', '.', '3,4');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (65, 2017, 3457, '      Leer, Landkreis', '5203', '932', '394', '112', '613', '1019', '1650', '6,6', '6', '5,8', '6,2', '.', '5,7');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (66, 2017, 3458, '      Oldenburg, Landkreis', '2689', '606', '185', '60', '287', '610', '759', '4,1', '3,7', '3,7', '3,8', '.', '3,6');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (67, 2017, 3459, '      Osnabr�ck, Landkreis', '6802', '1330', '542', '94', '616', '1753', '2267', '3,7', '3,4', '3,4', '3,4', '.', '2,6');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (68, 2017, 3460, '      Vechta, Landkreis', '3367', '1327', '186', '76', '435', '712', '855', '4,3', '3,9', '3,8', '4', '.', '3,7');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (69, 2017, 3461, '      Wesermarsch, Landkreis', '3229', '619', '227', '68', '334', '685', '1363', '7,6', '6,8', '6,4', '7,4', '.', '5,9');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (70, 2017, 3462, '      Wittmund, Landkreis', '1792', '200', '107', '27', '178', '458', '484', '6,9', '6,3', '6,4', '6,1', '.', '4,8');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (71, 2017, 4000, '  Bremen', '35687', '12442', '1536', '602', '3402', '5866', '15732', '11,2', '10,2', '10,7', '9,7', '25,7', '9,3');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (72, 2017, 4011, '      Bremen, Kreisfreie Stadt', '28027', '10330', '1179', '448', '2599', '4598', '12497', '10,6', '9,7', '10,1', '9,2', '.', '8,6');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (73, 2017, 4012, '      Bremerhaven, Kreisfreie Stadt', '7660', '2111', '357', '154', '803', '1267', '3235', '14,3', '13', '13,4', '12,6', '.', '12,6');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (74, 2017, 5, '  Nordrhein-Westfalen', '701219', '214021', '47153', '12019', '64090', '134040', '287671', '8,1', '7,4', '7,6', '7,1', '21,2', '6,3');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (75, 2017, 51, '    D�sseldorf, Regierungsbezirk', '229165', '74548', '15250', '3795', '19708', '42248', '97118', '.', '.', '.', '.', '.', '.');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (76, 2017, 5111, '      D�sseldorf, Kreisfreie Stadt', '24259', '9303', '1788', '281', '1608', '4608', '8859', '8,1', '7,4', '7,8', '6,9', '.', '5,8');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (77, 2017, 5112, '      Duisburg, Kreisfreie Stadt', '31309', '12389', '2014', '592', '2776', '4974', '13813', '13,7', '12,5', '12,1', '12,9', '.', '10,7');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (78, 2017, 5113, '      Essen, Kreisfreie Stadt', '33699', '11754', '1836', '638', '3185', '5015', '14621', '12,4', '11,4', '11,9', '10,9', '.', '10,9');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (79, 2017, 5114, '      Krefeld, Kreisfreie Stadt', '12370', '3537', '831', '143', '859', '2406', '5853', '11,5', '10,4', '10,7', '10,1', '.', '7,3');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (80, 2017, 5116, '      M�nchengladbach, Kreisfreie Stadt', '13968', '3670', '1001', '264', '1330', '2579', '5713', '11,2', '10,2', '10,2', '10,2', '.', '8,9');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (81, 2017, 5117, '      M�lheim an der Ruhr, Kreisfreie Stadt', '6692', '2518', '372', '43', '294', '1461', '3576', '8,7', '8', '8,1', '7,8', '.', '4,1');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (82, 2017, 5119, '      Oberhausen, Kreisfreie Stadt', '11767', '3462', '848', '216', '1156', '2099', '5721', '11,8', '10,8', '11', '10,5', '.', '10,5');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (83, 2017, 5120, '      Remscheid, Kreisfreie Stadt', '4924', '1730', '319', '104', '473', '844', '2130', '9,3', '8,5', '8,5', '8,4', '.', '8');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (84, 2017, 5122, '      Solingen, Kreisfreie Stadt', '7134', '2484', '428', '180', '819', '1207', '2919', '9,1', '8,3', '8,6', '8,1', '.', '9,2');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (85, 2017, 5124, '      Wuppertal, Kreisfreie Stadt', '16571', '6848', '848', '339', '1660', '2455', '6395', '10,1', '9,3', '9,8', '8,7', '.', '9');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (86, 2017, 5154, '      Kleve, Kreis', '10130', '2440', '640', '208', '1132', '2030', '4624', '6,8', '6,2', '6,3', '6,2', '.', '5,8');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (87, 2017, 5158, '      Mettmann, Kreis', '16008', '4742', '998', '197', '1127', '3787', '6775', '6,9', '6,2', '6,5', '5,9', '.', '4,8');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (88, 2017, 5162, '      Rhein-Kreis Neuss', '13466', '3613', '1050', '160', '889', '2932', '5180', '6,3', '5,7', '5,9', '5,4', '.', '4');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (89, 2017, 5166, '      Viersen, Kreis', '10328', '2275', '880', '154', '881', '2482', '4080', '7,1', '6,4', '6,7', '6,2', '.', '5,2');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (90, 2017, 5170, '      Wesel, Kreis', '16539', '3784', '1397', '276', '1520', '3369', '6859', '7,6', '6,9', '7', '6,7', '.', '6,3');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (91, 2017, 53, '    K�ln, Regierungsbezirk', '162417', '48294', '10064', '2601', '13900', '32476', '63238', '.', '.', '.', '.', '.', '.');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (92, 2017, 5314, '      Bonn, Kreisfreie Stadt', '11126', '3422', '828', '155', '865', '2034', '4897', '7,3', '6,7', '7,3', '6', '.', '5,2');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (93, 2017, 5315, '      K�ln, Kreisfreie Stadt', '48227', '17702', '2861', '612', '3126', '9070', '21296', '9,2', '8,4', '8,9', '7,9', '.', '5,6');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (94, 2017, 5316, '      Leverkusen, Kreisfreie Stadt', '6994', '2206', '474', '121', '685', '1285', '2753', '9', '8,1', '8,3', '7,9', '.', '8');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (95, 2017, 5334, '      St�dteregion Aachen (einschl. Stadt Aachen)', '23094', '6728', '823', '318', '1883', '4389', '4961', '8,5', '7,8', '8', '7,5', '.', '5,4');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (96, 2017, 5334002, '      Aachen, krfr. Stadt', '11046', '3774', '593', '142', '865', '2018', '5251', '.', '.', '.', '.', '.', '.');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (97, 2017, 5354, '      Aachen, Kreis', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (98, 2017, 5358, '      D�ren, Kreis', '9861', '2392', '564', '199', '1099', '1747', '4158', '7,7', '7,1', '7,2', '6,9', '.', '7,1');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (99, 2017, 5362, '      Rhein-Erft-Kreis', '16890', '4742', '1245', '297', '1511', '3804', '7181', '7,4', '6,7', '6,8', '6,7', '.', '5,9');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (100, 2017, 5366, '      Euskirchen, Kreis', '5715', '1048', '355', '99', '528', '1334', '2264', '5,9', '5,4', '5,6', '5,2', '.', '4,4');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (101, 2017, 5370, '      Heinsberg, Kreis', '7496', '1512', '461', '140', '773', '1626', '2419', '6,2', '5,7', '5,7', '5,6', '.', '5,2');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (102, 2017, 5374, '      Oberbergischer Kreis', '7792', '1842', '624', '153', '886', '1871', '3119', '5,8', '5,3', '5,4', '5,1', '.', '5,1');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (103, 2017, 5378, '      Rheinisch-Bergischer Kreis', '8850', '2257', '671', '130', '762', '2131', '4024', '6,6', '6', '6,4', '5,6', '.', '5,2');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (104, 2017, 5382, '      Rhein-Sieg-Kreis', '16370', '4444', '1158', '376', '1783', '3185', '6166', '5,7', '5,2', '5,5', '4,8', '.', '5,3');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (105, 2017, 55, '    M�nster, Regierungsbezirk', '95277', '27728', '5974', '1798', '9659', '18123', '41714', '.', '.', '.', '.', '.', '.');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (106, 2017, 5512, '      Bottrop, Kreisfreie Stadt', '4628', '1164', '298', '82', '476', '854', '1867', '8,5', '7,7', '8,2', '7,3', '.', '8');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (107, 2017, 5513, '      Gelsenkirchen, Kreisfreie Stadt', '17766', '6227', '1102', '308', '1654', '2887', '7483', '15,4', '14', '14,1', '13,8', '.', '12,2');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (108, 2017, 5515, '      M�nster, Kreisfreie Stadt', '8894', '2405', '542', '142', '791', '1704', '3809', '5,8', '5,4', '6,1', '4,7', '.', '3,8');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (109, 2017, 5554, '      Borken, Kreis', '7851', '2183', '603', '143', '875', '1783', '2815', '4,1', '3,7', '3,7', '3,8', '.', '3');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (110, 2017, 5558, '      Coesfeld, Kreis', '3588', '786', '227', '90', '475', '802', '909', '3,3', '3', '3,2', '2,8', '.', '3,3');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (111, 2017, 5562, '      Recklinghausen, Kreis', '32910', '8851', '1816', '548', '2902', '6383', '17546', '11,2', '10,2', '10,5', '9,9', '.', '8,9');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (112, 2017, 5566, '      Steinfurt, Kreis', '11093', '3376', '846', '301', '1506', '2045', '3639', '4,9', '4,5', '4,5', '4,4', '.', '4,9');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (113, 2017, 5570, '      Warendorf, Kreis', '8546', '2736', '540', '184', '979', '1664', '3646', '6,1', '5,5', '5,7', '5,4', '.', '5,5');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (114, 2017, 57, '    Detmold, Regierungsbezirk', '64501', '18118', '3738', '1207', '6982', '12661', '24682', '.', '.', '.', '.', '.', '.');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (115, 2017, 5711, '      Bielefeld, Kreisfreie Stadt', '14359', '4982', '764', '244', '1363', '2476', '6291', '9,1', '8,3', '8,5', '8', '.', '7');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (116, 2017, 5754, '      G�tersloh, Kreis', '9279', '3138', '457', '148', '988', '1758', '3229', '5', '4,5', '4,3', '4,8', '.', '4,1');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (117, 2017, 5758, '      Herford, Kreis', '7594', '1911', '543', '116', '762', '1715', '2671', '6,2', '5,6', '5,8', '5,4', '.', '5,4');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (118, 2017, 5762, '      H�xter, Kreis', '3414', '640', '288', '62', '387', '801', '1167', '5', '4,6', '4,9', '4,1', '.', '4,5');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (119, 2017, 5766, '      Lippe, Kreis', '12037', '3138', '557', '268', '1383', '2100', '5136', '7,2', '6,6', '6,8', '6,3', '.', '6,7');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (120, 2017, 5770, '      Minden-L�bbecke, Kreis', '8422', '2013', '442', '186', '1085', '1616', '2801', '5,6', '5,1', '5,4', '4,8', '.', '5,9');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (121, 2017, 5774, '      Paderborn, Kreis', '9395', '2296', '687', '182', '1014', '2195', '3387', '6,1', '5,6', '5,7', '5,5', '.', '5');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (122, 2017, 59, '    Arnsberg, Regierungsbezirk', '149859', '45332', '12127', '2618', '13841', '28532', '60919', '.', '.', '.', '.', '.', '.');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (123, 2017, 5911, '      Bochum, Kreisfreie Stadt', '18392', '5401', '1436', '296', '1606', '3443', '7772', '10,7', '9,7', '10,6', '8,7', '.', '8,5');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (124, 2017, 5913, '      Dortmund, Kreisfreie Stadt', '34100', '11892', '2691', '613', '3050', '5553', '14227', '12,1', '11,1', '11,5', '10,5', '.', '9,7');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (125, 2017, 5914, '      Hagen, Kreisfreie Stadt', '10221', '3918', '697', '251', '1119', '1758', '4630', '11,4', '10,3', '10,6', '10', '.', '10,6');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (126, 2017, 5915, '      Hamm, Kreisfreie Stadt', '8349', '2933', '647', '148', '786', '1299', '3944', '10', '9,2', '9,1', '9,3', '.', '7,8');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (127, 2017, 5916, '      Herne, Kreisfreie Stadt', '9335', '2963', '744', '161', '857', '1548', '3735', '13,3', '12,1', '12,5', '11,6', '.', '11');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (128, 2017, 5954, '      Ennepe-Ruhr-Kreis', '10890', '2974', '949', '130', '838', '2250', '4025', '7', '6,4', '6,8', '6', '.', '5,3');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (129, 2017, 5958, '      Hochsauerlandkreis', '6611', '1727', '499', '124', '738', '1392', '2548', '5', '4,5', '4,6', '4,4', '.', '4,3');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (130, 2017, 5962, '      M�rkischer Kreis', '14710', '4300', '1257', '236', '1318', '3360', '5962', '7,2', '6,5', '6,5', '6,6', '.', '5,4');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (131, 2017, 5966, '      Olpe, Kreis', '3156', '903', '318', '63', '350', '686', '1011', '4,4', '4', '4', '4,1', '.', '3,6');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (132, 2017, 5970, '      Siegen-Wittgenstein, Kreis', '7942', '2177', '650', '193', '950', '1737', '2742', '5,6', '5,1', '5,2', '5,1', '.', '5');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (133, 2017, 5974, '      Soest, Kreis', '9585', '2242', '759', '164', '938', '2142', '3492', '6,3', '5,8', '5,9', '5,6', '.', '5,3');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (134, 2017, 5978, '      Unna, Kreis', '16569', '3903', '1480', '238', '1292', '3364', '6831', '8,7', '7,9', '8,2', '7,7', '.', '6,1');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (135, 2017, 6, '  Hessen', '166286', '59739', '12240', '3617', '16796', '31629', '59953', '5,6', '5', '5,1', '4,9', '12,6', '5');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (136, 2017, 64, '    Darmstadt, Regierungsbezirk', '108606', '44494', '6948', '2408', '10606', '19605', '39513', '.', '.', '.', '.', '.', '.');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (137, 2017, 6411, '      Darmstadt, Kreisfreie Stadt', '5007', '1957', '229', '121', '523', '813', '1789', '6,5', '5,9', '6,1', '5,7', '.', '5,7');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (138, 2017, 6412, '      Frankfurt am Main, Kreisfreie Stadt', '23307', '11000', '1319', '432', '1922', '3772', '7679', '6,6', '5,9', '6,1', '5,6', '.', '5,7');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (139, 2017, 6413, '      Offenbach am Main, Kreisfreie Stadt', '6770', '3800', '482', '126', '459', '1123', '2744', '10,7', '9,7', '8,6', '11', '.', '6,6');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (140, 2017, 6414, '      Wiesbaden, Landeshauptstadt, Kreisfreie Stadt', '11042', '4366', '621', '314', '1268', '1831', '4522', '8,1', '7,4', '7,4', '7,4', '.', '8,5');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (141, 2017, 6431, '      Bergstra�e, Landkreis', '5481', '1689', '356', '44', '304', '1223', '1977', '4,2', '3,8', '3,8', '3,7', '.', '2,1');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (142, 2017, 6432, '      Darmstadt-Dieburg, Landkreis', '7653', '2794', '452', '163', '840', '1671', '3315', '5,3', '4,7', '4,8', '4,7', '.', '5,1');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (143, 2017, 6433, '      Gro�-Gerau, Landkreis', '7955', '3708', '512', '207', '854', '1276', '3020', '6', '5,4', '5,1', '5,7', '.', '5,6');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (144, 2017, 6434, '      Hochtaunuskreis', '4228', '1680', '349', '77', '392', '866', '1723', '4,1', '3,6', '3,8', '3,4', '.', '3,9');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (145, 2017, 6435, '      Main-Kinzig-Kreis', '10158', '3679', '766', '314', '1210', '1711', '3470', '5,1', '4,6', '4,5', '4,6', '.', '5,3');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (146, 2017, 6436, '      Main-Taunus-Kreis', '4583', '1871', '353', '115', '477', '741', '1588', '4,1', '3,7', '3,8', '3,5', '.', '4,5');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (147, 2017, 6437, '      Odenwaldkreis', '2618', '842', '152', '82', '314', '712', '1107', '5,6', '5', '4,9', '5,2', '.', '5,3');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (148, 2017, 6438, '      Offenbach, Landkreis', '8354', '3553', '538', '185', '811', '1503', '2405', '5', '4,5', '4,4', '4,5', '.', '4,6');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (149, 2017, 6439, '      Rheingau-Taunus-Kreis', '4494', '1556', '208', '81', '442', '941', '1855', '5,2', '4,6', '4,8', '4,4', '.', '4,7');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (150, 2017, 6440, '      Wetteraukreis', '6958', '2002', '611', '147', '790', '1422', '2319', '4,8', '4,3', '4,5', '3,9', '.', '4,9');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (151, 2017, 65, '    Gie�en, Regierungsbezirk', '26998', '7264', '2732', '515', '2829', '5562', '10445', '.', '.', '.', '.', '.', '.');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (152, 2017, 6531, '      Gie�en, Landkreis', '8217', '2161', '869', '138', '801', '1638', '3543', '6,4', '5,8', '6,4', '5,2', '.', '4,9');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (153, 2017, 6532, '      Lahn-Dill-Kreis', '7379', '2180', '795', '143', '759', '1672', '3262', '6', '5,4', '5,4', '5,4', '.', '5,2');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (154, 2017, 6533, '      Limburg-Weilburg, Landkreis', '4097', '1133', '346', '78', '412', '772', '1316', '5', '4,5', '4,5', '4,4', '.', '4,2');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (155, 2017, 6534, '      Marburg-Biedenkopf, Landkreis', '4926', '1290', '486', '98', '568', '899', '1627', '4,2', '3,8', '4', '3,5', '.', '3,7');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (156, 2017, 6535, '      Vogelsbergkreis', '2379', '500', '236', '58', '289', '580', '697', '4,6', '4,1', '4,6', '3,6', '.', '4,9');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (157, 2017, 66, '    Kassel, Regierungsbezirk', '30682', '7980', '2560', '694', '3362', '6462', '9995', '.', '.', '.', '.', '.', '.');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (158, 2017, 6611, '      Kassel, Kreisfreie Stadt', '8638', '3173', '449', '191', '922', '1364', '3035', '9,1', '8,2', '8,7', '7,6', '.', '7,8');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (159, 2017, 6631, '      Fulda, Landkreis', '3506', '858', '427', '86', '413', '782', '851', '3,2', '2,9', '2,9', '2,9', '.', '2,8');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (160, 2017, 6632, '      Hersfeld-Rotenburg, Landkreis', '2811', '808', '244', '67', '303', '595', '1118', '4,9', '4,4', '4,4', '4,5', '.', '4,6');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (161, 2017, 6633, '      Kassel, Landkreis', '5243', '1102', '426', '117', '572', '1218', '1686', '4,7', '4,2', '4,3', '4', '.', '4,8');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (162, 2017, 6634, '      Schwalm-Eder-Kreis', '4184', '859', '368', '101', '464', '927', '1295', '4,8', '4,3', '4,5', '4,1', '.', '4,7');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (163, 2017, 6635, '      Waldeck-Frankenberg, Landkreis', '3423', '723', '360', '68', '363', '876', '1021', '4,4', '3,9', '4,2', '3,6', '.', '3,8');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (164, 2017, 6636, '      Werra-Mei�ner-Kreis', '2876', '457', '286', '64', '326', '700', '989', '6,3', '5,6', '6', '5,1', '.', '6,3');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (165, 2017, 7, '  Rheinland-Pfalz', '106299', '27025', '6163', '2431', '11457', '22858', '33604', '5,3', '4,8', '5', '4,7', '13,4', '4,6');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (166, 2017, 71, '    Koblenz, Stat. Region', '35979', '7975', '1983', '778', '3671', '7998', '10828', '.', '.', '.', '.', '.', '.');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (167, 2017, 7111, '      Koblenz, Kreisfreie Stadt', '3669', '1032', '155', '62', '324', '619', '1191', '6,7', '6,1', '6,6', '5,6', '.', '4,5');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (168, 2017, 7131, '      Ahrweiler, Landkreis', '2577', '707', '146', '50', '263', '604', '723', '4,2', '3,8', '4,1', '3,5', '.', '3,3');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (169, 2017, 7132, '      Altenkirchen (Westerwald), Landkreis', '3322', '531', '259', '72', '344', '790', '1103', '5,2', '4,7', '4,5', '5', '.', '4,1');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (170, 2017, 7133, '      Bad Kreuznach, Landkreis', '5248', '1264', '258', '134', '567', '1116', '2030', '6,9', '6,2', '6,4', '6,1', '.', '6,4');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (171, 2017, 7134, '      Birkenfeld, Landkreis', '2597', '409', '181', '75', '306', '556', '1074', '6,7', '6', '6,2', '5,8', '.', '6,8');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (172, 2017, 7135, '      Cochem-Zell, Landkreis', '1188', '208', '57', '27', '139', '296', '333', '3,9', '3,6', '3,8', '3,3', '.', '3,4');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (173, 2017, 7137, '      Mayen-Koblenz, Landkreis', '4048', '793', '197', '107', '486', '965', '1010', '3,9', '3,5', '3,6', '3,4', '.', '3,8');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (174, 2017, 7138, '      Neuwied, Landkreis', '5156', '1344', '293', '108', '509', '1152', '1690', '5,9', '5,3', '5,4', '5,2', '.', '4,5');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (175, 2017, 7140, '      Rhein-Hunsr�ck-Kreis', '2162', '411', '66', '49', '235', '540', '300', '4,1', '3,7', '3,8', '3,7', '.', '3,6');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (176, 2017, 7141, '      Rhein-Lahn-Kreis', '2303', '488', '131', '21', '150', '510', '556', '3,9', '3,5', '3,6', '3,4', '.', '2,1');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (177, 2017, 7143, '      Westerwaldkreis', '3709', '788', '240', '73', '348', '850', '818', '3,7', '3,3', '3,4', '3,3', '.', '2,7');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (178, 2017, 72, '    Trier, Stat. Region', '10567', '2244', '744', '290', '1317', '2389', '2858', '.', '.', '.', '.', '.', '.');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (179, 2017, 7211, '      Trier, Kreisfreie Stadt', '2980', '683', '171', '79', '380', '542', '742', '5,5', '5,1', '5,8', '4,4', '.', '4,3');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (180, 2017, 7231, '      Bernkastel-Wittlich, Landkreis', '2200', '468', '173', '64', '265', '596', '525', '3,9', '3,5', '3,6', '3,5', '.', '3,6');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (181, 2017, 7232, '      Eifelkreis Bitburg-Pr�m', '1553', '352', '110', '48', '200', '349', '369', '3,2', '3', '3,2', '2,7', '.', '2,6');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (182, 2017, 7233, '      Vulkaneifel, Landkreis', '1411', '268', '97', '36', '168', '328', '554', '4,7', '4,3', '4,3', '4,2', '.', '4');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (183, 2017, 7235, '      Trier-Saarburg, Landkreis', '2423', '473', '193', '63', '304', '574', '668', '3,3', '3', '3,3', '2,8', '.', '3');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (184, 2017, 73, '    Rheinhessen-Pfalz, Stat. Region', '59750', '16805', '3436', '1361', '6471', '12468', '19918', '.', '.', '.', '.', '.', '.');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (185, 2017, 7311, '      Frankenthal (Pfalz), Kreisfreie Stadt', '1636', '511', '81', '30', '148', '360', '586', '7,7', '7', '6,7', '7,3', '.', '6,3');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (186, 2017, 7312, '      Kaiserslautern, Kreisfreie Stadt', '4932', '1279', '273', '103', '513', '1048', '1970', '10,3', '9,4', '9,3', '9,4', '.', '8,3');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (187, 2017, 7313, '      Landau in der Pfalz, Kreisfreie Stadt', '1322', '360', '67', '31', '158', '255', '441', '5,8', '5,3', '5,9', '4,6', '.', '4,8');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (188, 2017, 7314, '      Ludwigshafen am Rhein, Kreisfreie Stadt', '7348', '3103', '357', '190', '805', '1167', '2437', '9,2', '8,3', '8', '8,7', '.', '8,3');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (189, 2017, 7315, '      Mainz, Kreisfreie Stadt', '7037', '2901', '338', '153', '676', '1189', '2407', '6,6', '6,1', '6,6', '5,5', '.', '5,1');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (190, 2017, 7316, '      Neustadt an der Weinstra�e, Kreisfreie Stadt', '1608', '379', '101', '31', '185', '311', '477', '6,2', '5,6', '6', '5,2', '.', '6,5');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (191, 2017, 7317, '      Pirmasens, Kreisfreie Stadt', '2554', '465', '164', '56', '266', '534', '1157', '13,7', '12,3', '12,8', '11,7', '.', '12,1');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (192, 2017, 7318, '      Speyer, Kreisfreie Stadt', '1534', '427', '102', '34', '148', '308', '588', '6,3', '5,7', '5,9', '5,4', '.', '5,4');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (193, 2017, 7319, '      Worms, Kreisfreie Stadt', '3272', '1104', '96', '88', '378', '522', '1116', '8', '7,3', '7', '7,6', '.', '7,5');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (194, 2017, 7320, '      Zweibr�cken, Kreisfreie Stadt', '1254', '262', '74', '21', '122', '290', '469', '7,7', '6,9', '7,4', '6,4', '.', '6,8');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (195, 2017, 7331, '      Alzey-Worms, Landkreis', '2837', '690', '144', '74', '330', '646', '800', '4,5', '4', '4,1', '4', '.', '4,5');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (196, 2017, 7332, '      Bad D�rkheim, Landkreis', '2783', '559', '220', '58', '285', '706', '863', '4,3', '3,9', '4,1', '3,7', '.', '4');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (197, 2017, 7333, '      Donnersbergkreis', '2077', '375', '141', '41', '212', '459', '678', '5,6', '5', '5', '5', '.', '5');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (198, 2017, 7334, '      Germersheim, Landkreis', '3010', '842', '203', '67', '334', '672', '767', '4,5', '4,1', '4,1', '4,2', '.', '4,2');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (199, 2017, 7335, '      Kaiserslautern, Landkreis', '2894', '556', '188', '73', '369', '620', '994', '5,7', '5,2', '5,4', '5', '.', '6,4');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (200, 2017, 7336, '      Kusel, Landkreis', '1686', '230', '137', '45', '203', '459', '597', '5', '4,5', '4,8', '4,2', '.', '5,4');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (201, 2017, 7337, '      S�dliche Weinstra�e, Landkreis', '2468', '560', '149', '47', '259', '630', '752', '4,5', '4,1', '4,2', '3,9', '.', '4');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (202, 2017, 7338, '      Rhein-Pfalz-Kreis', '3018', '744', '200', '68', '298', '833', '933', '3,9', '3,5', '3,6', '3,3', '.', '3,4');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (203, 2017, 7339, '      Mainz-Bingen, Landkreis', '4168', '1103', '216', '107', '500', '817', '1145', '4', '3,6', '3,8', '3,5', '.', '4,5');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (204, 2017, 7340, '      S�dwestpfalz, Landkreis', '2312', '355', '185', '44', '282', '642', '741', '4,9', '4,5', '4,7', '4,2', '.', '5,4');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (205, 2017, 8, '  Baden-W�rttemberg, Land', '212837', '70447', '15048', '3390', '19506', '46875', '62448', '3,9', '3,5', '3,6', '3,4', '8,4', '2,8');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (206, 2017, 81, '    Stuttgart, Regierungsbezirk', '81317', '29901', '5338', '1278', '7295', '17681', '24604', '.', '.', '.', '.', '.', '.');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (207, 2017, 8111, '      Stuttgart, Landeshauptstadt, Stadtkreis', '15580', '6852', '813', '192', '1066', '2510', '5948', '5,1', '4,7', '4,9', '4,5', '.', '3,1');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (208, 2017, 8115, '      B�blingen, Landkreis', '6513', '2666', '369', '83', '519', '1481', '1649', '3,4', '3,1', '3', '3,1', '.', '2,3');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (209, 2017, 8116, '      Esslingen, Landkreis', '10131', '4044', '569', '166', '939', '2157', '3024', '3,8', '3,4', '3,5', '3,3', '.', '2,9');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (210, 2017, 8117, '      G�ppingen, Landkreis', '5211', '1801', '320', '94', '507', '1130', '1235', '4,1', '3,7', '3,6', '3,9', '.', '3,2');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (211, 2017, 8118, '      Ludwigsburg, Landkreis', '10103', '4032', '774', '190', '980', '2294', '2814', '3,7', '3,3', '3,3', '3,4', '.', '3,1');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (212, 2017, 8119, '      Rems-Murr-Kreis, Landkreis', '7898', '3020', '470', '111', '713', '1725', '2114', '3,7', '3,4', '3,5', '3,2', '.', '2,7');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (213, 2017, 8121, '      Heilbronn, Stadtkreis', '3586', '1564', '249', '62', '373', '680', '897', '5,8', '5,2', '5,3', '5,1', '.', '4,7');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (214, 2017, 8125, '      Heilbronn, Landkreis', '6284', '1953', '505', '93', '641', '1525', '1917', '3,6', '3,2', '3,1', '3,3', '.', '2,8');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (215, 2017, 8126, '      Hohenlohekreis, Landkreis', '1726', '406', '171', '30', '177', '505', '509', '2,9', '2,6', '2,6', '2,6', '.', '2,3');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (216, 2017, 8127, '      Schw�bisch Hall, Landkreis', '3411', '711', '277', '76', '341', '898', '1051', '3,4', '3,1', '3,1', '3,1', '.', '2,6');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (217, 2017, 8128, '      Main-Tauber-Kreis, Landkreis', '2226', '504', '323', '46', '264', '575', '664', '3,3', '2,9', '3', '2,9', '.', '3,2');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (218, 2017, 8135, '      Heidenheim, Landkreis', '3340', '995', '218', '57', '300', '799', '1051', '5,2', '4,7', '4,6', '4,8', '.', '3,6');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (219, 2017, 8136, '      Ostalbkreis, Landkreis', '5308', '1352', '280', '79', '474', '1401', '1731', '3,4', '3,1', '3', '3,1', '.', '2,3');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (220, 2017, 82, '    Karlsruhe, Regierungsbzirk', '59136', '18706', '5156', '873', '5026', '13306', '18139', '.', '.', '.', '.', '.', '.');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (221, 2017, 8211, '      Baden-Baden, Stadtkreis', '1446', '518', '91', '20', '98', '417', '538', '5,7', '5,1', '5', '5,2', '.', '3,5');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (222, 2017, 8212, '      Karlsruhe, Stadtkreis', '7378', '2103', '554', '115', '592', '1417', '2211', '5', '4,5', '4,7', '4,4', '.', '3,2');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (223, 2017, 8215, '      Karlsruhe, Landkreis', '7637', '2184', '587', '156', '854', '1830', '1852', '3,4', '3,1', '3,2', '3', '.', '3,2');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (224, 2017, 8216, '      Rastatt, Landkreis', '3931', '1052', '362', '70', '383', '1050', '1081', '3,3', '3', '2,9', '3,1', '.', '2,7');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (225, 2017, 8221, '      Heidelberg, Stadtkreis', '3169', '968', '287', '28', '185', '583', '957', '4,8', '4,4', '5', '3,7', '.', '2');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (226, 2017, 8222, '      Mannheim, Stadtkreis', '8868', '3458', '734', '60', '453', '1692', '3396', '6', '5,4', '5,5', '5,3', '.', '2,4');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (227, 2017, 8225, '      Neckar-Odenwald-Kreis, Landkreis', '2864', '632', '439', '52', '316', '848', '944', '4', '3,6', '3,5', '3,7', '.', '3,5');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (228, 2017, 8226, '      Rhein-Neckar-Kreis, Landkreis', '11898', '3600', '1254', '183', '1031', '2825', '3787', '4,4', '4', '4,2', '3,8', '.', '3,4');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (229, 2017, 8231, '      Pforzheim, Stadtkreis', '4091', '1866', '245', '57', '333', '684', '1185', '6,9', '6,3', '6,1', '6,4', '.', '4,3');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (230, 2017, 8235, '      Calw, Landkreis', '2857', '843', '178', '39', '257', '740', '748', '3,6', '3,3', '3,3', '3,2', '.', '2,5');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (231, 2017, 8236, '      Enzkreis, Landkreis', '2901', '954', '234', '56', '307', '637', '787', '2,9', '2,6', '2,6', '2,7', '.', '2,4');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (232, 2017, 8237, '      Freudenstadt, Landkreis', '2098', '527', '191', '37', '220', '583', '653', '3,4', '3,1', '2,9', '3,2', '.', '2,6');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (233, 2017, 83, '    Freiburg, Regierungsbezirk', '41267', '11977', '2714', '695', '3925', '9040', '11245', '.', '.', '.', '.', '.', '.');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (234, 2017, 8311, '      Freiburg im Breisgau, Stadtkreis', '6362', '1949', '357', '75', '397', '1074', '2036', '5,8', '5,3', '6', '4,6', '.', '2,6');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (235, 2017, 8315, '      Breisgau-Hochschwarzwald, Landkreis', '4358', '1089', '319', '65', '407', '1039', '1119', '3,3', '3', '3,2', '2,8', '.', '2,3');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (236, 2017, 8316, '      Emmendingen, Landkreis', '2563', '694', '181', '49', '281', '613', '536', '3', '2,7', '2,9', '2,5', '.', '2,5');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (237, 2017, 8317, '      Ortenaukreis, Landkreis', '7777', '2205', '523', '121', '715', '1906', '2712', '3,5', '3,2', '3,1', '3,3', '.', '2,5');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (238, 2017, 8325, '      Rottweil, Landkreis', '2045', '594', '158', '42', '246', '499', '499', '2,9', '2,6', '2,6', '2,5', '.', '2,4');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (239, 2017, 8326, '      Schwarzwald-Baar-Kreis, Landkreis', '3658', '1142', '269', '80', '398', '872', '871', '3,5', '3,1', '3,1', '3,1', '.', '2,8');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (240, 2017, 8327, '      Tuttlingen, Landkreis', '2152', '688', '111', '45', '241', '403', '525', '3', '2,7', '2,7', '2,8', '.', '2,5');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (241, 2017, 8335, '      Konstanz, Landkreis', '5358', '1573', '327', '92', '500', '1148', '1221', '3,8', '3,4', '3,8', '3,1', '.', '2,4');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (242, 2017, 8336, '      L�rrach, Landkreis', '4072', '1059', '304', '61', '390', '871', '979', '3,5', '3,2', '3,6', '2,8', '.', '2,3');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (243, 2017, 8337, '      Waldshut, Landkreis', '2923', '982', '165', '64', '349', '615', '747', '3,4', '3,2', '3,7', '2,6', '.', '2,7');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (244, 2017, 84, '    T�bingen, Regierungsbezirk', '31117', '9864', '1840', '544', '3260', '6848', '8460', '.', '.', '.', '.', '.', '.');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (245, 2017, 8415, '      Reutlingen, Landkreis', '5577', '2092', '290', '88', '519', '1106', '1709', '3,9', '3,5', '3,6', '3,4', '.', '2,9');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (246, 2017, 8416, '      T�bingen, Landkreis', '3561', '1144', '189', '44', '317', '722', '1053', '3,2', '2,9', '3,3', '2,6', '.', '2,1');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (247, 2017, 8417, '      Zollernalbkreis, Landkreis', '3548', '850', '297', '55', '331', '868', '1090', '3,8', '3,4', '3,3', '3,5', '.', '2,7');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (248, 2017, 8421, '      Ulm, Stadtkreis', '2608', '1064', '159', '49', '286', '522', '638', '4,1', '3,8', '4,1', '3,5', '.', '3,5');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (249, 2017, 8425, '      Alb-Donau-Kreis, Landkreis', '2972', '1028', '179', '61', '369', '775', '734', '3', '2,7', '2,7', '2,6', '.', '2,7');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (250, 2017, 8426, '      Biberach, Landkreis', '2721', '860', '175', '63', '365', '585', '693', '2,7', '2,4', '2,4', '2,4', '.', '2,6');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (251, 2017, 8435, '      Bodenseekreis, Landkreis', '3352', '1052', '170', '49', '299', '761', '829', '3,1', '2,8', '2,8', '2,8', '.', '2,2');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (252, 2017, 8436, '      Ravensburg, Landkreis', '4464', '1294', '254', '95', '540', '910', '1142', '3,1', '2,8', '3,1', '2,5', '.', '2,6');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (253, 2017, 8437, '      Sigmaringen, Landkreis', '2313', '478', '127', '39', '234', '599', '572', '3,5', '3,2', '3,1', '3,3', '.', '2,5');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (254, 2017, 9, '  Bayern', '231353', '67249', '21440', '5233', '23106', '54234', '55588', '3,6', '3,2', '3,3', '3', '7,9', '2,8');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (255, 2017, 91, '    Oberbayern, Regierungsbezirk', '80563', '29233', '5925', '1521', '6911', '17484', '19182', '.', '.', '.', '.', '.', '.');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (256, 2017, 9161, '      Ingolstadt', '2466', '838', '184', '76', '295', '506', '582', '3,5', '3,1', '3', '3,3', '.', '3,6');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (257, 2017, 9162, '      M�nchen, Landeshauptstadt', '35718', '16135', '2432', '498', '2102', '6828', '9577', '4,7', '4,2', '4,3', '4,1', '.', '2,7');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (258, 2017, 9163, '      Rosenheim', '1565', '578', '136', '42', '179', '303', '347', '5,1', '4,6', '4,8', '4,4', '.', '4,6');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (259, 2017, 9171, '      Alt�tting, Landkreis', '2041', '510', '168', '51', '231', '496', '506', '3,7', '3,3', '3,4', '3,2', '.', '3,2');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (260, 2017, 9172, '      Berchtesgadener Land, Landkreis', '1993', '557', '170', '31', '175', '513', '507', '4,4', '3,9', '4,4', '3,5', '.', '2,7');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (261, 2017, 9173, '      Bad T�lz-Wolfratshausen, Landkreis', '1672', '512', '114', '38', '196', '423', '248', '2,7', '2,5', '2,7', '2,2', '.', '2,4');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (262, 2017, 9174, '      Dachau, Landkreis', '1949', '694', '165', '53', '229', '452', '324', '2,5', '2,2', '2,3', '2,1', '.', '2,3');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (263, 2017, 9175, '      Ebersberg, Landkreis', '1518', '433', '114', '33', '172', '369', '296', '2,2', '2', '2,1', '1,8', '.', '2,1');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (264, 2017, 9176, '      Eichst�tt, Landkreis', '1079', '302', '84', '35', '160', '237', '117', '1,6', '1,5', '1,5', '1,4', '.', '1,8');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (265, 2017, 9177, '      Erding, Landkreis', '1450', '373', '132', '30', '136', '362', '232', '2', '1,8', '1,9', '1,7', '.', '1,4');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (266, 2017, 9178, '      Freising, Landkreis', '2155', '684', '189', '44', '236', '493', '366', '2,3', '2,1', '2,1', '2', '.', '2');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (267, 2017, 9179, '      F�rstenfeldbruck, Landkreis', '3046', '957', '213', '65', '297', '717', '629', '2,9', '2,6', '2,7', '2,4', '.', '2,4');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (268, 2017, 9180, '      Garmisch-Partenkirchen, Landkreis', '1528', '447', '91', '35', '159', '399', '335', '3,8', '3,4', '3,7', '3,1', '.', '2,9');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (269, 2017, 9181, '      Landsberg am Lech, Landkreis', '1707', '419', '109', '49', '240', '424', '307', '3', '2,7', '2,9', '2,4', '.', '3,1');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (270, 2017, 9182, '      Miesbach, Landkreis', '1354', '327', '108', '24', '122', '353', '297', '2,9', '2,6', '2,8', '2,3', '.', '1,9');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (271, 2017, 9183, '      M�hldorf a.Inn, Landkreis', '2247', '596', '207', '58', '250', '482', '629', '4', '3,6', '3,8', '3,4', '.', '3,2');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (272, 2017, 9184, '      M�nchen, Landkreis', '4782', '1776', '340', '61', '353', '1091', '1321', '3', '2,6', '2,7', '2,5', '.', '2');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (273, 2017, 9185, '      Neuburg-Schrobenhausen, Landkreis', '1168', '339', '95', '33', '158', '255', '191', '2,4', '2,1', '2,1', '2,1', '.', '2,3');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (274, 2017, 9186, '      Pfaffenhofen a.d.Ilm, Landkreis', '1309', '342', '112', '38', '159', '338', '137', '2', '1,8', '1,8', '1,8', '.', '1,9');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (275, 2017, 9187, '      Rosenheim, Landkreis', '3409', '736', '254', '86', '389', '848', '743', '2,8', '2,5', '2,7', '2,2', '.', '2,3');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (276, 2017, 9188, '      Starnberg, Landkreis', '1814', '567', '112', '37', '173', '483', '398', '3,1', '2,8', '3', '2,5', '.', '2,6');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (277, 2017, 9189, '      Traunstein, Landkreis', '2738', '629', '253', '65', '313', '668', '639', '3,3', '2,9', '3,2', '2,5', '.', '2,7');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (278, 2017, 9190, '      Weilheim-Schongau, Landkreis', '1855', '482', '143', '38', '189', '443', '454', '2,9', '2,6', '2,8', '2,3', '.', '2,1');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (279, 2017, 92, '    Niederbayern, Regierungsbezirk', '21192', '4905', '1845', '588', '2451', '5408', '4631', '.', '.', '.', '.', '.', '.');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (280, 2017, 9261, '      Landshut', '1645', '629', '121', '37', '178', '311', '398', '4,7', '4,2', '4,4', '4', '.', '4,1');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (281, 2017, 9262, '      Passau', '1243', '365', '65', '31', '130', '253', '260', '5,1', '4,6', '5,3', '4', '.', '3,6');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (282, 2017, 9263, '      Straubing', '1198', '289', '125', '44', '151', '283', '313', '5,2', '4,6', '4,7', '4,5', '.', '5,2');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (283, 2017, 9271, '      Deggendorf, Landkreis', '1881', '326', '197', '43', '188', '521', '338', '3,1', '2,8', '3', '2,6', '.', '2,2');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (284, 2017, 9272, '      Freyung-Grafenau, Landkreis', '1291', '136', '152', '30', '121', '413', '224', '3,3', '3', '3,6', '2,2', '.', '2,2');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (285, 2017, 9273, '      Kelheim, Landkreis', '1760', '535', '166', '53', '230', '436', '385', '2,9', '2,6', '2,6', '2,5', '.', '2,7');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (286, 2017, 9274, '      Landshut, Landkreis', '2288', '601', '186', '66', '281', '564', '619', '2,8', '2,5', '2,7', '2,4', '.', '2,6');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (287, 2017, 9275, '      Passau, Landkreis', '3406', '602', '255', '95', '378', '926', '704', '3,6', '3,3', '3,5', '2,9', '.', '2,9');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (288, 2017, 9276, '      Regen, Landkreis', '1543', '312', '126', '39', '173', '438', '283', '4,1', '3,6', '4,3', '2,8', '.', '3,3');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (289, 2017, 9277, '      Rottal-Inn, Landkreis', '1970', '441', '160', '57', '225', '494', '562', '3,4', '3', '3,2', '2,8', '.', '2,6');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (290, 2017, 9278, '      Straubing-Bogen, Landkreis', '1403', '240', '140', '41', '166', '378', '261', '2,7', '2,4', '2,7', '2,2', '.', '2,4');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (291, 2017, 9279, '      Dingolfing-Landau, Landkreis', '1564', '430', '152', '53', '229', '393', '284', '2,9', '2,6', '2,7', '2,4', '.', '3');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (292, 2017, 93, '    Oberpfalz, Regierungsbezirk', '17936', '3530', '1918', '430', '1990', '4405', '4109', '.', '.', '.', '.', '.', '.');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (293, 2017, 9361, '      Amberg', '1224', '288', '119', '27', '133', '273', '348', '5,9', '5,3', '5,6', '5', '.', '5,4');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (294, 2017, 9362, '      Regensburg', '2798', '866', '230', '51', '275', '464', '560', '3,6', '3,3', '3,6', '2,9', '.', '2,7');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (295, 2017, 9363, '      Weiden i.d.OPf.', '1426', '323', '133', '40', '168', '307', '430', '6,8', '6,1', '6,5', '5,7', '.', '6,6');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (296, 2017, 9371, '      Amberg-Sulzbach, Landkreis', '1599', '235', '189', '42', '181', '414', '351', '3', '2,7', '2,8', '2,6', '.', '2,6');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (297, 2017, 9372, '      Cham, Landkreis', '1920', '254', '252', '44', '209', '539', '283', '3', '2,7', '3,2', '2,1', '.', '2,3');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (298, 2017, 9373, '      Neumarkt i.d.OPf., Landkreis', '1404', '216', '138', '29', '146', '367', '268', '2,1', '1,9', '2', '1,7', '.', '1,5');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (299, 2017, 9374, '      Neustadt a.d.Waldnaab, Landkreis', '1760', '232', '219', '43', '197', '482', '505', '3,7', '3,3', '3,5', '3', '.', '3,1');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (300, 2017, 9375, '      Regensburg, Landkreis', '2046', '410', '216', '49', '240', '522', '360', '2,1', '1,9', '2', '1,7', '.', '2');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (301, 2017, 9376, '      Schwandorf, Landkreis', '2323', '504', '260', '69', '278', '608', '546', '3,1', '2,8', '3', '2,6', '.', '2,7');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (302, 2017, 9377, '      Tirschenreuth, Landkreis', '1435', '201', '162', '36', '163', '429', '458', '4', '3,5', '3,8', '3,2', '.', '3,3');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (303, 2017, 94, '    Oberfranken, Regierungsbezirk', '21066', '3768', '2423', '527', '2262', '5587', '5129', '.', '.', '.', '.', '.', '.');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (304, 2017, 9461, '      Bamberg', '1765', '500', '139', '42', '195', '340', '446', '4,9', '4,5', '4,8', '4', '.', '4,2');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (305, 2017, 9462, '      Bayreuth', '1960', '391', '286', '43', '184', '425', '501', '5,6', '5', '5,6', '4,4', '.', '3,7');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (306, 2017, 9463, '      Coburg', '1120', '218', '103', '27', '131', '264', '306', '5,6', '5', '5,1', '4,8', '.', '5,5');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (307, 2017, 9464, '      Hof', '1661', '643', '194', '60', '222', '316', '408', '7,9', '7,1', '7,8', '6,3', '.', '8,8');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (308, 2017, 9471, '      Bamberg, Landkreis', '2021', '233', '207', '50', '242', '569', '409', '2,6', '2,3', '2,4', '2,2', '.', '2,4');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (309, 2017, 9472, '      Bayreuth, Landkreis', '1811', '169', '239', '38', '162', '599', '428', '3,4', '3,1', '3,2', '2,8', '.', '2,5');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (310, 2017, 9473, '      Coburg, Landkreis', '1546', '165', '186', '40', '173', '426', '375', '3,5', '3,1', '3', '3,2', '.', '3,4');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (311, 2017, 9474, '      Forchheim, Landkreis', '1796', '341', '195', '36', '174', '504', '389', '3', '2,7', '2,8', '2,6', '.', '2,3');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (312, 2017, 9475, '      Hof, Landkreis', '1562', '264', '149', '39', '170', '451', '342', '3,5', '3,1', '3,5', '2,6', '.', '3,2');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (313, 2017, 9476, '      Kronach, Landkreis', '1179', '109', '170', '23', '107', '387', '242', '3,5', '3,1', '3,2', '2,9', '.', '2,8');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (314, 2017, 9477, '      Kulmbach, Landkreis', '1563', '220', '215', '47', '166', '457', '473', '4,3', '3,8', '4', '3,6', '.', '3,9');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (315, 2017, 9478, '      Lichtenfels, Landkreis', '1342', '146', '176', '36', '150', '402', '338', '3,9', '3,5', '3,4', '3,6', '.', '3,4');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (316, 2017, 9479, '      Wunsiedel i.Fichtelgebirge, Landkreis', '1740', '370', '164', '46', '187', '448', '472', '5,1', '4,5', '4,9', '4,2', '.', '4,9');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (317, 2017, 95, '    Mittelfranken, Regierungsbezirk', '38164', '12258', '4466', '874', '3801', '8344', '10577', '.', '.', '.', '.', '.', '.');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (318, 2017, 9561, '      Ansbach', '974', '292', '95', '17', '105', '213', '231', '4,7', '4,2', '4,3', '4,2', '.', '4,1');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (319, 2017, 9562, '      Erlangen', '2384', '802', '271', '58', '246', '495', '813', '4,3', '3,9', '3,9', '3,8', '.', '3,5');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (320, 2017, 9563, '      F�rth', '3889', '1389', '421', '93', '377', '757', '1363', '6', '5,3', '5,4', '5,3', '.', '4,9');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (321, 2017, 9564, '      N�rnberg', '17096', '6872', '1992', '389', '1625', '3173', '4921', '6,6', '6', '6', '5,9', '.', '5,5');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (322, 2017, 9565, '      Schwabach', '758', '258', '75', '20', '90', '153', '206', '3,9', '3,4', '3,7', '3,1', '.', '4');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (323, 2017, 9571, '      Ansbach, Landkreis', '2848', '603', '335', '71', '307', '796', '760', '3,1', '2,7', '2,7', '2,7', '.', '2,4');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (324, 2017, 9572, '      Erlangen-H�chstadt, Landkreis', '1547', '294', '166', '36', '161', '435', '317', '2,3', '2', '2,2', '1,9', '.', '2,1');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (325, 2017, 9573, '      F�rth, Landkreis', '1781', '303', '260', '29', '134', '501', '526', '3,1', '2,7', '2,9', '2,5', '.', '2');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (326, 2017, 9574, '      N�rnberger Land, Landkreis', '2366', '547', '324', '54', '254', '620', '462', '2,8', '2,5', '2,7', '2,3', '.', '2,6');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (327, 2017, 9575, '      Neustadt a.d.Aisch-Bad Windsheim, Landkreis', '1217', '225', '145', '40', '164', '283', '219', '2,4', '2,2', '2,3', '2', '.', '2,5');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (328, 2017, 9576, '      Roth, Landkreis', '1687', '300', '197', '31', '183', '488', '384', '2,7', '2,4', '2,3', '2,4', '.', '2,2');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (329, 2017, 9577, '      Wei�enburg-Gunzenhausen, Landkreis', '1617', '371', '185', '37', '154', '429', '375', '3,5', '3,1', '3,2', '2,9', '.', '2,4');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (330, 2017, 96, '    Unterfranken, Regierungsbezirk', '22246', '5256', '2107', '561', '2428', '5524', '5301', '.', '.', '.', '.', '.', '.');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (331, 2017, 9661, '      Aschaffenburg', '2002', '738', '164', '57', '213', '354', '506', '5,7', '5,1', '5,1', '5,1', '.', '5,4');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (332, 2017, 9662, '      Schweinfurt', '1726', '574', '123', '36', '163', '350', '548', '7', '6,3', '6', '6,6', '.', '5,6');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (333, 2017, 9663, '      W�rzburg', '2788', '779', '251', '50', '246', '572', '748', '4,2', '3,8', '4,4', '3,3', '.', '2,5');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (334, 2017, 9671, '      Aschaffenburg, Landkreis', '2906', '748', '217', '84', '359', '707', '707', '3,3', '2,9', '2,9', '3', '.', '3,3');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (335, 2017, 9672, '      Bad Kissingen, Landkreis', '1855', '278', '212', '43', '169', '557', '546', '3,7', '3,3', '3,3', '3,2', '.', '2,6');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (336, 2017, 9673, '      Rh�n-Grabfeld, Landkreis', '1174', '174', '140', '28', '114', '362', '311', '2,9', '2,6', '2,8', '2,4', '.', '2,2');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (337, 2017, 9674, '      Ha�berge, Landkreis', '1352', '157', '143', '36', '141', '386', '303', '3,1', '2,7', '2,8', '2,7', '.', '2,4');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (338, 2017, 9675, '      Kitzingen, Landkreis', '1301', '317', '120', '33', '169', '297', '199', '2,9', '2,5', '2,7', '2,3', '.', '2,8');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (339, 2017, 9676, '      Miltenberg, Landkreis', '2084', '577', '178', '65', '255', '504', '379', '3,2', '2,9', '2,9', '2,8', '.', '3,1');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (340, 2017, 9677, '      Main-Spessart, Landkreis', '1449', '247', '172', '37', '167', '440', '266', '2,2', '2', '2,1', '1,8', '.', '2');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (341, 2017, 9678, '      Schweinfurt, Landkreis', '1676', '288', '192', '41', '190', '477', '392', '2,9', '2,5', '2,5', '2,6', '.', '2,5');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (342, 2017, 9679, '      W�rzburg, Landkreis', '1932', '377', '195', '51', '242', '518', '396', '2,3', '2,1', '2,3', '1,8', '.', '2,5');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (343, 2017, 97, '    Schwaben, Regierungsbezirk', '30185', '8300', '2756', '731', '3263', '7482', '6659', '.', '.', '.', '.', '.', '.');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (344, 2017, 9761, '      Augsburg', '8629', '3218', '671', '199', '833', '1738', '2300', '6', '5,4', '5,4', '5,5', '.', '4,5');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (345, 2017, 9762, '      Kaufbeuren', '1108', '265', '133', '37', '122', '262', '382', '5,2', '4,6', '4,7', '4,6', '.', '4,4');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (346, 2017, 9763, '      Kempten (Allg�u)', '1372', '394', '121', '38', '160', '331', '306', '4,2', '3,7', '3,8', '3,6', '.', '3,6');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (347, 2017, 9764, '      Memmingen', '836', '268', '68', '20', '96', '228', '192', '3,9', '3,5', '3,4', '3,6', '.', '3,6');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (348, 2017, 9771, '      Aichach-Friedberg, Landkreis', '1650', '369', '165', '38', '185', '442', '292', '2,5', '2,2', '2,3', '2', '.', '2');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (349, 2017, 9772, '      Augsburg, Landkreis', '3485', '798', '320', '94', '430', '882', '576', '2,8', '2,5', '2,6', '2,5', '.', '2,7');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (350, 2017, 9773, '      Dillingen a.d.Donau, Landkreis', '1130', '263', '128', '21', '116', '341', '217', '2,3', '2,1', '2', '2,2', '.', '1,8');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (351, 2017, 9774, '      G�nzburg, Landkreis', '1573', '432', '202', '36', '173', '418', '357', '2,5', '2,2', '2,1', '2,3', '.', '2');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (352, 2017, 9775, '      Neu-Ulm, Landkreis', '2252', '583', '216', '39', '217', '581', '363', '2,6', '2,3', '2,4', '2,2', '.', '2');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (353, 2017, 9776, '      Lindau (Bodensee), Landkreis', '1061', '310', '85', '28', '112', '257', '188', '2,7', '2,4', '2,5', '2,4', '.', '2,1');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (354, 2017, 9777, '      Ostallg�u, Landkreis', '1843', '378', '146', '46', '210', '501', '383', '2,7', '2,4', '2,4', '2,5', '.', '2,1');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (355, 2017, 9778, '      Unterallg�u, Landkreis', '1515', '311', '142', '45', '192', '438', '304', '2,2', '1,9', '1,9', '2', '.', '1,8');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (356, 2017, 9779, '      Donau-Ries, Landkreis', '1480', '261', '162', '30', '152', '432', '327', '2,2', '1,9', '1,8', '2,1', '.', '1,6');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (357, 2017, 9780, '      Oberallg�u, Landkreis', '2250', '450', '197', '63', '264', '630', '472', '3', '2,6', '2,8', '2,5', '.', '2,5');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (358, 2017, 10, '  Saarland', '34672', '9239', '1379', '597', '2841', '6846', '8526', '7,2', '6,7', '7,1', '6,2', '21,1', '5,3');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (359, 2017, 10041, '      Saarbr�cken, Regionalverband', '15695', '4664', '342', '276', '1283', '2676', '2055', '10', '9,3', '10', '8,5', '.', '7,1');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (360, 2017, 10041100, '      Saarbr�cken, Landeshauptstadt', '10264', '3222', '472', '152', '740', '1713', '3778', '.', '.', '.', '.', '.', '.');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (361, 2017, 10042, '      Merzig-Wadern, Landkreis', '2574', '521', '122', '58', '256', '633', '842', '5,1', '4,7', '5,2', '4,3', '.', '4,1');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (362, 2017, 10043, '      Neunkirchen, Landkreis', '5396', '1385', '269', '134', '599', '1021', '1918', '8,2', '7,6', '8,1', '7,1', '.', '8,5');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (363, 2017, 10044, '      Saarlouis, Landkreis', '5361', '1309', '294', '52', '286', '1089', '1907', '5,6', '5,2', '5,5', '4,9', '.', '2,8');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (364, 2017, 10045, '      Saarpfalz-Kreis', '3976', '1036', '245', '62', '318', '1012', '1339', '5,7', '5,3', '5,5', '5', '.', '4,3');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (365, 2017, 10046, '      St. Wendel, Landkreis', '1669', '325', '107', '15', '99', '414', '465', '3,8', '3,5', '3,6', '3,4', '.', '2,1');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (366, 2017, 11000, '  Berlin', '168991', '51150', '8417', '3488', '13771', '27754', '48771', '10,5', '9', '9,7', '8,2', '18,5', '9,6');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (367, 2017, 11001001, '      Berlin-Mitte', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (368, 2017, 11002002, '      Berlin-Friedrichshain-Kreuzberg', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (369, 2017, 11003003, '      Berlin-Pankow', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (370, 2017, 11004004, '      Berlin-Charlottenburg-Wilmersdorf', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (371, 2017, 11005005, '      Berlin-Spandau', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (372, 2017, 11006006, '      Berlin-Steglitz-Zehlendorf', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (373, 2017, 11007007, '      Berlin-Tempelhof-Sch�neberg', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (374, 2017, 11008008, '      Berlin-Neuk�lln', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (375, 2017, 11009009, '      Berlin-Treptow-K�penick', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (376, 2017, 11010010, '      Berlin-Marzahn-Hellersdorf', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (377, 2017, 11011011, '      Berlin-Lichtenberg', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (378, 2017, 11012012, '      Berlin-Reinickendorf', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (379, 2017, 12, '  Brandenburg', '92648', '8448', '4982', '1463', '6254', '23955', '38576', '7,8', '7', '7,5', '6,4', '21,8', '7,9');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (380, 2017, 12051, '      Brandenburg an der Havel, Kreisfreie Stadt', '3437', '340', '169', '59', '284', '770', '1546', '10,3', '9,3', '10,5', '7,9', '.', '12');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (381, 2017, 12052, '      Cottbus, Kreisfreie Stadt', '4574', '654', '257', '52', '241', '994', '1726', '9,8', '8,8', '9,8', '7,8', '.', '6,8');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (382, 2017, 12053, '      Frankfurt (Oder), Kreisfreie Stadt', '2656', '340', '156', '32', '171', '598', '1072', '9,9', '8,9', '9,8', '8', '.', '10');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (383, 2017, 12054, '      Potsdam, Kreisfreie Stadt', '5380', '963', '269', '63', '370', '988', '1761', '6,6', '6', '6,9', '5', '.', '6,1');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (384, 2017, 12060, '      Barnim, Landkreis', '6094', '537', '435', '111', '489', '1617', '2401', '7,1', '6,3', '7,1', '5,5', '.', '9');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (385, 2017, 12061, '      Dahme-Spreewald, Landkreis', '4206', '363', '255', '30', '125', '1198', '1468', '5,3', '4,7', '5,1', '4,4', '.', '2,2');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (386, 2017, 12062, '      Elbe-Elster, Landkreis', '4443', '204', '224', '36', '172', '1516', '2089', '9,1', '8,1', '8,3', '7,9', '.', '5,7');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (387, 2017, 12063, '      Havelland, Landkreis', '5457', '476', '349', '115', '476', '1444', '2365', '7', '6,4', '6,8', '5,9', '.', '8,3');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (388, 2017, 12064, '      M�rkisch-Oderland, Landkreis', '6713', '402', '337', '131', '476', '1936', '2165', '7,3', '6,6', '7,3', '5,9', '.', '8,6');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (389, 2017, 12065, '      Oberhavel, Landkreis', '6968', '682', '417', '103', '512', '1586', '3133', '6,9', '6,2', '6,6', '5,8', '.', '7,5');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (390, 2017, 12066, '      Oberspreewald-Lausitz, Landkreis', '5435', '318', '290', '54', '212', '1474', '2476', '10,3', '9,2', '9,3', '9', '.', '6,3');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (391, 2017, 12067, '      Oder-Spree, Landkreis', '6822', '724', '140', '143', '545', '1922', '3321', '8,1', '7,3', '7,7', '6,8', '.', '10,7');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (392, 2017, 12068, '      Ostprignitz-Ruppin, Landkreis', '3998', '261', '230', '74', '324', '933', '1347', '8,5', '7,7', '8', '7,5', '.', '10,6');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (393, 2017, 12069, '      Potsdam-Mittelmark, Landkreis', '5527', '592', '289', '102', '384', '1594', '2159', '5,4', '4,9', '5,4', '4,3', '.', '5,6');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (394, 2017, 12070, '      Prignitz, Landkreis', '3648', '267', '195', '56', '273', '959', '1543', '10,1', '9,1', '9,5', '8,6', '.', '10,8');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (395, 2017, 12071, '      Spree-Nei�e, Landkreis', '4561', '321', '268', '36', '190', '1266', '1774', '8,2', '7,4', '7,7', '7', '.', '6,1');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (396, 2017, 12072, '      Teltow-Fl�ming, Landkreis', '5033', '453', '230', '96', '388', '1223', '2005', '6,2', '5,6', '5,9', '5,2', '.', '6,5');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (397, 2017, 12073, '      Uckermark, Landkreis', '7697', '553', '472', '168', '622', '1938', '4225', '13,6', '12,4', '12,5', '12,3', '.', '16,6');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (398, 2017, 13, '  Mecklenburg-Vorpommern', '70982', '5478', '4441', '1442', '5879', '17372', '24946', '9,4', '8,6', '9,4', '7,7', '21,7', '10,2');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (399, 2017, 13001, '      Kreisfreie Stadt Greifswald, Hansestadt', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (400, 2017, 13002, '      Kreisfreie Stadt Neubrandenburg, Stadt', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (401, 2017, 13003, '      Kreisfreie Stadt Rostock, Hansestadt', '9298', '1144', '452', '186', '874', '1712', '2978', '9,5', '8,7', '9,9', '7,4', '.', '9');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (402, 2017, 13004, '      Kreisfreie Stadt Schwerin, Landeshauptstadt', '4430', '731', '261', '121', '487', '809', '1350', '9,9', '9,1', '10,2', '8,1', '.', '14,3');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (403, 2017, 13005, '      Kreisfreie Stadt Stralsund, Hansestadt', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (404, 2017, 13006, '      Kreisfreie Stadt Wismar, Hansestadt', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (405, 2017, 13051, '      Landkreis Bad Doberan', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (406, 2017, 13052, '      Landkreis Demmin', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (407, 2017, 13053, '      Landkreis G�strow', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (408, 2017, 13054, '      Landkreis Ludwigslust', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (409, 2017, 13055, '      Landkreis Mecklenburg-Strelitz', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (410, 2017, 13056, '      Landkreis M�ritz', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (411, 2017, 13057, '      Landkreis Nordvorpommern', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (412, 2017, 13058, '      Landkreis Nordwestmecklenburg', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (413, 2017, 13059, '      Landkreis Ostvorpommern', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (414, 2017, 13060, '      Landkreis Parchim', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (415, 2017, 13061, '      Landkreis R�gen', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (416, 2017, 13062, '      Landkreis Uecker-Randow', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (417, 2017, 13071, '      Landkreis Mecklenburgische Seenplatte', '14296', '841', '1192', '253', '1057', '3794', '6188', '11,6', '10,6', '11,5', '9,7', '.', '12,5');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (418, 2017, 13072, '      Landkreis Rostock', '7210', '453', '312', '144', '582', '1918', '2563', '7,2', '6,6', '7,2', '5,9', '.', '8,7');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (419, 2017, 13073, '      Landkreis Vorpommern-R�gen', '11194', '744', '595', '220', '828', '2815', '3074', '10,7', '9,9', '10,8', '8,9', '.', '10,6');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (420, 2017, 13074, '      Landkreis Nordwestmecklenburg', '5695', '449', '354', '127', '489', '1465', '2043', '7,5', '6,9', '7,3', '6,3', '.', '8,1');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (421, 2017, 13075, '      Landkreis Vorpommern-Greifswald', '11958', '702', '758', '228', '966', '3045', '4482', '11,1', '10,2', '11,3', '9', '.', '12');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (422, 2017, 13076, '      Landkreis Ludwigslust-Parchim', '6902', '415', '517', '163', '595', '1814', '2268', '6,6', '6', '6,3', '5,7', '.', '8');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (423, 2017, 14, '  Sachsen', '140348', '15184', '8586', '2440', '10333', '37109', '52720', '7,4', '6,7', '7,1', '6,2', '23', '7,1');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (424, 2017, 141, '    Chemnitz, Regierungsbezirk', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (425, 2017, 14161, '      Chemnitz, Kreisfreie Stadt', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (426, 2017, 14166, '      Plauen, Kreisfreie Stadt', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (427, 2017, 14167, '      Zwickau, Kreisfreie Stadt', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (428, 2017, 14171, '      Annaberg, Landkreis', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (429, 2017, 14173, '      Chemnitzer Land, Landkreis', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (430, 2017, 14177, '      Freiberg, Landkreis', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (431, 2017, 14178, '      Vogtlandkreis', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (432, 2017, 14181, '      Mittlerer Erzgebirgskreis', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (433, 2017, 14182, '      Mittweida, Landkreis', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (434, 2017, 14188, '      Stollberg, Landkreis', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (435, 2017, 14191, '      Aue-Schwarzenberg, Landkreis', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (436, 2017, 14193, '      Zwickauer Land, Landkreis', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (437, 2017, 142, '    Dresden, Regierungsbezirk', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (438, 2017, 14262, '      Dresden, Kreisfreie Stadt', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (439, 2017, 14263, '      G�rlitz, Kreisfreie Stadt', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (440, 2017, 14264, '      Hoyerswerda, Kreisfreie Stadt', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (441, 2017, 14272, '      Bautzen, Landkreis', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (442, 2017, 14280, '      Mei�en, Landkreis', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (443, 2017, 14284, '      Niederschlesischer Oberlausitzkreis', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (444, 2017, 14285, '      Riesa-Gro�enhain, Landkreis', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (445, 2017, 14286, '      L�bau-Zittau, Landkreis', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (446, 2017, 14287, '      S�chsische Schweiz, Landkreis', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (447, 2017, 14290, '      Wei�eritzkreis', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (448, 2017, 14292, '      Kamenz, Landkreis', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (449, 2017, 143, '    Leipzig, Regierungsbezirk', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (450, 2017, 14365, '      Leipzig, Kreisfreie Stadt', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (451, 2017, 14374, '      Delitzsch, Landkreis', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (452, 2017, 14375, '      D�beln, Landkreis', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (453, 2017, 14379, '      Leipziger Land, Landkreis', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (454, 2017, 14383, '      Muldentalkreis', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (455, 2017, 14389, '      Torgau-Oschatz, Landkreis', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (456, 2017, 145, '    Chemnitz, Stat. Region', '45079', '4093', '3340', '823', '3422', '12981', '16879', '.', '.', '.', '.', '.', '.');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (457, 2017, 14511, '      Chemnitz, Stadt', '9353', '1474', '718', '205', '805', '2188', '3405', '8,4', '7,6', '8,1', '7', '.', '8,9');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (458, 2017, 14521, '      Erzgebirgskreis', '9632', '548', '715', '179', '691', '2922', '3625', '6', '5,4', '5,6', '5,3', '.', '5,9');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (459, 2017, 14522, '      Mittelsachsen, Landkreis', '9542', '607', '687', '151', '668', '2972', '4006', '6,5', '5,9', '6', '5,7', '.', '6,3');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (460, 2017, 14523, '      Vogtlandkreis', '6855', '710', '452', '134', '566', '1994', '2088', '6,6', '5,9', '6,1', '5,6', '.', '7,6');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (461, 2017, 14524, '      Zwickau, Landkreis', '9697', '754', '768', '154', '692', '2905', '3755', '6,4', '5,8', '5,9', '5,7', '.', '6,1');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (462, 2017, 146, '    Dresden, Stat. Region', '55888', '5654', '3281', '861', '3722', '15319', '22448', '.', '.', '.', '.', '.', '.');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (463, 2017, 14612, '      Dresden, Stadt', '19074', '3169', '1068', '344', '1522', '3902', '6821', '7,3', '6,6', '7,2', '5,9', '.', '6,4');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (464, 2017, 14625, '      Bautzen, Landkreis', '9707', '486', '649', '136', '546', '3016', '4494', '6,8', '6,2', '6,4', '6', '.', '5,8');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (465, 2017, 14626, '      G�rlitz, Landkreis', '11676', '934', '625', '166', '704', '3748', '5317', '10,1', '9,2', '9,8', '8,6', '.', '8,6');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (466, 2017, 14627, '      Mei�en, Landkreis', '8181', '539', '549', '89', '432', '2548', '3024', '7,2', '6,5', '6,7', '6,3', '.', '5,9');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (467, 2017, 14628, '      S�chsische Schweiz-Osterzgebirge, Landkreis', '7250', '526', '390', '126', '518', '2105', '2792', '6,3', '5,7', '6,2', '5,1', '.', '6,6');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (468, 2017, 147, '    Leipzig, Stat. Region', '39381', '5437', '1965', '754', '3189', '8808', '13393', '.', '.', '.', '.', '.', '.');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (469, 2017, 14713, '      Leipzig, Stadt', '22946', '4334', '1078', '453', '1995', '4047', '6797', '8,5', '7,7', '8,4', '6,9', '.', '8,2');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (470, 2017, 14729, '      Leipzig, Landkreis', '8149', '477', '484', '140', '582', '2220', '2948', '6,7', '6,1', '6,6', '5,5', '.', '7,4');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (471, 2017, 14730, '      Nordsachsen, Landkreis', '8286', '626', '403', '161', '612', '2541', '3648', '8,6', '7,8', '8,1', '7,4', '.', '9,6');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (472, 2017, 15, '  Sachsen-Anhalt', '96960', '9078', '4037', '1728', '7298', '22849', '36999', '9,2', '8,4', '8,7', '8,1', '26,1', '9,5');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (473, 2017, 15001, '      Dessau-Ro�lau, Kreisfreie Stadt', '3631', '412', '197', '56', '263', '914', '1509', '9,6', '8,8', '9,6', '8', '.', '10,6');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (474, 2017, 15002, '      Halle (Saale), Kreisfreie Stadt', '11530', '2488', '482', '233', '1050', '2045', '4013', '10,7', '9,9', '11', '8,7', '.', '10,5');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (475, 2017, 15003, '      Magdeburg, Kreisfreie Stadt', '11419', '2021', '379', '210', '1032', '2144', '3883', '10,1', '9,3', '9,9', '8,6', '.', '10,4');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (476, 2017, 15081, '      Altmarkkreis Salzwedel', '3616', '233', '156', '68', '261', '913', '1411', '8,4', '7,8', '8', '7,5', '.', '8,4');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (477, 2017, 15082, '      Anhalt-Bitterfeld, Landkreis', '6790', '405', '307', '161', '568', '1729', '2722', '8,7', '8,1', '8,1', '8', '.', '11');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (478, 2017, 15083, '      B�rde, Landkreis', '5666', '285', '282', '103', '427', '1514', '2208', '6,5', '5,9', '6,2', '5,6', '.', '7,4');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (479, 2017, 15084, '      Burgenlandkreis', '7652', '549', '310', '169', '640', '1867', '2933', '8,8', '8,1', '8,1', '8,1', '.', '10,5');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (480, 2017, 15085, '      Harz, Landkreis', '7023', '163', '247', '101', '460', '1708', '2165', '6,9', '6,4', '6,7', '6,1', '.', '6,5');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (481, 2017, 15086, '      Jerichower Land, Landkreis', '3729', '257', '146', '60', '249', '1093', '1451', '8,5', '7,8', '7,9', '7,6', '.', '9');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (482, 2017, 15087, '      Mansfeld-S�dharz, Landkreis', '7716', '419', '294', '139', '548', '2146', '3776', '12,1', '11,1', '11,1', '11,2', '.', '12,7');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (483, 2017, 15088, '      Saalekreis', '7597', '632', '373', '134', '593', '1751', '3133', '8,3', '7,6', '7,9', '7,3', '.', '9,7');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (484, 2017, 15089, '      Salzlandkreis', '9497', '456', '361', '107', '463', '2225', '3401', '10,6', '9,8', '9,9', '9,6', '.', '7,7');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (485, 2017, 15090, '      Stendal, Landkreis', '5850', '398', '208', '100', '406', '1377', '2289', '11', '10,1', '10,2', '10', '.', '11,3');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (486, 2017, 15091, '      Wittenberg, Landkreis', '5244', '359', '295', '86', '338', '1425', '2105', '8,6', '7,9', '7,9', '7,9', '.', '8,1');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (487, 2017, 151, '    Dessau, Stat. Region', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (488, 2017, 15101, '      Dessau, Kreisfreie Stadt', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (489, 2017, 15151, '      Anhalt-Zerbst, Kreis', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (490, 2017, 15153, '      Bernburg, Kreis', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (491, 2017, 15154, '      Bitterfeld, Kreis', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (492, 2017, 15159, '      K�then, Kreis', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (493, 2017, 15171, '      Wittenberg, Kreis', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (494, 2017, 152, '    Halle, Stat. Region', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (495, 2017, 15202, '      Halle (Saale), Kreisfreie Stadt', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (496, 2017, 15256, '      Burgenlandkreis', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (497, 2017, 15260, '      Mansfelder Land, Kreis', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (498, 2017, 15261, '      Merseburg-Querfurt, Kreis', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (499, 2017, 15265, '      Saalkreis', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (500, 2017, 15266, '      Sangerhausen, Kreis', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (501, 2017, 15268, '      Wei�enfels, Kreis', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (502, 2017, 153, '    Magdeburg, Stat. Region', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (503, 2017, 15303, '      Magdeburg, Kreisfreie Stadt', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (504, 2017, 15352, '      Aschersleben-Sta�furt, Kreis', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (505, 2017, 15355, '      B�rdekreis', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (506, 2017, 15357, '      Halberstadt, Kreis', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (507, 2017, 15358, '      Jerichower Land, Kreis', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (508, 2017, 15362, '      Ohrekreis', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (509, 2017, 15363, '      Stendal, Kreis', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (510, 2017, 15364, '      Quedlinburg, Kreis', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (511, 2017, 15367, '      Sch�nebeck, Kreis', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (512, 2017, 15369, '      Wernigerode, Kreis', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (513, 2017, 15370, '      Altmarkkreis Salzwedel, Kreis', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (514, 2017, 16, '  Th�ringen', '68614', '6740', '5144', '1339', '5200', '18821', '23871', '6,6', '6,1', '6,3', '5,7', '21,1', '6,7');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (515, 2017, 16051, '      Erfurt, krsfr. Stadt', '7448', '1331', '538', '114', '470', '1678', '2336', '7,4', '6,8', '7,3', '6,2', '.', '5,7');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (516, 2017, 16052, '      Gera, krsfr. Stadt', '4616', '678', '290', '117', '470', '1116', '1746', '10,7', '9,8', '10,6', '8,9', '.', '16,1');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (517, 2017, 16053, '      Jena, krsfr. Stadt', '3302', '669', '150', '70', '292', '640', '1169', '6,6', '6,1', '6,7', '5,4', '.', '6,2');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (518, 2017, 16054, '      Suhl, krsfr. Stadt', '955', '68', '77', '26', '80', '274', '315', '5,8', '5,3', '5,6', '5', '.', '6,6');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (519, 2017, 16055, '      Weimar, krsfr. Stadt', '2032', '255', '114', '36', '154', '397', '654', '7,1', '6,5', '7,2', '5,8', '.', '6');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (520, 2017, 16056, '      Eisenach, krsfr. Stadt', '1556', '225', '187', '38', '146', '337', '465', '7,8', '7,1', '7,1', '7,1', '.', '9,1');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (521, 2017, 16061, '      Eichsfeld, Kreis', '2364', '209', '238', '38', '162', '786', '759', '4,8', '4,3', '4,3', '4,4', '.', '4');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (522, 2017, 16062, '      Nordhausen, Kreis', '3237', '265', '212', '62', '249', '725', '1078', '8,2', '7,5', '7,4', '7,6', '.', '8,3');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (523, 2017, 16063, '      Wartburgkreis', '3005', '184', '211', '64', '237', '913', '1137', '4,8', '4,4', '4,4', '4,3', '.', '5,1');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (524, 2017, 16064, '      Unstrut-Hainich-Kreis', '4577', '326', '359', '97', '379', '1385', '1842', '9', '8,2', '8,3', '8,1', '.', '9,8');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (525, 2017, 16065, '      Kyffh�userkreis', '3398', '157', '253', '43', '194', '970', '1366', '9,5', '8,7', '8,7', '8,6', '.', '7,7');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (526, 2017, 16066, '      Schmalkalden-Meiningen, Kreis', '3013', '281', '263', '73', '260', '815', '915', '4,9', '4,5', '4,7', '4,2', '.', '5,8');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (527, 2017, 16067, '      Gotha, Kreis', '4181', '427', '430', '94', '342', '1212', '1336', '6,3', '5,7', '5,7', '5,7', '.', '6,8');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (528, 2017, 16068, '      S�mmerda, Kreis', '2311', '93', '202', '48', '182', '724', '766', '6,7', '6,1', '6,5', '5,6', '.', '7,7');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (529, 2017, 16069, '      Hildburghausen, Kreis', '1402', '117', '171', '30', '110', '427', '435', '4,3', '3,9', '4,3', '3,5', '.', '4,7');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (530, 2017, 16070, '      Ilm-Kreis', '3310', '285', '269', '66', '242', '898', '1098', '6,4', '5,8', '6,1', '5,5', '.', '6,5');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (531, 2017, 16071, '      Weimarer Land, Kreis', '2018', '143', '124', '29', '129', '584', '588', '5', '4,5', '5,1', '3,9', '.', '4,5');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (532, 2017, 16072, '      Sonneberg, Kreis', '1072', '87', '109', '22', '82', '308', '242', '3,9', '3,6', '3,8', '3,4', '.', '4,2');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (533, 2017, 16073, '      Saalfeld-Rudolstadt, Kreis', '3397', '230', '226', '67', '239', '1062', '1233', '6,5', '5,9', '6,4', '5,4', '.', '6,8');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (534, 2017, 16074, '      Saale-Holzland-Kreis', '2239', '83', '150', '39', '151', '671', '788', '5,5', '5', '5,4', '4,5', '.', '5,4');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (535, 2017, 16075, '      Saale-Orla-Kreis', '2314', '152', '155', '37', '150', '802', '771', '5,7', '5,2', '5,4', '5', '.', '5,1');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (536, 2017, 16076, '      Greiz, Kreis', '2874', '144', '202', '47', '177', '947', '1102', '6,2', '5,7', '6', '5,3', '.', '5,9');
INSERT INTO kriminal."Arbeitslosigkeit" VALUES (537, 2017, 16077, '      Altenburger Land, Kreis', '3991', '329', '214', '82', '306', '1151', '1730', '9,5', '8,6', '8,6', '8,5', '.', '11');


--
-- TOC entry 4294 (class 0 OID 0)
-- Dependencies: 225
-- Name: Arbeitslosigkeit_id_seq; Type: SEQUENCE SET; Schema: kriminal; Owner: postgres
--

SELECT pg_catalog.setval('kriminal."Arbeitslosigkeit_id_seq"', 537, true);


--
-- TOC entry 4157 (class 2606 OID 332921)
-- Name: Arbeitslosigkeit Arbeitslosigkeit_pkey; Type: CONSTRAINT; Schema: kriminal; Owner: postgres
--

ALTER TABLE ONLY kriminal."Arbeitslosigkeit"
    ADD CONSTRAINT "Arbeitslosigkeit_pkey" PRIMARY KEY (id);


-- Completed on 2019-08-14 15:41:30

--
-- PostgreSQL database dump complete
--

