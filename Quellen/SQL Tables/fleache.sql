--
-- PostgreSQL database dump
--

-- Dumped from database version 11.4
-- Dumped by pg_dump version 11.4

-- Started on 2019-08-14 16:54:04

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'LATIN1';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 226 (class 1259 OID 332950)
-- Name: Flaeche_Kreise; Type: TABLE; Schema: kriminal; Owner: postgres
--

CREATE TABLE kriminal."Flaeche_Kreise" (
    id integer NOT NULL,
    "Jahr" character varying,
    "GS" integer,
    "Gebiet" character varying,
    "Flaeche" character varying
);


ALTER TABLE kriminal."Flaeche_Kreise" OWNER TO postgres;

--
-- TOC entry 225 (class 1259 OID 332948)
-- Name: Flaeche_Kreise_id_seq; Type: SEQUENCE; Schema: kriminal; Owner: postgres
--

CREATE SEQUENCE kriminal."Flaeche_Kreise_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE kriminal."Flaeche_Kreise_id_seq" OWNER TO postgres;

--
-- TOC entry 4293 (class 0 OID 0)
-- Dependencies: 225
-- Name: Flaeche_Kreise_id_seq; Type: SEQUENCE OWNED BY; Schema: kriminal; Owner: postgres
--

ALTER SEQUENCE kriminal."Flaeche_Kreise_id_seq" OWNED BY kriminal."Flaeche_Kreise".id;


--
-- TOC entry 4155 (class 2604 OID 332953)
-- Name: Flaeche_Kreise id; Type: DEFAULT; Schema: kriminal; Owner: postgres
--

ALTER TABLE ONLY kriminal."Flaeche_Kreise" ALTER COLUMN id SET DEFAULT nextval('kriminal."Flaeche_Kreise_id_seq"'::regclass);


--
-- TOC entry 4287 (class 0 OID 332950)
-- Dependencies: 226
-- Data for Name: Flaeche_Kreise; Type: TABLE DATA; Schema: kriminal; Owner: postgres
--

INSERT INTO kriminal."Flaeche_Kreise" VALUES (1, '31.12.2017', 1, '  Schleswig-Holstein', '15804,2');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (2, '31.12.2017', 1001, '      Flensburg, Kreisfreie Stadt', '56,73');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (3, '31.12.2017', 1002, '      Kiel, Landeshauptstadt, Kreisfreie Stadt', '118,65');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (4, '31.12.2017', 1003, '      L�beck, Hansestadt, Kreisfreie Stadt', '214,19');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (5, '31.12.2017', 1004, '      Neum�nster, Kreisfreie Stadt', '71,66');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (6, '31.12.2017', 1051, '      Dithmarschen, Landkreis', '1428,17');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (7, '31.12.2017', 1053, '      Herzogtum Lauenburg, Landkreis', '1263,07');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (8, '31.12.2017', 1054, '      Nordfriesland, Landkreis', '2083,46');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (9, '31.12.2017', 1055, '      Ostholstein, Landkreis', '1393,02');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (10, '31.12.2017', 1056, '      Pinneberg, Landkreis', '664,25');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (11, '31.12.2017', 1057, '      Pl�n, Landkreis', '1083,56');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (12, '31.12.2017', 1058, '      Rendsburg-Eckernf�rde, Landkreis', '2189,79');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (13, '31.12.2017', 1059, '      Schleswig-Flensburg, Landkreis', '2071,28');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (14, '31.12.2017', 1060, '      Segeberg, Landkreis', '1344,47');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (15, '31.12.2017', 1061, '      Steinburg, Landkreis', '1055,7');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (16, '31.12.2017', 1062, '      Stormarn, Landkreis', '766,21');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (17, '31.12.2017', 2000, '  Hamburg', '755,09');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (18, '31.12.2017', 3, '  Niedersachsen', '47709,82');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (19, '31.12.2017', 31, '    Braunschweig, Stat. Region', '8117,36');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (20, '31.12.2017', 3101, '      Braunschweig, Kreisfreie Stadt', '192,7');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (21, '31.12.2017', 3102, '      Salzgitter, Kreisfreie Stadt', '224,49');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (22, '31.12.2017', 3103, '      Wolfsburg, Kreisfreie Stadt', '204,61');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (23, '31.12.2017', 3151, '      Gifhorn, Landkreis', '1567,58');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (24, '31.12.2017', 3152, '      G�ttingen, Landkreis', '-');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (25, '31.12.2017', 3153, '      Goslar, Landkreis', '966,72');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (26, '31.12.2017', 3154, '      Helmstedt, Landkreis', '676,15');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (27, '31.12.2017', 3155, '      Northeim, Landkreis', '1268,77');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (28, '31.12.2017', 3156, '      Osterode am Harz, Landkreis', '-');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (29, '31.12.2017', 3157, '      Peine, Landkreis', '536,5');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (30, '31.12.2017', 3158, '      Wolfenb�ttel, Landkreis', '724,3');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (31, '31.12.2017', 3159, '      G�ttingen, Landkreis', '1755,55');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (32, '31.12.2017', 32, '    Hannover, Stat. Region', '9064,74');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (33, '31.12.2017', 3241, '      Region Hannover, Landkreis', '2297,13');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (34, '31.12.2017', 3241001, '      Hannover, Landeshauptstadt', '204,3');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (35, '31.12.2017', 3251, '      Diepholz, Landkreis', '1991');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (36, '31.12.2017', 3252, '      Hameln-Pyrmont, Landkreis', '797,54');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (37, '31.12.2017', 3254, '      Hildesheim, Landkreis', '1208,33');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (38, '31.12.2017', 3255, '      Holzminden, Landkreis', '694,24');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (39, '31.12.2017', 3256, '      Nienburg (Weser), Landkreis', '1400,82');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (40, '31.12.2017', 3257, '      Schaumburg, Landkreis', '675,68');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (41, '31.12.2017', 33, '    L�neburg, Stat. Region', '15541,46');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (42, '31.12.2017', 3351, '      Celle, Landkreis', '1550,82');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (43, '31.12.2017', 3352, '      Cuxhaven, Landkreis', '2058,97');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (44, '31.12.2017', 3353, '      Harburg, Landkreis', '1248,45');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (45, '31.12.2017', 3354, '      L�chow-Dannenberg, Landkreis', '1227,22');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (46, '31.12.2017', 3355, '      L�neburg, Landkreis', '1327,79');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (47, '31.12.2017', 3356, '      Osterholz, Landkreis', '652,67');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (48, '31.12.2017', 3357, '      Rotenburg (W�mme), Landkreis', '2074,78');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (49, '31.12.2017', 3358, '      Heidekreis, Landkreis', '1881,45');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (50, '31.12.2017', 3359, '      Stade, Landkreis', '1267,38');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (51, '31.12.2017', 3360, '      Uelzen, Landkreis', '1462,59');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (52, '31.12.2017', 3361, '      Verden, Landkreis', '789,33');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (53, '31.12.2017', 34, '    Weser-Ems, Stat. Region', '14986,27');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (54, '31.12.2017', 3401, '      Delmenhorst, Kreisfreie Stadt', '62,45');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (55, '31.12.2017', 3402, '      Emden, Kreisfreie Stadt', '112,34');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (56, '31.12.2017', 3403, '      Oldenburg (Oldenburg), Kreisfreie Stadt', '103,09');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (57, '31.12.2017', 3404, '      Osnabr�ck, Kreisfreie Stadt', '119,8');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (58, '31.12.2017', 3405, '      Wilhelmshaven, Kreisfreie Stadt', '107,07');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (59, '31.12.2017', 3451, '      Ammerland, Landkreis', '730,64');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (60, '31.12.2017', 3452, '      Aurich, Landkreis', '1287,36');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (61, '31.12.2017', 3453, '      Cloppenburg, Landkreis', '1420,35');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (62, '31.12.2017', 3454, '      Emsland, Landkreis', '2883,67');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (63, '31.12.2017', 3455, '      Friesland, Landkreis', '609,53');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (64, '31.12.2017', 3456, '      Grafschaft Bentheim, Landkreis', '981,79');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (65, '31.12.2017', 3457, '      Leer, Landkreis', '1085,72');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (66, '31.12.2017', 3458, '      Oldenburg, Landkreis', '1064,84');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (67, '31.12.2017', 3459, '      Osnabr�ck, Landkreis', '2121,81');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (68, '31.12.2017', 3460, '      Vechta, Landkreis', '814,2');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (69, '31.12.2017', 3461, '      Wesermarsch, Landkreis', '824,78');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (70, '31.12.2017', 3462, '      Wittmund, Landkreis', '656,86');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (71, '31.12.2017', 4000, '  Bremen', '419,84');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (72, '31.12.2017', 4011, '      Bremen, Kreisfreie Stadt', '326,18');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (73, '31.12.2017', 4012, '      Bremerhaven, Kreisfreie Stadt', '93,66');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (74, '31.12.2017', 5, '  Nordrhein-Westfalen', '34112,45');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (75, '31.12.2017', 51, '    D�sseldorf, Regierungsbezirk', '5292,33');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (76, '31.12.2017', 5111, '      D�sseldorf, Kreisfreie Stadt', '217,41');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (77, '31.12.2017', 5112, '      Duisburg, Kreisfreie Stadt', '232,8');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (78, '31.12.2017', 5113, '      Essen, Kreisfreie Stadt', '210,34');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (79, '31.12.2017', 5114, '      Krefeld, Kreisfreie Stadt', '137,77');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (80, '31.12.2017', 5116, '      M�nchengladbach, Kreisfreie Stadt', '170,47');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (81, '31.12.2017', 5117, '      M�lheim an der Ruhr, Kreisfreie Stadt', '91,28');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (82, '31.12.2017', 5119, '      Oberhausen, Kreisfreie Stadt', '77,09');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (83, '31.12.2017', 5120, '      Remscheid, Kreisfreie Stadt', '74,52');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (84, '31.12.2017', 5122, '      Solingen, Kreisfreie Stadt', '89,54');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (85, '31.12.2017', 5124, '      Wuppertal, Kreisfreie Stadt', '168,39');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (86, '31.12.2017', 5154, '      Kleve, Kreis', '1232,99');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (87, '31.12.2017', 5158, '      Mettmann, Kreis', '407,22');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (88, '31.12.2017', 5162, '      Rhein-Kreis Neuss', '576,42');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (89, '31.12.2017', 5166, '      Viersen, Kreis', '563,28');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (90, '31.12.2017', 5170, '      Wesel, Kreis', '1042,8');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (91, '31.12.2017', 53, '    K�ln, Regierungsbezirk', '7364,06');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (92, '31.12.2017', 5314, '      Bonn, Kreisfreie Stadt', '141,06');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (93, '31.12.2017', 5315, '      K�ln, Kreisfreie Stadt', '405,01');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (94, '31.12.2017', 5316, '      Leverkusen, Kreisfreie Stadt', '78,87');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (95, '31.12.2017', 5334, '      St�dteregion Aachen (einschl. Stadt Aachen)', '706,91');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (96, '31.12.2017', 5334002, '      Aachen, krfr. Stadt', '160,85');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (97, '31.12.2017', 5354, '      Aachen, Kreis', '-');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (98, '31.12.2017', 5358, '      D�ren, Kreis', '941,49');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (99, '31.12.2017', 5362, '      Rhein-Erft-Kreis', '704,71');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (100, '31.12.2017', 5366, '      Euskirchen, Kreis', '1248,73');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (101, '31.12.2017', 5370, '      Heinsberg, Kreis', '627,91');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (102, '31.12.2017', 5374, '      Oberbergischer Kreis', '918,85');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (103, '31.12.2017', 5378, '      Rheinisch-Bergischer Kreis', '437,32');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (104, '31.12.2017', 5382, '      Rhein-Sieg-Kreis', '1153,21');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (105, '31.12.2017', 55, '    M�nster, Regierungsbezirk', '6918,35');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (106, '31.12.2017', 5512, '      Bottrop, Kreisfreie Stadt', '100,61');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (107, '31.12.2017', 5513, '      Gelsenkirchen, Kreisfreie Stadt', '104,94');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (108, '31.12.2017', 5515, '      M�nster, Kreisfreie Stadt', '303,28');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (109, '31.12.2017', 5554, '      Borken, Kreis', '1420,98');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (110, '31.12.2017', 5558, '      Coesfeld, Kreis', '1112,05');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (111, '31.12.2017', 5562, '      Recklinghausen, Kreis', '761,31');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (112, '31.12.2017', 5566, '      Steinfurt, Kreis', '1795,76');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (113, '31.12.2017', 5570, '      Warendorf, Kreis', '1319,41');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (114, '31.12.2017', 57, '    Detmold, Regierungsbezirk', '6525,3');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (115, '31.12.2017', 5711, '      Bielefeld, Kreisfreie Stadt', '258,82');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (116, '31.12.2017', 5754, '      G�tersloh, Kreis', '969,21');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (117, '31.12.2017', 5758, '      Herford, Kreis', '450,41');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (118, '31.12.2017', 5762, '      H�xter, Kreis', '1201,42');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (119, '31.12.2017', 5766, '      Lippe, Kreis', '1246,22');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (120, '31.12.2017', 5770, '      Minden-L�bbecke, Kreis', '1152,41');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (121, '31.12.2017', 5774, '      Paderborn, Kreis', '1246,8');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (122, '31.12.2017', 59, '    Arnsberg, Regierungsbezirk', '8012,41');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (123, '31.12.2017', 5911, '      Bochum, Kreisfreie Stadt', '145,66');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (124, '31.12.2017', 5913, '      Dortmund, Kreisfreie Stadt', '280,71');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (125, '31.12.2017', 5914, '      Hagen, Kreisfreie Stadt', '160,45');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (126, '31.12.2017', 5915, '      Hamm, Kreisfreie Stadt', '226,43');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (127, '31.12.2017', 5916, '      Herne, Kreisfreie Stadt', '51,42');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (128, '31.12.2017', 5954, '      Ennepe-Ruhr-Kreis', '409,64');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (129, '31.12.2017', 5958, '      Hochsauerlandkreis', '1960,17');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (130, '31.12.2017', 5962, '      M�rkischer Kreis', '1061,06');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (131, '31.12.2017', 5966, '      Olpe, Kreis', '712,14');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (132, '31.12.2017', 5970, '      Siegen-Wittgenstein, Kreis', '1132,89');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (133, '31.12.2017', 5974, '      Soest, Kreis', '1328,63');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (134, '31.12.2017', 5978, '      Unna, Kreis', '543,21');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (135, '31.12.2017', 6, '  Hessen', '21115,68');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (136, '31.12.2017', 64, '    Darmstadt, Regierungsbezirk', '7444,25');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (137, '31.12.2017', 6411, '      Darmstadt, Kreisfreie Stadt', '122,07');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (138, '31.12.2017', 6412, '      Frankfurt am Main, Kreisfreie Stadt', '248,31');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (139, '31.12.2017', 6413, '      Offenbach am Main, Kreisfreie Stadt', '44,88');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (140, '31.12.2017', 6414, '      Wiesbaden, Landeshauptstadt, Kreisfreie Stadt', '203,87');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (141, '31.12.2017', 6431, '      Bergstra�e, Landkreis', '719,47');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (142, '31.12.2017', 6432, '      Darmstadt-Dieburg, Landkreis', '658,64');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (143, '31.12.2017', 6433, '      Gro�-Gerau, Landkreis', '453,03');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (144, '31.12.2017', 6434, '      Hochtaunuskreis', '481,84');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (145, '31.12.2017', 6435, '      Main-Kinzig-Kreis', '1397,32');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (146, '31.12.2017', 6436, '      Main-Taunus-Kreis', '222,53');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (147, '31.12.2017', 6437, '      Odenwaldkreis', '623,97');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (148, '31.12.2017', 6438, '      Offenbach, Landkreis', '356,24');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (149, '31.12.2017', 6439, '      Rheingau-Taunus-Kreis', '811,41');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (150, '31.12.2017', 6440, '      Wetteraukreis', '1100,66');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (151, '31.12.2017', 65, '    Gie�en, Regierungsbezirk', '5380,58');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (152, '31.12.2017', 6531, '      Gie�en, Landkreis', '854,56');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (153, '31.12.2017', 6532, '      Lahn-Dill-Kreis', '1066,3');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (154, '31.12.2017', 6533, '      Limburg-Weilburg, Landkreis', '738,44');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (155, '31.12.2017', 6534, '      Marburg-Biedenkopf, Landkreis', '1262,37');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (156, '31.12.2017', 6535, '      Vogelsbergkreis', '1458,91');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (157, '31.12.2017', 66, '    Kassel, Regierungsbezirk', '8290,85');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (158, '31.12.2017', 6611, '      Kassel, Kreisfreie Stadt', '106,8');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (159, '31.12.2017', 6631, '      Fulda, Landkreis', '1380,41');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (160, '31.12.2017', 6632, '      Hersfeld-Rotenburg, Landkreis', '1097,75');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (161, '31.12.2017', 6633, '      Kassel, Landkreis', '1293,35');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (162, '31.12.2017', 6634, '      Schwalm-Eder-Kreis', '1539,01');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (163, '31.12.2017', 6635, '      Waldeck-Frankenberg, Landkreis', '1848,7');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (164, '31.12.2017', 6636, '      Werra-Mei�ner-Kreis', '1024,83');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (165, '31.12.2017', 7, '  Rheinland-Pfalz', '19858');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (166, '31.12.2017', 71, '    Koblenz, Stat. Region', '8074,93');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (167, '31.12.2017', 7111, '      Koblenz, Kreisfreie Stadt', '105,25');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (168, '31.12.2017', 7131, '      Ahrweiler, Landkreis', '787,02');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (169, '31.12.2017', 7132, '      Altenkirchen (Westerwald), Landkreis', '642,38');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (170, '31.12.2017', 7133, '      Bad Kreuznach, Landkreis', '863,89');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (171, '31.12.2017', 7134, '      Birkenfeld, Landkreis', '776,83');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (172, '31.12.2017', 7135, '      Cochem-Zell, Landkreis', '692,43');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (173, '31.12.2017', 7137, '      Mayen-Koblenz, Landkreis', '817,73');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (174, '31.12.2017', 7138, '      Neuwied, Landkreis', '627,06');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (175, '31.12.2017', 7140, '      Rhein-Hunsr�ck-Kreis', '991,06');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (176, '31.12.2017', 7141, '      Rhein-Lahn-Kreis', '782,24');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (177, '31.12.2017', 7143, '      Westerwaldkreis', '989,04');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (178, '31.12.2017', 72, '    Trier, Stat. Region', '4925,82');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (179, '31.12.2017', 7211, '      Trier, Kreisfreie Stadt', '117,06');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (180, '31.12.2017', 7231, '      Bernkastel-Wittlich, Landkreis', '1167,94');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (181, '31.12.2017', 7232, '      Eifelkreis Bitburg-Pr�m', '1626,95');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (182, '31.12.2017', 7233, '      Vulkaneifel, Landkreis', '911,64');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (183, '31.12.2017', 7235, '      Trier-Saarburg, Landkreis', '1102,24');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (184, '31.12.2017', 73, '    Rheinhessen-Pfalz, Stat. Region', '6851,04');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (185, '31.12.2017', 7311, '      Frankenthal (Pfalz), Kreisfreie Stadt', '43,88');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (186, '31.12.2017', 7312, '      Kaiserslautern, Kreisfreie Stadt', '139,7');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (187, '31.12.2017', 7313, '      Landau in der Pfalz, Kreisfreie Stadt', '82,94');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (188, '31.12.2017', 7314, '      Ludwigshafen am Rhein, Kreisfreie Stadt', '77,43');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (189, '31.12.2017', 7315, '      Mainz, Kreisfreie Stadt', '97,73');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (190, '31.12.2017', 7316, '      Neustadt an der Weinstra�e, Kreisfreie Stadt', '117,09');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (191, '31.12.2017', 7317, '      Pirmasens, Kreisfreie Stadt', '61,36');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (192, '31.12.2017', 7318, '      Speyer, Kreisfreie Stadt', '42,71');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (193, '31.12.2017', 7319, '      Worms, Kreisfreie Stadt', '108,73');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (194, '31.12.2017', 7320, '      Zweibr�cken, Kreisfreie Stadt', '70,64');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (195, '31.12.2017', 7331, '      Alzey-Worms, Landkreis', '588,07');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (196, '31.12.2017', 7332, '      Bad D�rkheim, Landkreis', '594,64');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (197, '31.12.2017', 7333, '      Donnersbergkreis', '645,41');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (198, '31.12.2017', 7334, '      Germersheim, Landkreis', '463,32');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (199, '31.12.2017', 7335, '      Kaiserslautern, Landkreis', '640');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (200, '31.12.2017', 7336, '      Kusel, Landkreis', '573,61');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (201, '31.12.2017', 7337, '      S�dliche Weinstra�e, Landkreis', '639,93');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (202, '31.12.2017', 7338, '      Rhein-Pfalz-Kreis', '304,99');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (203, '31.12.2017', 7339, '      Mainz-Bingen, Landkreis', '605,36');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (204, '31.12.2017', 7340, '      S�dwestpfalz, Landkreis', '953,52');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (205, '31.12.2017', 8, '  Baden-W�rttemberg, Land', '35673,73');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (206, '31.12.2017', 81, '    Stuttgart, Regierungsbezirk', '10556,89');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (207, '31.12.2017', 8111, '      Stuttgart, Landeshauptstadt, Stadtkreis', '207,35');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (208, '31.12.2017', 8115, '      B�blingen, Landkreis', '617,76');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (209, '31.12.2017', 8116, '      Esslingen, Landkreis', '641,28');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (210, '31.12.2017', 8117, '      G�ppingen, Landkreis', '642,34');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (211, '31.12.2017', 8118, '      Ludwigsburg, Landkreis', '686,77');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (212, '31.12.2017', 8119, '      Rems-Murr-Kreis, Landkreis', '858,08');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (213, '31.12.2017', 8121, '      Heilbronn, Stadtkreis', '99,89');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (214, '31.12.2017', 8125, '      Heilbronn, Landkreis', '1099,91');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (215, '31.12.2017', 8126, '      Hohenlohekreis, Landkreis', '776,76');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (216, '31.12.2017', 8127, '      Schw�bisch Hall, Landkreis', '1484,07');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (217, '31.12.2017', 8128, '      Main-Tauber-Kreis, Landkreis', '1304,13');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (218, '31.12.2017', 8135, '      Heidenheim, Landkreis', '627,14');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (219, '31.12.2017', 8136, '      Ostalbkreis, Landkreis', '1511,39');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (220, '31.12.2017', 82, '    Karlsruhe, Regierungsbzirk', '6918,03');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (221, '31.12.2017', 8211, '      Baden-Baden, Stadtkreis', '140,19');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (222, '31.12.2017', 8212, '      Karlsruhe, Stadtkreis', '173,42');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (223, '31.12.2017', 8215, '      Karlsruhe, Landkreis', '1085,28');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (224, '31.12.2017', 8216, '      Rastatt, Landkreis', '738,43');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (225, '31.12.2017', 8221, '      Heidelberg, Stadtkreis', '108,89');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (226, '31.12.2017', 8222, '      Mannheim, Stadtkreis', '144,97');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (227, '31.12.2017', 8225, '      Neckar-Odenwald-Kreis, Landkreis', '1125,94');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (228, '31.12.2017', 8226, '      Rhein-Neckar-Kreis, Landkreis', '1061,55');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (229, '31.12.2017', 8231, '      Pforzheim, Stadtkreis', '98,07');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (230, '31.12.2017', 8235, '      Calw, Landkreis', '797,29');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (231, '31.12.2017', 8236, '      Enzkreis, Landkreis', '573,6');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (232, '31.12.2017', 8237, '      Freudenstadt, Landkreis', '870,39');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (233, '31.12.2017', 83, '    Freiburg, Regierungsbezirk', '9346,4');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (234, '31.12.2017', 8311, '      Freiburg im Breisgau, Stadtkreis', '153,04');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (235, '31.12.2017', 8315, '      Breisgau-Hochschwarzwald, Landkreis', '1378,31');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (236, '31.12.2017', 8316, '      Emmendingen, Landkreis', '679,79');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (237, '31.12.2017', 8317, '      Ortenaukreis, Landkreis', '1850,34');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (238, '31.12.2017', 8325, '      Rottweil, Landkreis', '769,42');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (239, '31.12.2017', 8326, '      Schwarzwald-Baar-Kreis, Landkreis', '1025,33');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (240, '31.12.2017', 8327, '      Tuttlingen, Landkreis', '734,38');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (241, '31.12.2017', 8335, '      Konstanz, Landkreis', '817,98');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (242, '31.12.2017', 8336, '      L�rrach, Landkreis', '806,71');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (243, '31.12.2017', 8337, '      Waldshut, Landkreis', '1131,1');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (244, '31.12.2017', 84, '    T�bingen, Regierungsbezirk', '8852,42');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (245, '31.12.2017', 8415, '      Reutlingen, Landkreis', '1027,85');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (246, '31.12.2017', 8416, '      T�bingen, Landkreis', '519,12');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (247, '31.12.2017', 8417, '      Zollernalbkreis, Landkreis', '917,58');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (248, '31.12.2017', 8421, '      Ulm, Stadtkreis', '118,68');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (249, '31.12.2017', 8425, '      Alb-Donau-Kreis, Landkreis', '1358,55');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (250, '31.12.2017', 8426, '      Biberach, Landkreis', '1409,53');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (251, '31.12.2017', 8435, '      Bodenseekreis, Landkreis', '664,79');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (252, '31.12.2017', 8436, '      Ravensburg, Landkreis', '1632,1');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (253, '31.12.2017', 8437, '      Sigmaringen, Landkreis', '1204,23');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (254, '31.12.2017', 9, '  Bayern', '70542,03');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (255, '31.12.2017', 91, '    Oberbayern, Regierungsbezirk', '17529,25');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (256, '31.12.2017', 9161, '      Ingolstadt', '133,35');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (257, '31.12.2017', 9162, '      M�nchen, Landeshauptstadt', '310,71');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (258, '31.12.2017', 9163, '      Rosenheim', '37,22');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (259, '31.12.2017', 9171, '      Alt�tting, Landkreis', '569,29');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (260, '31.12.2017', 9172, '      Berchtesgadener Land, Landkreis', '839,83');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (261, '31.12.2017', 9173, '      Bad T�lz-Wolfratshausen, Landkreis', '1110,67');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (262, '31.12.2017', 9174, '      Dachau, Landkreis', '579,16');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (263, '31.12.2017', 9175, '      Ebersberg, Landkreis', '549,4');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (264, '31.12.2017', 9176, '      Eichst�tt, Landkreis', '1213,84');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (265, '31.12.2017', 9177, '      Erding, Landkreis', '870,75');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (266, '31.12.2017', 9178, '      Freising, Landkreis', '799,85');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (267, '31.12.2017', 9179, '      F�rstenfeldbruck, Landkreis', '434,8');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (268, '31.12.2017', 9180, '      Garmisch-Partenkirchen, Landkreis', '1012,21');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (269, '31.12.2017', 9181, '      Landsberg am Lech, Landkreis', '804,37');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (270, '31.12.2017', 9182, '      Miesbach, Landkreis', '866,22');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (271, '31.12.2017', 9183, '      M�hldorf a.Inn, Landkreis', '805,33');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (272, '31.12.2017', 9184, '      M�nchen, Landkreis', '664,26');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (273, '31.12.2017', 9185, '      Neuburg-Schrobenhausen, Landkreis', '739,72');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (274, '31.12.2017', 9186, '      Pfaffenhofen a.d.Ilm, Landkreis', '761,05');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (275, '31.12.2017', 9187, '      Rosenheim, Landkreis', '1439,45');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (276, '31.12.2017', 9188, '      Starnberg, Landkreis', '487,72');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (277, '31.12.2017', 9189, '      Traunstein, Landkreis', '1533,76');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (278, '31.12.2017', 9190, '      Weilheim-Schongau, Landkreis', '966,3');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (279, '31.12.2017', 92, '    Niederbayern, Regierungsbezirk', '10326,05');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (280, '31.12.2017', 9261, '      Landshut', '65,83');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (281, '31.12.2017', 9262, '      Passau', '69,56');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (282, '31.12.2017', 9263, '      Straubing', '67,59');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (283, '31.12.2017', 9271, '      Deggendorf, Landkreis', '861,17');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (284, '31.12.2017', 9272, '      Freyung-Grafenau, Landkreis', '983,87');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (285, '31.12.2017', 9273, '      Kelheim, Landkreis', '1065,16');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (286, '31.12.2017', 9274, '      Landshut, Landkreis', '1347,57');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (287, '31.12.2017', 9275, '      Passau, Landkreis', '1530,12');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (288, '31.12.2017', 9276, '      Regen, Landkreis', '974,79');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (289, '31.12.2017', 9277, '      Rottal-Inn, Landkreis', '1281,21');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (290, '31.12.2017', 9278, '      Straubing-Bogen, Landkreis', '1201,62');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (291, '31.12.2017', 9279, '      Dingolfing-Landau, Landkreis', '877,58');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (292, '31.12.2017', 93, '    Oberpfalz, Regierungsbezirk', '9690,21');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (293, '31.12.2017', 9361, '      Amberg', '50,13');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (294, '31.12.2017', 9362, '      Regensburg', '80,85');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (295, '31.12.2017', 9363, '      Weiden i.d.OPf.', '70,57');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (296, '31.12.2017', 9371, '      Amberg-Sulzbach, Landkreis', '1255,87');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (297, '31.12.2017', 9372, '      Cham, Landkreis', '1526,83');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (298, '31.12.2017', 9373, '      Neumarkt i.d.OPf., Landkreis', '1343,97');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (299, '31.12.2017', 9374, '      Neustadt a.d.Waldnaab, Landkreis', '1427,71');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (300, '31.12.2017', 9375, '      Regensburg, Landkreis', '1391,67');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (301, '31.12.2017', 9376, '      Schwandorf, Landkreis', '1458,36');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (302, '31.12.2017', 9377, '      Tirschenreuth, Landkreis', '1084,25');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (303, '31.12.2017', 94, '    Oberfranken, Regierungsbezirk', '7231,15');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (304, '31.12.2017', 9461, '      Bamberg', '54,62');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (305, '31.12.2017', 9462, '      Bayreuth', '66,89');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (306, '31.12.2017', 9463, '      Coburg', '48,29');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (307, '31.12.2017', 9464, '      Hof', '58,02');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (308, '31.12.2017', 9471, '      Bamberg, Landkreis', '1167,79');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (309, '31.12.2017', 9472, '      Bayreuth, Landkreis', '1273,63');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (310, '31.12.2017', 9473, '      Coburg, Landkreis', '590,42');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (311, '31.12.2017', 9474, '      Forchheim, Landkreis', '642,83');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (312, '31.12.2017', 9475, '      Hof, Landkreis', '892,52');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (313, '31.12.2017', 9476, '      Kronach, Landkreis', '651,49');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (314, '31.12.2017', 9477, '      Kulmbach, Landkreis', '658,34');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (315, '31.12.2017', 9478, '      Lichtenfels, Landkreis', '519,94');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (316, '31.12.2017', 9479, '      Wunsiedel i.Fichtelgebirge, Landkreis', '606,37');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (317, '31.12.2017', 95, '    Mittelfranken, Regierungsbezirk', '7243,69');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (318, '31.12.2017', 9561, '      Ansbach', '99,91');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (319, '31.12.2017', 9562, '      Erlangen', '76,96');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (320, '31.12.2017', 9563, '      F�rth', '63,35');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (321, '31.12.2017', 9564, '      N�rnberg', '186,45');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (322, '31.12.2017', 9565, '      Schwabach', '40,8');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (323, '31.12.2017', 9571, '      Ansbach, Landkreis', '1971,33');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (324, '31.12.2017', 9572, '      Erlangen-H�chstadt, Landkreis', '564,56');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (325, '31.12.2017', 9573, '      F�rth, Landkreis', '307,44');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (326, '31.12.2017', 9574, '      N�rnberger Land, Landkreis', '799,52');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (327, '31.12.2017', 9575, '      Neustadt a.d.Aisch-Bad Windsheim, Landkreis', '1267,45');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (328, '31.12.2017', 9576, '      Roth, Landkreis', '895,15');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (329, '31.12.2017', 9577, '      Wei�enburg-Gunzenhausen, Landkreis', '970,78');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (330, '31.12.2017', 96, '    Unterfranken, Regierungsbezirk', '8530,05');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (331, '31.12.2017', 9661, '      Aschaffenburg', '62,45');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (332, '31.12.2017', 9662, '      Schweinfurt', '35,7');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (333, '31.12.2017', 9663, '      W�rzburg', '87,6');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (334, '31.12.2017', 9671, '      Aschaffenburg, Landkreis', '698,9');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (335, '31.12.2017', 9672, '      Bad Kissingen, Landkreis', '1136,84');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (336, '31.12.2017', 9673, '      Rh�n-Grabfeld, Landkreis', '1021,72');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (337, '31.12.2017', 9674, '      Ha�berge, Landkreis', '956,19');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (338, '31.12.2017', 9675, '      Kitzingen, Landkreis', '684,14');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (339, '31.12.2017', 9676, '      Miltenberg, Landkreis', '715,58');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (340, '31.12.2017', 9677, '      Main-Spessart, Landkreis', '1321,19');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (341, '31.12.2017', 9678, '      Schweinfurt, Landkreis', '841,39');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (342, '31.12.2017', 9679, '      W�rzburg, Landkreis', '968,36');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (343, '31.12.2017', 97, '    Schwaben, Regierungsbezirk', '9991,62');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (344, '31.12.2017', 9761, '      Augsburg', '146,87');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (345, '31.12.2017', 9762, '      Kaufbeuren', '40,02');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (346, '31.12.2017', 9763, '      Kempten (Allg�u)', '63,28');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (347, '31.12.2017', 9764, '      Memmingen', '70,11');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (348, '31.12.2017', 9771, '      Aichach-Friedberg, Landkreis', '780,24');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (349, '31.12.2017', 9772, '      Augsburg, Landkreis', '1070,62');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (350, '31.12.2017', 9773, '      Dillingen a.d.Donau, Landkreis', '792,23');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (351, '31.12.2017', 9774, '      G�nzburg, Landkreis', '762,4');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (352, '31.12.2017', 9775, '      Neu-Ulm, Landkreis', '515,84');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (353, '31.12.2017', 9776, '      Lindau (Bodensee), Landkreis', '323,39');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (354, '31.12.2017', 9777, '      Ostallg�u, Landkreis', '1394,45');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (355, '31.12.2017', 9778, '      Unterallg�u, Landkreis', '1229,59');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (356, '31.12.2017', 9779, '      Donau-Ries, Landkreis', '1274,58');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (357, '31.12.2017', 9780, '      Oberallg�u, Landkreis', '1527,99');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (358, '31.12.2017', 10, '  Saarland', '2571,1');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (359, '31.12.2017', 10041, '      Saarbr�cken, Regionalverband', '410,95');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (360, '31.12.2017', 10041100, '      Saarbr�cken, Landeshauptstadt', '167,52');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (361, '31.12.2017', 10042, '      Merzig-Wadern, Landkreis', '556,66');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (362, '31.12.2017', 10043, '      Neunkirchen, Landkreis', '249,8');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (363, '31.12.2017', 10044, '      Saarlouis, Landkreis', '459,35');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (364, '31.12.2017', 10045, '      Saarpfalz-Kreis', '418,27');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (365, '31.12.2017', 10046, '      St. Wendel, Landkreis', '476,07');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (366, '31.12.2017', 11000, '  Berlin', '891,12');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (367, '31.12.2017', 11001001, '      Berlin-Mitte', '-');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (368, '31.12.2017', 11002002, '      Berlin-Friedrichshain-Kreuzberg', '-');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (369, '31.12.2017', 11003003, '      Berlin-Pankow', '-');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (370, '31.12.2017', 11004004, '      Berlin-Charlottenburg-Wilmersdorf', '-');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (371, '31.12.2017', 11005005, '      Berlin-Spandau', '-');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (372, '31.12.2017', 11006006, '      Berlin-Steglitz-Zehlendorf', '-');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (373, '31.12.2017', 11007007, '      Berlin-Tempelhof-Sch�neberg', '-');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (374, '31.12.2017', 11008008, '      Berlin-Neuk�lln', '-');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (375, '31.12.2017', 11009009, '      Berlin-Treptow-K�penick', '-');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (376, '31.12.2017', 11010010, '      Berlin-Marzahn-Hellersdorf', '-');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (377, '31.12.2017', 11011011, '      Berlin-Lichtenberg', '-');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (378, '31.12.2017', 11012012, '      Berlin-Reinickendorf', '-');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (379, '31.12.2017', 12, '  Brandenburg', '29654,37');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (380, '31.12.2017', 12051, '      Brandenburg an der Havel, Kreisfreie Stadt', '229,72');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (381, '31.12.2017', 12052, '      Cottbus, Kreisfreie Stadt', '165,63');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (382, '31.12.2017', 12053, '      Frankfurt (Oder), Kreisfreie Stadt', '147,85');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (383, '31.12.2017', 12054, '      Potsdam, Kreisfreie Stadt', '188,26');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (384, '31.12.2017', 12060, '      Barnim, Landkreis', '1479,59');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (385, '31.12.2017', 12061, '      Dahme-Spreewald, Landkreis', '2274,51');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (386, '31.12.2017', 12062, '      Elbe-Elster, Landkreis', '1899,6');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (387, '31.12.2017', 12063, '      Havelland, Landkreis', '1727,3');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (388, '31.12.2017', 12064, '      M�rkisch-Oderland, Landkreis', '2158,65');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (389, '31.12.2017', 12065, '      Oberhavel, Landkreis', '1808,18');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (390, '31.12.2017', 12066, '      Oberspreewald-Lausitz, Landkreis', '1223,07');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (391, '31.12.2017', 12067, '      Oder-Spree, Landkreis', '2256,75');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (392, '31.12.2017', 12068, '      Ostprignitz-Ruppin, Landkreis', '2526,56');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (393, '31.12.2017', 12069, '      Potsdam-Mittelmark, Landkreis', '2592,01');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (394, '31.12.2017', 12070, '      Prignitz, Landkreis', '2138,55');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (395, '31.12.2017', 12071, '      Spree-Nei�e, Landkreis', '1656,97');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (396, '31.12.2017', 12072, '      Teltow-Fl�ming, Landkreis', '2104,22');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (397, '31.12.2017', 12073, '      Uckermark, Landkreis', '3076,97');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (398, '31.12.2017', 13, '  Mecklenburg-Vorpommern', '23293,3');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (399, '31.12.2017', 13001, '      Kreisfreie Stadt Greifswald, Hansestadt', '-');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (400, '31.12.2017', 13002, '      Kreisfreie Stadt Neubrandenburg, Stadt', '-');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (401, '31.12.2017', 13003, '      Kreisfreie Stadt Rostock, Hansestadt', '181,36');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (402, '31.12.2017', 13004, '      Kreisfreie Stadt Schwerin, Landeshauptstadt', '130,52');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (403, '31.12.2017', 13005, '      Kreisfreie Stadt Stralsund, Hansestadt', '-');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (404, '31.12.2017', 13006, '      Kreisfreie Stadt Wismar, Hansestadt', '-');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (405, '31.12.2017', 13051, '      Landkreis Bad Doberan', '-');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (406, '31.12.2017', 13052, '      Landkreis Demmin', '-');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (407, '31.12.2017', 13053, '      Landkreis G�strow', '-');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (408, '31.12.2017', 13054, '      Landkreis Ludwigslust', '-');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (409, '31.12.2017', 13055, '      Landkreis Mecklenburg-Strelitz', '-');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (410, '31.12.2017', 13056, '      Landkreis M�ritz', '-');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (411, '31.12.2017', 13057, '      Landkreis Nordvorpommern', '-');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (412, '31.12.2017', 13058, '      Landkreis Nordwestmecklenburg', '-');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (413, '31.12.2017', 13059, '      Landkreis Ostvorpommern', '-');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (414, '31.12.2017', 13060, '      Landkreis Parchim', '-');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (415, '31.12.2017', 13061, '      Landkreis R�gen', '-');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (416, '31.12.2017', 13062, '      Landkreis Uecker-Randow', '-');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (417, '31.12.2017', 13071, '      Landkreis Mecklenburgische Seenplatte', '5495,62');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (418, '31.12.2017', 13072, '      Landkreis Rostock', '3431,29');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (419, '31.12.2017', 13073, '      Landkreis Vorpommern-R�gen', '3215,03');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (420, '31.12.2017', 13074, '      Landkreis Nordwestmecklenburg', '2127,12');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (421, '31.12.2017', 13075, '      Landkreis Vorpommern-Greifswald', '3945,57');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (422, '31.12.2017', 13076, '      Landkreis Ludwigslust-Parchim', '4766,79');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (423, '31.12.2017', 14, '  Sachsen', '18449,97');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (424, '31.12.2017', 141, '    Chemnitz, Regierungsbezirk', '-');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (425, '31.12.2017', 14161, '      Chemnitz, Kreisfreie Stadt', '-');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (426, '31.12.2017', 14166, '      Plauen, Kreisfreie Stadt', '-');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (427, '31.12.2017', 14167, '      Zwickau, Kreisfreie Stadt', '-');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (428, '31.12.2017', 14171, '      Annaberg, Landkreis', '-');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (429, '31.12.2017', 14173, '      Chemnitzer Land, Landkreis', '-');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (430, '31.12.2017', 14177, '      Freiberg, Landkreis', '-');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (431, '31.12.2017', 14178, '      Vogtlandkreis', '-');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (432, '31.12.2017', 14181, '      Mittlerer Erzgebirgskreis', '-');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (433, '31.12.2017', 14182, '      Mittweida, Landkreis', '-');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (434, '31.12.2017', 14188, '      Stollberg, Landkreis', '-');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (435, '31.12.2017', 14191, '      Aue-Schwarzenberg, Landkreis', '-');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (436, '31.12.2017', 14193, '      Zwickauer Land, Landkreis', '-');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (437, '31.12.2017', 142, '    Dresden, Regierungsbezirk', '-');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (438, '31.12.2017', 14262, '      Dresden, Kreisfreie Stadt', '-');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (439, '31.12.2017', 14263, '      G�rlitz, Kreisfreie Stadt', '-');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (440, '31.12.2017', 14264, '      Hoyerswerda, Kreisfreie Stadt', '-');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (441, '31.12.2017', 14272, '      Bautzen, Landkreis', '-');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (442, '31.12.2017', 14280, '      Mei�en, Landkreis', '-');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (443, '31.12.2017', 14284, '      Niederschlesischer Oberlausitzkreis', '-');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (444, '31.12.2017', 14285, '      Riesa-Gro�enhain, Landkreis', '-');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (445, '31.12.2017', 14286, '      L�bau-Zittau, Landkreis', '-');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (446, '31.12.2017', 14287, '      S�chsische Schweiz, Landkreis', '-');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (447, '31.12.2017', 14290, '      Wei�eritzkreis', '-');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (448, '31.12.2017', 14292, '      Kamenz, Landkreis', '-');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (449, '31.12.2017', 143, '    Leipzig, Regierungsbezirk', '-');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (450, '31.12.2017', 14365, '      Leipzig, Kreisfreie Stadt', '-');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (451, '31.12.2017', 14374, '      Delitzsch, Landkreis', '-');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (452, '31.12.2017', 14375, '      D�beln, Landkreis', '-');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (453, '31.12.2017', 14379, '      Leipziger Land, Landkreis', '-');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (454, '31.12.2017', 14383, '      Muldentalkreis', '-');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (455, '31.12.2017', 14389, '      Torgau-Oschatz, Landkreis', '-');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (456, '31.12.2017', 145, '    Chemnitz, Stat. Region', '6528,02');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (457, '31.12.2017', 14511, '      Chemnitz, Stadt', '221,05');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (458, '31.12.2017', 14521, '      Erzgebirgskreis', '1827,91');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (459, '31.12.2017', 14522, '      Mittelsachsen, Landkreis', '2116,85');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (460, '31.12.2017', 14523, '      Vogtlandkreis', '1412,42');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (461, '31.12.2017', 14524, '      Zwickau, Landkreis', '949,79');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (462, '31.12.2017', 146, '    Dresden, Stat. Region', '7944,27');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (463, '31.12.2017', 14612, '      Dresden, Stadt', '328,48');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (464, '31.12.2017', 14625, '      Bautzen, Landkreis', '2395,6');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (465, '31.12.2017', 14626, '      G�rlitz, Landkreis', '2111,41');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (466, '31.12.2017', 14627, '      Mei�en, Landkreis', '1454,59');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (467, '31.12.2017', 14628, '      S�chsische Schweiz-Osterzgebirge, Landkreis', '1654,19');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (468, '31.12.2017', 147, '    Leipzig, Stat. Region', '3977,68');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (469, '31.12.2017', 14713, '      Leipzig, Stadt', '297,8');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (470, '31.12.2017', 14729, '      Leipzig, Landkreis', '1651,32');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (471, '31.12.2017', 14730, '      Nordsachsen, Landkreis', '2028,56');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (472, '31.12.2017', 15, '  Sachsen-Anhalt', '20453,79');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (473, '31.12.2017', 15001, '      Dessau-Ro�lau, Kreisfreie Stadt', '244,75');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (474, '31.12.2017', 15002, '      Halle (Saale), Kreisfreie Stadt', '135,03');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (475, '31.12.2017', 15003, '      Magdeburg, Kreisfreie Stadt', '201,01');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (476, '31.12.2017', 15081, '      Altmarkkreis Salzwedel', '2293,28');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (477, '31.12.2017', 15082, '      Anhalt-Bitterfeld, Landkreis', '1454,26');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (478, '31.12.2017', 15083, '      B�rde, Landkreis', '2366,84');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (479, '31.12.2017', 15084, '      Burgenlandkreis', '1413,72');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (480, '31.12.2017', 15085, '      Harz, Landkreis', '2104,57');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (481, '31.12.2017', 15086, '      Jerichower Land, Landkreis', '1576,91');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (482, '31.12.2017', 15087, '      Mansfeld-S�dharz, Landkreis', '1448,84');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (483, '31.12.2017', 15088, '      Saalekreis', '1433,73');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (484, '31.12.2017', 15089, '      Salzlandkreis', '1427,13');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (485, '31.12.2017', 15090, '      Stendal, Landkreis', '2423,26');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (486, '31.12.2017', 15091, '      Wittenberg, Landkreis', '1930,47');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (487, '31.12.2017', 151, '    Dessau, Stat. Region', '-');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (488, '31.12.2017', 15101, '      Dessau, Kreisfreie Stadt', '-');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (489, '31.12.2017', 15151, '      Anhalt-Zerbst, Kreis', '-');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (490, '31.12.2017', 15153, '      Bernburg, Kreis', '-');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (491, '31.12.2017', 15154, '      Bitterfeld, Kreis', '-');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (492, '31.12.2017', 15159, '      K�then, Kreis', '-');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (493, '31.12.2017', 15171, '      Wittenberg, Kreis', '-');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (494, '31.12.2017', 152, '    Halle, Stat. Region', '-');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (495, '31.12.2017', 15202, '      Halle (Saale), Kreisfreie Stadt', '-');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (496, '31.12.2017', 15256, '      Burgenlandkreis', '-');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (497, '31.12.2017', 15260, '      Mansfelder Land, Kreis', '-');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (498, '31.12.2017', 15261, '      Merseburg-Querfurt, Kreis', '-');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (499, '31.12.2017', 15265, '      Saalkreis', '-');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (500, '31.12.2017', 15266, '      Sangerhausen, Kreis', '-');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (501, '31.12.2017', 15268, '      Wei�enfels, Kreis', '-');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (502, '31.12.2017', 153, '    Magdeburg, Stat. Region', '-');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (503, '31.12.2017', 15303, '      Magdeburg, Kreisfreie Stadt', '-');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (504, '31.12.2017', 15352, '      Aschersleben-Sta�furt, Kreis', '-');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (505, '31.12.2017', 15355, '      B�rdekreis', '-');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (506, '31.12.2017', 15357, '      Halberstadt, Kreis', '-');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (507, '31.12.2017', 15358, '      Jerichower Land, Kreis', '-');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (508, '31.12.2017', 15362, '      Ohrekreis', '-');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (509, '31.12.2017', 15363, '      Stendal, Kreis', '-');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (510, '31.12.2017', 15364, '      Quedlinburg, Kreis', '-');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (511, '31.12.2017', 15367, '      Sch�nebeck, Kreis', '-');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (512, '31.12.2017', 15369, '      Wernigerode, Kreis', '-');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (513, '31.12.2017', 15370, '      Altmarkkreis Salzwedel, Kreis', '-');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (514, '31.12.2017', 16, '  Th�ringen', '16202,41');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (515, '31.12.2017', 16051, '      Erfurt, krsfr. Stadt', '269,91');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (516, '31.12.2017', 16052, '      Gera, krsfr. Stadt', '152,19');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (517, '31.12.2017', 16053, '      Jena, krsfr. Stadt', '114,76');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (518, '31.12.2017', 16054, '      Suhl, krsfr. Stadt', '103,03');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (519, '31.12.2017', 16055, '      Weimar, krsfr. Stadt', '84,48');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (520, '31.12.2017', 16056, '      Eisenach, krsfr. Stadt', '104,17');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (521, '31.12.2017', 16061, '      Eichsfeld, Kreis', '943,07');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (522, '31.12.2017', 16062, '      Nordhausen, Kreis', '713,91');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (523, '31.12.2017', 16063, '      Wartburgkreis', '1307,43');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (524, '31.12.2017', 16064, '      Unstrut-Hainich-Kreis', '979,69');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (525, '31.12.2017', 16065, '      Kyffh�userkreis', '1037,91');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (526, '31.12.2017', 16066, '      Schmalkalden-Meiningen, Kreis', '1210,73');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (527, '31.12.2017', 16067, '      Gotha, Kreis', '936,08');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (528, '31.12.2017', 16068, '      S�mmerda, Kreis', '806,86');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (529, '31.12.2017', 16069, '      Hildburghausen, Kreis', '938,42');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (530, '31.12.2017', 16070, '      Ilm-Kreis', '843,71');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (531, '31.12.2017', 16071, '      Weimarer Land, Kreis', '804,48');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (532, '31.12.2017', 16072, '      Sonneberg, Kreis', '433,61');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (533, '31.12.2017', 16073, '      Saalfeld-Rudolstadt, Kreis', '1036,03');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (534, '31.12.2017', 16074, '      Saale-Holzland-Kreis', '815,24');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (535, '31.12.2017', 16075, '      Saale-Orla-Kreis', '1151,3');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (536, '31.12.2017', 16076, '      Greiz, Kreis', '845,98');
INSERT INTO kriminal."Flaeche_Kreise" VALUES (537, '31.12.2017', 16077, '      Altenburger Land, Kreis', '569,41');


--
-- TOC entry 4294 (class 0 OID 0)
-- Dependencies: 225
-- Name: Flaeche_Kreise_id_seq; Type: SEQUENCE SET; Schema: kriminal; Owner: postgres
--

SELECT pg_catalog.setval('kriminal."Flaeche_Kreise_id_seq"', 537, true);


--
-- TOC entry 4157 (class 2606 OID 332955)
-- Name: Flaeche_Kreise Flaeche_Kreise_pkey; Type: CONSTRAINT; Schema: kriminal; Owner: postgres
--

ALTER TABLE ONLY kriminal."Flaeche_Kreise"
    ADD CONSTRAINT "Flaeche_Kreise_pkey" PRIMARY KEY (id);


-- Completed on 2019-08-14 16:54:04

--
-- PostgreSQL database dump complete
--

