--
-- PostgreSQL database dump
--



SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'WIN1250';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 219 (class 1259 OID 32777)
-- Name: bevoelkerung; Type: TABLE; Schema: kriminal; Owner: postgres
--

CREATE TABLE kriminal.bevoelkerung (
    id integer NOT NULL,
    "Jahr" integer,
    "GS" integer,
    "Kreis" character varying,
    "BevoelkerungInsgesamt" character varying,
    "m�nnlich" character varying,
    weiblich character varying
);


ALTER TABLE kriminal.bevoelkerung OWNER TO postgres;

--
-- TOC entry 218 (class 1259 OID 32775)
-- Name: bevoelkerung_id_seq; Type: SEQUENCE; Schema: kriminal; Owner: postgres
--

CREATE SEQUENCE kriminal.bevoelkerung_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE kriminal.bevoelkerung_id_seq OWNER TO postgres;

--
-- TOC entry 4270 (class 0 OID 0)
-- Dependencies: 218
-- Name: bevoelkerung_id_seq; Type: SEQUENCE OWNED BY; Schema: kriminal; Owner: postgres
--

ALTER SEQUENCE kriminal.bevoelkerung_id_seq OWNED BY kriminal.bevoelkerung.id;


--
-- TOC entry 4131 (class 2604 OID 32780)
-- Name: bevoelkerung id; Type: DEFAULT; Schema: kriminal; Owner: postgres
--

ALTER TABLE ONLY kriminal.bevoelkerung ALTER COLUMN id SET DEFAULT nextval('kriminal.bevoelkerung_id_seq'::regclass);


--
-- TOC entry 4263 (class 0 OID 32777)
-- Dependencies: 219
-- Data for Name: bevoelkerung; Type: TABLE DATA; Schema: kriminal; Owner: postgres
--

INSERT INTO kriminal.bevoelkerung VALUES (1, 2017, 0, 'Deutschland', '82657002', '40770342', '41886661');
INSERT INTO kriminal.bevoelkerung VALUES (2, 2017, 1, '  Schleswig-Holstein', '2885874', '1414600', '1471274');
INSERT INTO kriminal.bevoelkerung VALUES (3, 2017, 1001, '      Flensburg, Kreisfreie Stadt', '87976', '43852', '44124');
INSERT INTO kriminal.bevoelkerung VALUES (4, 2017, 1002, '      Kiel, Landeshauptstadt, Kreisfreie Stadt', '247692', '120641', '127051');
INSERT INTO kriminal.bevoelkerung VALUES (5, 2017, 1003, '      L�beck, Hansestadt, Kreisfreie Stadt', '216515', '103903', '112613');
INSERT INTO kriminal.bevoelkerung VALUES (6, 2017, 1004, '      Neum�nster, Kreisfreie Stadt', '79508', '39314', '40194');
INSERT INTO kriminal.bevoelkerung VALUES (7, 2017, 1051, '      Dithmarschen, Landkreis', '133504', '65901', '67603');
INSERT INTO kriminal.bevoelkerung VALUES (8, 2017, 1053, '      Herzogtum Lauenburg, Landkreis', '195569', '96017', '99552');
INSERT INTO kriminal.bevoelkerung VALUES (9, 2017, 1054, '      Nordfriesland, Landkreis', '165194', '81020', '84174');
INSERT INTO kriminal.bevoelkerung VALUES (10, 2017, 1055, '      Ostholstein, Landkreis', '200699', '96943', '103756');
INSERT INTO kriminal.bevoelkerung VALUES (11, 2017, 1056, '      Pinneberg, Landkreis', '311658', '152857', '158801');
INSERT INTO kriminal.bevoelkerung VALUES (12, 2017, 1057, '      Pl�n, Landkreis', '128773', '62639', '66134');
INSERT INTO kriminal.bevoelkerung VALUES (13, 2017, 1058, '      Rendsburg-Eckernf�rde, Landkreis', '272680', '134241', '138439');
INSERT INTO kriminal.bevoelkerung VALUES (14, 2017, 1059, '      Schleswig-Flensburg, Landkreis', '199094', '98672', '100423');
INSERT INTO kriminal.bevoelkerung VALUES (15, 2017, 1060, '      Segeberg, Landkreis', '273130', '135086', '138044');
INSERT INTO kriminal.bevoelkerung VALUES (16, 2017, 1061, '      Steinburg, Landkreis', '131744', '65148', '66596');
INSERT INTO kriminal.bevoelkerung VALUES (17, 2017, 1062, '      Stormarn, Landkreis', '242142', '118368', '123774');
INSERT INTO kriminal.bevoelkerung VALUES (18, 2017, 2000, '  Hamburg', '1820511', '891748', '928763');
INSERT INTO kriminal.bevoelkerung VALUES (19, 2017, 3, '  Niedersachsen', '7954230', '3927636', '4026594');
INSERT INTO kriminal.bevoelkerung VALUES (20, 2017, 31, '    Braunschweig, Stat. Region', '1595672', '789459', '806213');
INSERT INTO kriminal.bevoelkerung VALUES (21, 2017, 3101, '      Braunschweig, Kreisfreie Stadt', '248345', '122768', '125578');
INSERT INTO kriminal.bevoelkerung VALUES (22, 2017, 3102, '      Salzgitter, Kreisfreie Stadt', '104108', '51759', '52349');
INSERT INTO kriminal.bevoelkerung VALUES (23, 2017, 3103, '      Wolfsburg, Kreisfreie Stadt', '123912', '61602', '62310');
INSERT INTO kriminal.bevoelkerung VALUES (24, 2017, 3151, '      Gifhorn, Landkreis', '174914', '87604', '87310');
INSERT INTO kriminal.bevoelkerung VALUES (25, 2017, 3152, '      G�ttingen, Landkreis', '-', '-', '-');
INSERT INTO kriminal.bevoelkerung VALUES (26, 2017, 3153, '      Goslar, Landkreis', '137771', '68112', '69660');
INSERT INTO kriminal.bevoelkerung VALUES (27, 2017, 3154, '      Helmstedt, Landkreis', '91900', '45565', '46335');
INSERT INTO kriminal.bevoelkerung VALUES (28, 2017, 3155, '      Northeim, Landkreis', '133328', '65621', '67707');
INSERT INTO kriminal.bevoelkerung VALUES (29, 2017, 3156, '      Osterode am Harz, Landkreis', '-', '-', '-');
INSERT INTO kriminal.bevoelkerung VALUES (30, 2017, 3157, '      Peine, Landkreis', '133174', '65825', '67349');
INSERT INTO kriminal.bevoelkerung VALUES (31, 2017, 3158, '      Wolfenb�ttel, Landkreis', '120671', '59705', '60966');
INSERT INTO kriminal.bevoelkerung VALUES (32, 2017, 3159, '      G�ttingen, Landkreis', '327551', '160900', '166651');
INSERT INTO kriminal.bevoelkerung VALUES (33, 2017, 32, '    Hannover, Stat. Region', '2142048', '1049091', '1092958');
INSERT INTO kriminal.bevoelkerung VALUES (34, 2017, 3241, '      Region Hannover, Landkreis', '1150688', '562643', '588045');
INSERT INTO kriminal.bevoelkerung VALUES (35, 2017, 3241001, '      Hannover, Landeshauptstadt', '533963', '260376', '273587');
INSERT INTO kriminal.bevoelkerung VALUES (36, 2017, 3251, '      Diepholz, Landkreis', '215547', '107078', '108469');
INSERT INTO kriminal.bevoelkerung VALUES (37, 2017, 3252, '      Hameln-Pyrmont, Landkreis', '148281', '71614', '76667');
INSERT INTO kriminal.bevoelkerung VALUES (38, 2017, 3254, '      Hildesheim, Landkreis', '276970', '134956', '142014');
INSERT INTO kriminal.bevoelkerung VALUES (39, 2017, 3255, '      Holzminden, Landkreis', '71327', '35303', '36025');
INSERT INTO kriminal.bevoelkerung VALUES (40, 2017, 3256, '      Nienburg (Weser), Landkreis', '121487', '60378', '61109');
INSERT INTO kriminal.bevoelkerung VALUES (41, 2017, 3257, '      Schaumburg, Landkreis', '157750', '77120', '80630');
INSERT INTO kriminal.bevoelkerung VALUES (42, 2017, 33, '    L�neburg, Stat. Region', '1705205', '842194', '863011');
INSERT INTO kriminal.bevoelkerung VALUES (43, 2017, 3351, '      Celle, Landkreis', '178567', '87803', '90764');
INSERT INTO kriminal.bevoelkerung VALUES (44, 2017, 3352, '      Cuxhaven, Landkreis', '198385', '97248', '101137');
INSERT INTO kriminal.bevoelkerung VALUES (45, 2017, 3353, '      Harburg, Landkreis', '250919', '123690', '127229');
INSERT INTO kriminal.bevoelkerung VALUES (46, 2017, 3354, '      L�chow-Dannenberg, Landkreis', '48591', '23909', '24683');
INSERT INTO kriminal.bevoelkerung VALUES (47, 2017, 3355, '      L�neburg, Landkreis', '182268', '89121', '93147');
INSERT INTO kriminal.bevoelkerung VALUES (48, 2017, 3356, '      Osterholz, Landkreis', '112900', '55540', '57360');
INSERT INTO kriminal.bevoelkerung VALUES (49, 2017, 3357, '      Rotenburg (W�mme), Landkreis', '163375', '82271', '81104');
INSERT INTO kriminal.bevoelkerung VALUES (50, 2017, 3358, '      Heidekreis, Landkreis', '139370', '69625', '69745');
INSERT INTO kriminal.bevoelkerung VALUES (51, 2017, 3359, '      Stade, Landkreis', '201763', '100498', '101265');
INSERT INTO kriminal.bevoelkerung VALUES (52, 2017, 3360, '      Uelzen, Landkreis', '92853', '45444', '47409');
INSERT INTO kriminal.bevoelkerung VALUES (53, 2017, 3361, '      Verden, Landkreis', '136216', '67046', '69170');
INSERT INTO kriminal.bevoelkerung VALUES (54, 2017, 34, '    Weser-Ems, Stat. Region', '2511306', '1246894', '1264413');
INSERT INTO kriminal.bevoelkerung VALUES (55, 2017, 3401, '      Delmenhorst, Kreisfreie Stadt', '77283', '38276', '39007');
INSERT INTO kriminal.bevoelkerung VALUES (56, 2017, 3402, '      Emden, Kreisfreie Stadt', '50547', '25272', '25275');
INSERT INTO kriminal.bevoelkerung VALUES (57, 2017, 3403, '      Oldenburg (Oldenburg), Kreisfreie Stadt', '166396', '79479', '86917');
INSERT INTO kriminal.bevoelkerung VALUES (58, 2017, 3404, '      Osnabr�ck, Kreisfreie Stadt', '164222', '79468', '84755');
INSERT INTO kriminal.bevoelkerung VALUES (59, 2017, 3405, '      Wilhelmshaven, Kreisfreie Stadt', '76259', '37542', '38717');
INSERT INTO kriminal.bevoelkerung VALUES (60, 2017, 3451, '      Ammerland, Landkreis', '123038', '60292', '62746');
INSERT INTO kriminal.bevoelkerung VALUES (61, 2017, 3452, '      Aurich, Landkreis', '190008', '93767', '96241');
INSERT INTO kriminal.bevoelkerung VALUES (62, 2017, 3453, '      Cloppenburg, Landkreis', '166928', '84370', '82558');
INSERT INTO kriminal.bevoelkerung VALUES (63, 2017, 3454, '      Emsland, Landkreis', '322514', '163930', '158584');
INSERT INTO kriminal.bevoelkerung VALUES (64, 2017, 3455, '      Friesland, Landkreis', '98459', '48091', '50368');
INSERT INTO kriminal.bevoelkerung VALUES (65, 2017, 3456, '      Grafschaft Bentheim, Landkreis', '135815', '67913', '67902');
INSERT INTO kriminal.bevoelkerung VALUES (66, 2017, 3457, '      Leer, Landkreis', '168600', '84047', '84553');
INSERT INTO kriminal.bevoelkerung VALUES (67, 2017, 3458, '      Oldenburg, Landkreis', '129704', '64491', '65213');
INSERT INTO kriminal.bevoelkerung VALUES (68, 2017, 3459, '      Osnabr�ck, Landkreis', '355474', '176491', '178983');
INSERT INTO kriminal.bevoelkerung VALUES (69, 2017, 3460, '      Vechta, Landkreis', '140106', '70888', '69218');
INSERT INTO kriminal.bevoelkerung VALUES (70, 2017, 3461, '      Wesermarsch, Landkreis', '89152', '44676', '44477');
INSERT INTO kriminal.bevoelkerung VALUES (71, 2017, 3462, '      Wittmund, Landkreis', '56806', '27904', '28903');
INSERT INTO kriminal.bevoelkerung VALUES (72, 2017, 4000, '  Bremen', '679893', '336070', '343823');
INSERT INTO kriminal.bevoelkerung VALUES (73, 2017, 4011, '      Bremen, Kreisfreie Stadt', '566863', '279827', '287036');
INSERT INTO kriminal.bevoelkerung VALUES (74, 2017, 4012, '      Bremerhaven, Kreisfreie Stadt', '113030', '56243', '56788');
INSERT INTO kriminal.bevoelkerung VALUES (75, 2017, 5, '  Nordrhein-Westfalen', '17901117', '8782170', '9118948');
INSERT INTO kriminal.bevoelkerung VALUES (76, 2017, 51, '    D�sseldorf, Regierungsbezirk', '5194805', '2534811', '2659995');
INSERT INTO kriminal.bevoelkerung VALUES (77, 2017, 5111, '      D�sseldorf, Kreisfreie Stadt', '615255', '297281', '317975');
INSERT INTO kriminal.bevoelkerung VALUES (78, 2017, 5112, '      Duisburg, Kreisfreie Stadt', '498978', '246641', '252337');
INSERT INTO kriminal.bevoelkerung VALUES (79, 2017, 5113, '      Essen, Kreisfreie Stadt', '583239', '283136', '300103');
INSERT INTO kriminal.bevoelkerung VALUES (80, 2017, 5114, '      Krefeld, Kreisfreie Stadt', '226756', '110530', '116226');
INSERT INTO kriminal.bevoelkerung VALUES (81, 2017, 5116, '      M�nchengladbach, Kreisfreie Stadt', '261557', '128547', '133010');
INSERT INTO kriminal.bevoelkerung VALUES (82, 2017, 5117, '      M�lheim an der Ruhr, Kreisfreie Stadt', '171101', '82524', '88577');
INSERT INTO kriminal.bevoelkerung VALUES (83, 2017, 5119, '      Oberhausen, Kreisfreie Stadt', '211402', '103786', '107617');
INSERT INTO kriminal.bevoelkerung VALUES (84, 2017, 5120, '      Remscheid, Kreisfreie Stadt', '110598', '54508', '56090');
INSERT INTO kriminal.bevoelkerung VALUES (85, 2017, 5122, '      Solingen, Kreisfreie Stadt', '158856', '77142', '81714');
INSERT INTO kriminal.bevoelkerung VALUES (86, 2017, 5124, '      Wuppertal, Kreisfreie Stadt', '352990', '172640', '180350');
INSERT INTO kriminal.bevoelkerung VALUES (87, 2017, 5154, '      Kleve, Kreis', '310800', '154890', '155910');
INSERT INTO kriminal.bevoelkerung VALUES (88, 2017, 5158, '      Mettmann, Kreis', '485090', '234411', '250679');
INSERT INTO kriminal.bevoelkerung VALUES (89, 2017, 5162, '      Rhein-Kreis Neuss', '448420', '217879', '230541');
INSERT INTO kriminal.bevoelkerung VALUES (90, 2017, 5166, '      Viersen, Kreis', '298578', '145885', '152693');
INSERT INTO kriminal.bevoelkerung VALUES (91, 2017, 5170, '      Wesel, Kreis', '461191', '225014', '236177');
INSERT INTO kriminal.bevoelkerung VALUES (92, 2017, 53, '    K�ln, Regierungsbezirk', '4446822', '2182086', '2264736');
INSERT INTO kriminal.bevoelkerung VALUES (93, 2017, 5314, '      Bonn, Kreisfreie Stadt', '323808', '154549', '169259');
INSERT INTO kriminal.bevoelkerung VALUES (94, 2017, 5315, '      K�ln, Kreisfreie Stadt', '1078165', '525878', '552287');
INSERT INTO kriminal.bevoelkerung VALUES (95, 2017, 5316, '      Leverkusen, Kreisfreie Stadt', '163345', '79509', '83836');
INSERT INTO kriminal.bevoelkerung VALUES (96, 2017, 5334, '      St�dteregion Aachen (einschl. Stadt Aachen)', '553270', '278927', '274344');
INSERT INTO kriminal.bevoelkerung VALUES (97, 2017, 5334002, '      Aachen, krfr. Stadt', '245612', '127655', '117957');
INSERT INTO kriminal.bevoelkerung VALUES (98, 2017, 5354, '      Aachen, Kreis', '-', '-', '-');
INSERT INTO kriminal.bevoelkerung VALUES (99, 2017, 5358, '      D�ren, Kreis', '262481', '130054', '132427');
INSERT INTO kriminal.bevoelkerung VALUES (100, 2017, 5362, '      Rhein-Erft-Kreis', '466379', '228046', '238334');
INSERT INTO kriminal.bevoelkerung VALUES (101, 2017, 5366, '      Euskirchen, Kreis', '191665', '94657', '97008');
INSERT INTO kriminal.bevoelkerung VALUES (102, 2017, 5370, '      Heinsberg, Kreis', '252879', '125121', '127758');
INSERT INTO kriminal.bevoelkerung VALUES (103, 2017, 5374, '      Oberbergischer Kreis', '273054', '134363', '138691');
INSERT INTO kriminal.bevoelkerung VALUES (104, 2017, 5378, '      Rheinisch-Bergischer Kreis', '283324', '137595', '145729');
INSERT INTO kriminal.bevoelkerung VALUES (105, 2017, 5382, '      Rhein-Sieg-Kreis', '598455', '293390', '305065');
INSERT INTO kriminal.bevoelkerung VALUES (106, 2017, 55, '    M�nster, Regierungsbezirk', '2620265', '1290123', '1330142');
INSERT INTO kriminal.bevoelkerung VALUES (107, 2017, 5512, '      Bottrop, Kreisfreie Stadt', '117387', '57156', '60231');
INSERT INTO kriminal.bevoelkerung VALUES (108, 2017, 5513, '      Gelsenkirchen, Kreisfreie Stadt', '261417', '130162', '131255');
INSERT INTO kriminal.bevoelkerung VALUES (109, 2017, 5515, '      M�nster, Kreisfreie Stadt', '312703', '149886', '162817');
INSERT INTO kriminal.bevoelkerung VALUES (110, 2017, 5554, '      Borken, Kreis', '369472', '184510', '184962');
INSERT INTO kriminal.bevoelkerung VALUES (111, 2017, 5558, '      Coesfeld, Kreis', '219190', '108236', '110954');
INSERT INTO kriminal.bevoelkerung VALUES (112, 2017, 5562, '      Recklinghausen, Kreis', '617010', '301151', '315859');
INSERT INTO kriminal.bevoelkerung VALUES (113, 2017, 5566, '      Steinfurt, Kreis', '445487', '221804', '223684');
INSERT INTO kriminal.bevoelkerung VALUES (114, 2017, 5570, '      Warendorf, Kreis', '277601', '137220', '140381');
INSERT INTO kriminal.bevoelkerung VALUES (115, 2017, 57, '    Detmold, Regierungsbezirk', '2054274', '1013311', '1040963');
INSERT INTO kriminal.bevoelkerung VALUES (116, 2017, 5711, '      Bielefeld, Kreisfreie Stadt', '333002', '160688', '172314');
INSERT INTO kriminal.bevoelkerung VALUES (117, 2017, 5754, '      G�tersloh, Kreis', '362439', '181086', '181353');
INSERT INTO kriminal.bevoelkerung VALUES (118, 2017, 5758, '      Herford, Kreis', '251307', '123237', '128070');
INSERT INTO kriminal.bevoelkerung VALUES (119, 2017, 5762, '      H�xter, Kreis', '141710', '70563', '71147');
INSERT INTO kriminal.bevoelkerung VALUES (120, 2017, 5766, '      Lippe, Kreis', '349001', '171238', '177764');
INSERT INTO kriminal.bevoelkerung VALUES (121, 2017, 5770, '      Minden-L�bbecke, Kreis', '311537', '153663', '157874');
INSERT INTO kriminal.bevoelkerung VALUES (122, 2017, 5774, '      Paderborn, Kreis', '305280', '152837', '152443');
INSERT INTO kriminal.bevoelkerung VALUES (123, 2017, 59, '    Arnsberg, Regierungsbezirk', '3584952', '1761839', '1823113');
INSERT INTO kriminal.bevoelkerung VALUES (124, 2017, 5911, '      Bochum, Kreisfreie Stadt', '365225', '177857', '187368');
INSERT INTO kriminal.bevoelkerung VALUES (125, 2017, 5913, '      Dortmund, Kreisfreie Stadt', '586207', '287987', '298220');
INSERT INTO kriminal.bevoelkerung VALUES (126, 2017, 5914, '      Hagen, Kreisfreie Stadt', '187998', '91560', '96438');
INSERT INTO kriminal.bevoelkerung VALUES (127, 2017, 5915, '      Hamm, Kreisfreie Stadt', '179378', '88111', '91268');
INSERT INTO kriminal.bevoelkerung VALUES (128, 2017, 5916, '      Herne, Kreisfreie Stadt', '156632', '76705', '79927');
INSERT INTO kriminal.bevoelkerung VALUES (129, 2017, 5954, '      Ennepe-Ruhr-Kreis', '325022', '157837', '167185');
INSERT INTO kriminal.bevoelkerung VALUES (130, 2017, 5958, '      Hochsauerlandkreis', '261930', '130568', '131363');
INSERT INTO kriminal.bevoelkerung VALUES (131, 2017, 5962, '      M�rkischer Kreis', '414135', '203938', '210197');
INSERT INTO kriminal.bevoelkerung VALUES (132, 2017, 5966, '      Olpe, Kreis', '134896', '67562', '67334');
INSERT INTO kriminal.bevoelkerung VALUES (133, 2017, 5970, '      Siegen-Wittgenstein, Kreis', '277955', '138069', '139887');
INSERT INTO kriminal.bevoelkerung VALUES (134, 2017, 5974, '      Soest, Kreis', '301674', '149905', '151770');
INSERT INTO kriminal.bevoelkerung VALUES (135, 2017, 5978, '      Unna, Kreis', '393902', '191743', '202159');
INSERT INTO kriminal.bevoelkerung VALUES (136, 2017, 6, '  Hessen', '6228175', '3074297', '3153879');
INSERT INTO kriminal.bevoelkerung VALUES (137, 2017, 64, '    Darmstadt, Regierungsbezirk', '3964642', '1954353', '2010289');
INSERT INTO kriminal.bevoelkerung VALUES (138, 2017, 6411, '      Darmstadt, Kreisfreie Stadt', '157846', '80374', '77472');
INSERT INTO kriminal.bevoelkerung VALUES (139, 2017, 6412, '      Frankfurt am Main, Kreisfreie Stadt', '741646', '366238', '375409');
INSERT INTO kriminal.bevoelkerung VALUES (140, 2017, 6413, '      Offenbach am Main, Kreisfreie Stadt', '125624', '62048', '63576');
INSERT INTO kriminal.bevoelkerung VALUES (141, 2017, 6414, '      Wiesbaden, Landeshauptstadt, Kreisfreie Stadt', '278137', '132872', '145265');
INSERT INTO kriminal.bevoelkerung VALUES (142, 2017, 6431, '      Bergstra�e, Landkreis', '268358', '132654', '135704');
INSERT INTO kriminal.bevoelkerung VALUES (143, 2017, 6432, '      Darmstadt-Dieburg, Landkreis', '295396', '146823', '148573');
INSERT INTO kriminal.bevoelkerung VALUES (144, 2017, 6433, '      Gro�-Gerau, Landkreis', '270224', '134854', '135371');
INSERT INTO kriminal.bevoelkerung VALUES (145, 2017, 6434, '      Hochtaunuskreis', '235493', '114151', '121342');
INSERT INTO kriminal.bevoelkerung VALUES (146, 2017, 6435, '      Main-Kinzig-Kreis', '417462', '206021', '211441');
INSERT INTO kriminal.bevoelkerung VALUES (147, 2017, 6436, '      Main-Taunus-Kreis', '236339', '115566', '120773');
INSERT INTO kriminal.bevoelkerung VALUES (148, 2017, 6437, '      Odenwaldkreis', '96535', '47705', '48831');
INSERT INTO kriminal.bevoelkerung VALUES (149, 2017, 6438, '      Offenbach, Landkreis', '350837', '172805', '178032');
INSERT INTO kriminal.bevoelkerung VALUES (150, 2017, 6439, '      Rheingau-Taunus-Kreis', '186135', '91601', '94535');
INSERT INTO kriminal.bevoelkerung VALUES (151, 2017, 6440, '      Wetteraukreis', '304613', '150645', '153968');
INSERT INTO kriminal.bevoelkerung VALUES (152, 2017, 65, '    Gie�en, Regierungsbezirk', '1044725', '516093', '528633');
INSERT INTO kriminal.bevoelkerung VALUES (153, 2017, 6531, '      Gie�en, Landkreis', '266378', '131026', '135352');
INSERT INTO kriminal.bevoelkerung VALUES (154, 2017, 6532, '      Lahn-Dill-Kreis', '254119', '125310', '128809');
INSERT INTO kriminal.bevoelkerung VALUES (155, 2017, 6533, '      Limburg-Weilburg, Landkreis', '172046', '85465', '86581');
INSERT INTO kriminal.bevoelkerung VALUES (156, 2017, 6534, '      Marburg-Biedenkopf, Landkreis', '245589', '120913', '124677');
INSERT INTO kriminal.bevoelkerung VALUES (157, 2017, 6535, '      Vogelsbergkreis', '106594', '53380', '53214');
INSERT INTO kriminal.bevoelkerung VALUES (158, 2017, 66, '    Kassel, Regierungsbezirk', '1218808', '603851', '614957');
INSERT INTO kriminal.bevoelkerung VALUES (159, 2017, 6611, '      Kassel, Kreisfreie Stadt', '199899', '97800', '102100');
INSERT INTO kriminal.bevoelkerung VALUES (160, 2017, 6631, '      Fulda, Landkreis', '221477', '109912', '111565');
INSERT INTO kriminal.bevoelkerung VALUES (161, 2017, 6632, '      Hersfeld-Rotenburg, Landkreis', '121069', '60527', '60542');
INSERT INTO kriminal.bevoelkerung VALUES (162, 2017, 6633, '      Kassel, Landkreis', '236790', '116392', '120398');
INSERT INTO kriminal.bevoelkerung VALUES (163, 2017, 6634, '      Schwalm-Eder-Kreis', '180930', '90638', '90292');
INSERT INTO kriminal.bevoelkerung VALUES (164, 2017, 6635, '      Waldeck-Frankenberg, Landkreis', '157612', '78698', '78914');
INSERT INTO kriminal.bevoelkerung VALUES (165, 2017, 6636, '      Werra-Mei�ner-Kreis', '101033', '49886', '51148');
INSERT INTO kriminal.bevoelkerung VALUES (166, 2017, 7, '  Rheinland-Pfalz', '4069866', '2008813', '2061053');
INSERT INTO kriminal.bevoelkerung VALUES (167, 2017, 71, '    Koblenz, Stat. Region', '1492623', '736393', '756231');
INSERT INTO kriminal.bevoelkerung VALUES (168, 2017, 7111, '      Koblenz, Kreisfreie Stadt', '113725', '55214', '58511');
INSERT INTO kriminal.bevoelkerung VALUES (169, 2017, 7131, '      Ahrweiler, Landkreis', '128685', '63111', '65574');
INSERT INTO kriminal.bevoelkerung VALUES (170, 2017, 7132, '      Altenkirchen (Westerwald), Landkreis', '128876', '63813', '65063');
INSERT INTO kriminal.bevoelkerung VALUES (171, 2017, 7133, '      Bad Kreuznach, Landkreis', '157471', '76862', '80609');
INSERT INTO kriminal.bevoelkerung VALUES (172, 2017, 7134, '      Birkenfeld, Landkreis', '80764', '40043', '40721');
INSERT INTO kriminal.bevoelkerung VALUES (173, 2017, 7135, '      Cochem-Zell, Landkreis', '61763', '30922', '30841');
INSERT INTO kriminal.bevoelkerung VALUES (174, 2017, 7137, '      Mayen-Koblenz, Landkreis', '213261', '105314', '107947');
INSERT INTO kriminal.bevoelkerung VALUES (175, 2017, 7138, '      Neuwied, Landkreis', '181596', '89306', '92291');
INSERT INTO kriminal.bevoelkerung VALUES (176, 2017, 7140, '      Rhein-Hunsr�ck-Kreis', '102982', '51228', '51754');
INSERT INTO kriminal.bevoelkerung VALUES (177, 2017, 7141, '      Rhein-Lahn-Kreis', '122467', '60614', '61853');
INSERT INTO kriminal.bevoelkerung VALUES (178, 2017, 7143, '      Westerwaldkreis', '201033', '99966', '101067');
INSERT INTO kriminal.bevoelkerung VALUES (179, 2017, 72, '    Trier, Stat. Region', '529163', '263165', '266001');
INSERT INTO kriminal.bevoelkerung VALUES (180, 2017, 7211, '      Trier, Kreisfreie Stadt', '110062', '54206', '55856');
INSERT INTO kriminal.bevoelkerung VALUES (181, 2017, 7231, '      Bernkastel-Wittlich, Landkreis', '112070', '55952', '56119');
INSERT INTO kriminal.bevoelkerung VALUES (182, 2017, 7232, '      Eifelkreis Bitburg-Pr�m', '98117', '49309', '48809');
INSERT INTO kriminal.bevoelkerung VALUES (183, 2017, 7233, '      Vulkaneifel, Landkreis', '60731', '30449', '30283');
INSERT INTO kriminal.bevoelkerung VALUES (184, 2017, 7235, '      Trier-Saarburg, Landkreis', '148183', '73249', '74934');
INSERT INTO kriminal.bevoelkerung VALUES (185, 2017, 73, '    Rheinhessen-Pfalz, Stat. Region', '2048086', '1009263', '1038829');
INSERT INTO kriminal.bevoelkerung VALUES (186, 2017, 7311, '      Frankenthal (Pfalz), Kreisfreie Stadt', '48431', '23433', '24999');
INSERT INTO kriminal.bevoelkerung VALUES (187, 2017, 7312, '      Kaiserslautern, Kreisfreie Stadt', '99493', '50158', '49336');
INSERT INTO kriminal.bevoelkerung VALUES (188, 2017, 7313, '      Landau in der Pfalz, Kreisfreie Stadt', '46149', '21819', '24331');
INSERT INTO kriminal.bevoelkerung VALUES (189, 2017, 7314, '      Ludwigshafen am Rhein, Kreisfreie Stadt', '167559', '83261', '84298');
INSERT INTO kriminal.bevoelkerung VALUES (190, 2017, 7315, '      Mainz, Kreisfreie Stadt', '214319', '104213', '110107');
INSERT INTO kriminal.bevoelkerung VALUES (191, 2017, 7316, '      Neustadt an der Weinstra�e, Kreisfreie Stadt', '53281', '25935', '27346');
INSERT INTO kriminal.bevoelkerung VALUES (192, 2017, 7317, '      Pirmasens, Kreisfreie Stadt', '40524', '19797', '20728');
INSERT INTO kriminal.bevoelkerung VALUES (193, 2017, 7318, '      Speyer, Kreisfreie Stadt', '50741', '24586', '26155');
INSERT INTO kriminal.bevoelkerung VALUES (194, 2017, 7319, '      Worms, Kreisfreie Stadt', '82838', '40706', '42132');
INSERT INTO kriminal.bevoelkerung VALUES (195, 2017, 7320, '      Zweibr�cken, Kreisfreie Stadt', '34349', '16974', '17375');
INSERT INTO kriminal.bevoelkerung VALUES (196, 2017, 7331, '      Alzey-Worms, Landkreis', '128358', '64023', '64335');
INSERT INTO kriminal.bevoelkerung VALUES (197, 2017, 7332, '      Bad D�rkheim, Landkreis', '132850', '65060', '67790');
INSERT INTO kriminal.bevoelkerung VALUES (198, 2017, 7333, '      Donnersbergkreis', '75153', '37262', '37891');
INSERT INTO kriminal.bevoelkerung VALUES (199, 2017, 7334, '      Germersheim, Landkreis', '128341', '63986', '64356');
INSERT INTO kriminal.bevoelkerung VALUES (200, 2017, 7335, '      Kaiserslautern, Landkreis', '105577', '52002', '53575');
INSERT INTO kriminal.bevoelkerung VALUES (201, 2017, 7336, '      Kusel, Landkreis', '70832', '35175', '35657');
INSERT INTO kriminal.bevoelkerung VALUES (202, 2017, 7337, '      S�dliche Weinstra�e, Landkreis', '110754', '54684', '56070');
INSERT INTO kriminal.bevoelkerung VALUES (203, 2017, 7338, '      Rhein-Pfalz-Kreis', '153336', '75584', '77752');
INSERT INTO kriminal.bevoelkerung VALUES (204, 2017, 7339, '      Mainz-Bingen, Landkreis', '209485', '103310', '106175');
INSERT INTO kriminal.bevoelkerung VALUES (205, 2017, 7340, '      S�dwestpfalz, Landkreis', '95716', '47295', '48421');
INSERT INTO kriminal.bevoelkerung VALUES (206, 2017, 8, '  Baden-W�rttemberg, Land', '10987659', '5456503', '5531156');
INSERT INTO kriminal.bevoelkerung VALUES (207, 2017, 81, '    Stuttgart, Regierungsbezirk', '4112483', '2049621', '2062862');
INSERT INTO kriminal.bevoelkerung VALUES (208, 2017, 8111, '      Stuttgart, Landeshauptstadt, Stadtkreis', '630388', '314711', '315677');
INSERT INTO kriminal.bevoelkerung VALUES (209, 2017, 8115, '      B�blingen, Landkreis', '387718', '192680', '195038');
INSERT INTO kriminal.bevoelkerung VALUES (210, 2017, 8116, '      Esslingen, Landkreis', '530620', '264832', '265788');
INSERT INTO kriminal.bevoelkerung VALUES (211, 2017, 8117, '      G�ppingen, Landkreis', '255482', '127174', '128308');
INSERT INTO kriminal.bevoelkerung VALUES (212, 2017, 8118, '      Ludwigsburg, Landkreis', '540266', '267940', '272327');
INSERT INTO kriminal.bevoelkerung VALUES (213, 2017, 8119, '      Rems-Murr-Kreis, Landkreis', '423788', '209553', '214235');
INSERT INTO kriminal.bevoelkerung VALUES (214, 2017, 8121, '      Heilbronn, Stadtkreis', '124442', '62628', '61814');
INSERT INTO kriminal.bevoelkerung VALUES (215, 2017, 8125, '      Heilbronn, Landkreis', '339172', '170199', '168973');
INSERT INTO kriminal.bevoelkerung VALUES (216, 2017, 8126, '      Hohenlohekreis, Landkreis', '111041', '56049', '54992');
INSERT INTO kriminal.bevoelkerung VALUES (217, 2017, 8127, '      Schw�bisch Hall, Landkreis', '193581', '97104', '96477');
INSERT INTO kriminal.bevoelkerung VALUES (218, 2017, 8128, '      Main-Tauber-Kreis, Landkreis', '132232', '65869', '66363');
INSERT INTO kriminal.bevoelkerung VALUES (219, 2017, 8135, '      Heidenheim, Landkreis', '131752', '65586', '66166');
INSERT INTO kriminal.bevoelkerung VALUES (220, 2017, 8136, '      Ostalbkreis, Landkreis', '312005', '155300', '156705');
INSERT INTO kriminal.bevoelkerung VALUES (221, 2017, 82, '    Karlsruhe, Regierungsbzirk', '2787549', '1385109', '1402440');
INSERT INTO kriminal.bevoelkerung VALUES (222, 2017, 8211, '      Baden-Baden, Stadtkreis', '54513', '26093', '28420');
INSERT INTO kriminal.bevoelkerung VALUES (223, 2017, 8212, '      Karlsruhe, Stadtkreis', '310959', '158933', '152026');
INSERT INTO kriminal.bevoelkerung VALUES (224, 2017, 8215, '      Karlsruhe, Landkreis', '441505', '219982', '221524');
INSERT INTO kriminal.bevoelkerung VALUES (225, 2017, 8216, '      Rastatt, Landkreis', '229562', '113884', '115678');
INSERT INTO kriminal.bevoelkerung VALUES (226, 2017, 8221, '      Heidelberg, Stadtkreis', '160258', '77243', '83015');
INSERT INTO kriminal.bevoelkerung VALUES (227, 2017, 8222, '      Mannheim, Stadtkreis', '306389', '152933', '153456');
INSERT INTO kriminal.bevoelkerung VALUES (228, 2017, 8225, '      Neckar-Odenwald-Kreis, Landkreis', '143327', '71978', '71350');
INSERT INTO kriminal.bevoelkerung VALUES (229, 2017, 8226, '      Rhein-Neckar-Kreis, Landkreis', '545573', '268254', '277319');
INSERT INTO kriminal.bevoelkerung VALUES (230, 2017, 8231, '      Pforzheim, Stadtkreis', '123891', '60807', '63084');
INSERT INTO kriminal.bevoelkerung VALUES (231, 2017, 8235, '      Calw, Landkreis', '156692', '78575', '78117');
INSERT INTO kriminal.bevoelkerung VALUES (232, 2017, 8236, '      Enzkreis, Landkreis', '197808', '98053', '99755');
INSERT INTO kriminal.bevoelkerung VALUES (233, 2017, 8237, '      Freudenstadt, Landkreis', '117074', '58376', '58699');
INSERT INTO kriminal.bevoelkerung VALUES (234, 2017, 83, '    Freiburg, Regierungsbezirk', '2247204', '1107192', '1140013');
INSERT INTO kriminal.bevoelkerung VALUES (235, 2017, 8311, '      Freiburg im Breisgau, Stadtkreis', '228613', '109020', '119593');
INSERT INTO kriminal.bevoelkerung VALUES (236, 2017, 8315, '      Breisgau-Hochschwarzwald, Landkreis', '261411', '128885', '132527');
INSERT INTO kriminal.bevoelkerung VALUES (237, 2017, 8316, '      Emmendingen, Landkreis', '163982', '80942', '83040');
INSERT INTO kriminal.bevoelkerung VALUES (238, 2017, 8317, '      Ortenaukreis, Landkreis', '424646', '211219', '213427');
INSERT INTO kriminal.bevoelkerung VALUES (239, 2017, 8325, '      Rottweil, Landkreis', '138593', '69241', '69352');
INSERT INTO kriminal.bevoelkerung VALUES (240, 2017, 8326, '      Schwarzwald-Baar-Kreis, Landkreis', '210646', '104617', '106029');
INSERT INTO kriminal.bevoelkerung VALUES (241, 2017, 8327, '      Tuttlingen, Landkreis', '138758', '69645', '69114');
INSERT INTO kriminal.bevoelkerung VALUES (242, 2017, 8335, '      Konstanz, Landkreis', '283103', '137968', '145135');
INSERT INTO kriminal.bevoelkerung VALUES (243, 2017, 8336, '      L�rrach, Landkreis', '227930', '111633', '116297');
INSERT INTO kriminal.bevoelkerung VALUES (244, 2017, 8337, '      Waldshut, Landkreis', '169525', '84025', '85501');
INSERT INTO kriminal.bevoelkerung VALUES (245, 2017, 84, '    T�bingen, Regierungsbezirk', '1840424', '914582', '925842');
INSERT INTO kriminal.bevoelkerung VALUES (246, 2017, 8415, '      Reutlingen, Landkreis', '284918', '141350', '143568');
INSERT INTO kriminal.bevoelkerung VALUES (247, 2017, 8416, '      T�bingen, Landkreis', '225195', '109794', '115402');
INSERT INTO kriminal.bevoelkerung VALUES (248, 2017, 8417, '      Zollernalbkreis, Landkreis', '187786', '93044', '94742');
INSERT INTO kriminal.bevoelkerung VALUES (249, 2017, 8421, '      Ulm, Stadtkreis', '124775', '61783', '62992');
INSERT INTO kriminal.bevoelkerung VALUES (250, 2017, 8425, '      Alb-Donau-Kreis, Landkreis', '193974', '97941', '96033');
INSERT INTO kriminal.bevoelkerung VALUES (251, 2017, 8426, '      Biberach, Landkreis', '197236', '99280', '97956');
INSERT INTO kriminal.bevoelkerung VALUES (252, 2017, 8435, '      Bodenseekreis, Landkreis', '213863', '105370', '108493');
INSERT INTO kriminal.bevoelkerung VALUES (253, 2017, 8436, '      Ravensburg, Landkreis', '282446', '140675', '141771');
INSERT INTO kriminal.bevoelkerung VALUES (254, 2017, 8437, '      Sigmaringen, Landkreis', '130234', '65346', '64888');
INSERT INTO kriminal.bevoelkerung VALUES (255, 2017, 9, '  Bayern', '12963978', '6419662', '6544316');
INSERT INTO kriminal.bevoelkerung VALUES (256, 2017, 91, '    Oberbayern, Regierungsbezirk', '4641429', '2294670', '2346759');
INSERT INTO kriminal.bevoelkerung VALUES (257, 2017, 9161, '      Ingolstadt', '134442', '67838', '66604');
INSERT INTO kriminal.bevoelkerung VALUES (258, 2017, 9162, '      M�nchen, Landeshauptstadt', '1460170', '711267', '748903');
INSERT INTO kriminal.bevoelkerung VALUES (259, 2017, 9163, '      Rosenheim', '62876', '30943', '31933');
INSERT INTO kriminal.bevoelkerung VALUES (260, 2017, 9171, '      Alt�tting, Landkreis', '109880', '54172', '55708');
INSERT INTO kriminal.bevoelkerung VALUES (261, 2017, 9172, '      Berchtesgadener Land, Landkreis', '104766', '51432', '53335');
INSERT INTO kriminal.bevoelkerung VALUES (262, 2017, 9173, '      Bad T�lz-Wolfratshausen, Landkreis', '126120', '62156', '63965');
INSERT INTO kriminal.bevoelkerung VALUES (448, 2017, 14290, '      Wei�eritzkreis', '-', '-', '-');
INSERT INTO kriminal.bevoelkerung VALUES (263, 2017, 9174, '      Dachau, Landkreis', '151771', '75920', '75851');
INSERT INTO kriminal.bevoelkerung VALUES (264, 2017, 9175, '      Ebersberg, Landkreis', '139908', '69781', '70127');
INSERT INTO kriminal.bevoelkerung VALUES (265, 2017, 9176, '      Eichst�tt, Landkreis', '131227', '66766', '64461');
INSERT INTO kriminal.bevoelkerung VALUES (266, 2017, 9177, '      Erding, Landkreis', '136157', '68648', '67509');
INSERT INTO kriminal.bevoelkerung VALUES (267, 2017, 9178, '      Freising, Landkreis', '176900', '90159', '86741');
INSERT INTO kriminal.bevoelkerung VALUES (268, 2017, 9179, '      F�rstenfeldbruck, Landkreis', '217344', '106481', '110864');
INSERT INTO kriminal.bevoelkerung VALUES (269, 2017, 9180, '      Garmisch-Partenkirchen, Landkreis', '88016', '42695', '45321');
INSERT INTO kriminal.bevoelkerung VALUES (270, 2017, 9181, '      Landsberg am Lech, Landkreis', '118933', '59604', '59329');
INSERT INTO kriminal.bevoelkerung VALUES (271, 2017, 9182, '      Miesbach, Landkreis', '99009', '48440', '50569');
INSERT INTO kriminal.bevoelkerung VALUES (272, 2017, 9183, '      M�hldorf a.Inn, Landkreis', '113854', '57587', '56268');
INSERT INTO kriminal.bevoelkerung VALUES (273, 2017, 9184, '      M�nchen, Landkreis', '344919', '171307', '173613');
INSERT INTO kriminal.bevoelkerung VALUES (274, 2017, 9185, '      Neuburg-Schrobenhausen, Landkreis', '95962', '48349', '47613');
INSERT INTO kriminal.bevoelkerung VALUES (275, 2017, 9186, '      Pfaffenhofen a.d.Ilm, Landkreis', '125665', '64032', '61633');
INSERT INTO kriminal.bevoelkerung VALUES (276, 2017, 9187, '      Rosenheim, Landkreis', '258458', '128001', '130457');
INSERT INTO kriminal.bevoelkerung VALUES (277, 2017, 9188, '      Starnberg, Landkreis', '135139', '65499', '69640');
INSERT INTO kriminal.bevoelkerung VALUES (278, 2017, 9189, '      Traunstein, Landkreis', '175861', '86949', '88912');
INSERT INTO kriminal.bevoelkerung VALUES (279, 2017, 9190, '      Weilheim-Schongau, Landkreis', '134056', '66649', '67407');
INSERT INTO kriminal.bevoelkerung VALUES (280, 2017, 92, '    Niederbayern, Regierungsbezirk', '1224717', '613151', '611567');
INSERT INTO kriminal.bevoelkerung VALUES (281, 2017, 9261, '      Landshut', '70609', '34272', '36337');
INSERT INTO kriminal.bevoelkerung VALUES (282, 2017, 9262, '      Passau', '51428', '24622', '26806');
INSERT INTO kriminal.bevoelkerung VALUES (283, 2017, 9263, '      Straubing', '47364', '23820', '23545');
INSERT INTO kriminal.bevoelkerung VALUES (284, 2017, 9271, '      Deggendorf, Landkreis', '118171', '58825', '59346');
INSERT INTO kriminal.bevoelkerung VALUES (285, 2017, 9272, '      Freyung-Grafenau, Landkreis', '78263', '39050', '39213');
INSERT INTO kriminal.bevoelkerung VALUES (286, 2017, 9273, '      Kelheim, Landkreis', '120544', '61204', '59341');
INSERT INTO kriminal.bevoelkerung VALUES (287, 2017, 9274, '      Landshut, Landkreis', '156341', '79362', '76979');
INSERT INTO kriminal.bevoelkerung VALUES (288, 2017, 9275, '      Passau, Landkreis', '189704', '94141', '95563');
INSERT INTO kriminal.bevoelkerung VALUES (289, 2017, 9276, '      Regen, Landkreis', '77338', '38624', '38715');
INSERT INTO kriminal.bevoelkerung VALUES (290, 2017, 9277, '      Rottal-Inn, Landkreis', '119994', '59889', '60105');
INSERT INTO kriminal.bevoelkerung VALUES (291, 2017, 9278, '      Straubing-Bogen, Landkreis', '99530', '50072', '49458');
INSERT INTO kriminal.bevoelkerung VALUES (292, 2017, 9279, '      Dingolfing-Landau, Landkreis', '95433', '49272', '46162');
INSERT INTO kriminal.bevoelkerung VALUES (293, 2017, 93, '    Oberpfalz, Regierungsbezirk', '1101393', '548237', '553156');
INSERT INTO kriminal.bevoelkerung VALUES (294, 2017, 9361, '      Amberg', '42298', '20855', '21443');
INSERT INTO kriminal.bevoelkerung VALUES (295, 2017, 9362, '      Regensburg', '149766', '72461', '77306');
INSERT INTO kriminal.bevoelkerung VALUES (296, 2017, 9363, '      Weiden i.d.OPf.', '42519', '20482', '22037');
INSERT INTO kriminal.bevoelkerung VALUES (297, 2017, 9371, '      Amberg-Sulzbach, Landkreis', '102923', '51496', '51427');
INSERT INTO kriminal.bevoelkerung VALUES (298, 2017, 9372, '      Cham, Landkreis', '127129', '63750', '63379');
INSERT INTO kriminal.bevoelkerung VALUES (299, 2017, 9373, '      Neumarkt i.d.OPf., Landkreis', '132153', '66782', '65372');
INSERT INTO kriminal.bevoelkerung VALUES (300, 2017, 9374, '      Neustadt a.d.Waldnaab, Landkreis', '94507', '46982', '47526');
INSERT INTO kriminal.bevoelkerung VALUES (301, 2017, 9375, '      Regensburg, Landkreis', '191341', '95809', '95532');
INSERT INTO kriminal.bevoelkerung VALUES (302, 2017, 9376, '      Schwandorf, Landkreis', '145935', '73354', '72581');
INSERT INTO kriminal.bevoelkerung VALUES (303, 2017, 9377, '      Tirschenreuth, Landkreis', '72824', '36270', '36554');
INSERT INTO kriminal.bevoelkerung VALUES (304, 2017, 94, '    Oberfranken, Regierungsbezirk', '1064617', '523268', '541350');
INSERT INTO kriminal.bevoelkerung VALUES (305, 2017, 9461, '      Bamberg', '76461', '36726', '39736');
INSERT INTO kriminal.bevoelkerung VALUES (306, 2017, 9462, '      Bayreuth', '73532', '35737', '37796');
INSERT INTO kriminal.bevoelkerung VALUES (307, 2017, 9463, '      Coburg', '41154', '19997', '21157');
INSERT INTO kriminal.bevoelkerung VALUES (308, 2017, 9464, '      Hof', '45567', '21843', '23724');
INSERT INTO kriminal.bevoelkerung VALUES (309, 2017, 9471, '      Bamberg, Landkreis', '146313', '73290', '73023');
INSERT INTO kriminal.bevoelkerung VALUES (310, 2017, 9472, '      Bayreuth, Landkreis', '103841', '51362', '52479');
INSERT INTO kriminal.bevoelkerung VALUES (311, 2017, 9473, '      Coburg, Landkreis', '86810', '42930', '43880');
INSERT INTO kriminal.bevoelkerung VALUES (312, 2017, 9474, '      Forchheim, Landkreis', '115470', '57252', '58219');
INSERT INTO kriminal.bevoelkerung VALUES (313, 2017, 9475, '      Hof, Landkreis', '95844', '46784', '49060');
INSERT INTO kriminal.bevoelkerung VALUES (314, 2017, 9476, '      Kronach, Landkreis', '67544', '33303', '34241');
INSERT INTO kriminal.bevoelkerung VALUES (315, 2017, 9477, '      Kulmbach, Landkreis', '72003', '35343', '36660');
INSERT INTO kriminal.bevoelkerung VALUES (316, 2017, 9478, '      Lichtenfels, Landkreis', '66709', '32937', '33772');
INSERT INTO kriminal.bevoelkerung VALUES (317, 2017, 9479, '      Wunsiedel i.Fichtelgebirge, Landkreis', '73373', '35767', '37606');
INSERT INTO kriminal.bevoelkerung VALUES (318, 2017, 95, '    Mittelfranken, Regierungsbezirk', '1754851', '863935', '890917');
INSERT INTO kriminal.bevoelkerung VALUES (319, 2017, 9561, '      Ansbach', '41592', '19908', '21685');
INSERT INTO kriminal.bevoelkerung VALUES (320, 2017, 9562, '      Erlangen', '110618', '54902', '55717');
INSERT INTO kriminal.bevoelkerung VALUES (321, 2017, 9563, '      F�rth', '125965', '61667', '64298');
INSERT INTO kriminal.bevoelkerung VALUES (322, 2017, 9564, '      N�rnberg', '513415', '249577', '263838');
INSERT INTO kriminal.bevoelkerung VALUES (323, 2017, 9565, '      Schwabach', '40744', '19853', '20891');
INSERT INTO kriminal.bevoelkerung VALUES (324, 2017, 9571, '      Ansbach, Landkreis', '182717', '91800', '90917');
INSERT INTO kriminal.bevoelkerung VALUES (325, 2017, 9572, '      Erlangen-H�chstadt, Landkreis', '134987', '67120', '67868');
INSERT INTO kriminal.bevoelkerung VALUES (326, 2017, 9573, '      F�rth, Landkreis', '116082', '56374', '59708');
INSERT INTO kriminal.bevoelkerung VALUES (327, 2017, 9574, '      N�rnberger Land, Landkreis', '169323', '83508', '85815');
INSERT INTO kriminal.bevoelkerung VALUES (328, 2017, 9575, '      Neustadt a.d.Aisch-Bad Windsheim, Landkreis', '99487', '49750', '49737');
INSERT INTO kriminal.bevoelkerung VALUES (329, 2017, 9576, '      Roth, Landkreis', '125832', '62571', '63261');
INSERT INTO kriminal.bevoelkerung VALUES (330, 2017, 9577, '      Wei�enburg-Gunzenhausen, Landkreis', '94091', '46907', '47184');
INSERT INTO kriminal.bevoelkerung VALUES (331, 2017, 96, '    Unterfranken, Regierungsbezirk', '1311292', '648899', '662394');
INSERT INTO kriminal.bevoelkerung VALUES (332, 2017, 9661, '      Aschaffenburg', '69558', '33675', '35883');
INSERT INTO kriminal.bevoelkerung VALUES (333, 2017, 9662, '      Schweinfurt', '53081', '25894', '27187');
INSERT INTO kriminal.bevoelkerung VALUES (334, 2017, 9663, '      W�rzburg', '126323', '60213', '66110');
INSERT INTO kriminal.bevoelkerung VALUES (335, 2017, 9671, '      Aschaffenburg, Landkreis', '173737', '86254', '87483');
INSERT INTO kriminal.bevoelkerung VALUES (336, 2017, 9672, '      Bad Kissingen, Landkreis', '103183', '50725', '52458');
INSERT INTO kriminal.bevoelkerung VALUES (337, 2017, 9673, '      Rh�n-Grabfeld, Landkreis', '79826', '39714', '40112');
INSERT INTO kriminal.bevoelkerung VALUES (338, 2017, 9674, '      Ha�berge, Landkreis', '84419', '42362', '42057');
INSERT INTO kriminal.bevoelkerung VALUES (339, 2017, 9675, '      Kitzingen, Landkreis', '90089', '45289', '44800');
INSERT INTO kriminal.bevoelkerung VALUES (340, 2017, 9676, '      Miltenberg, Landkreis', '128514', '64047', '64467');
INSERT INTO kriminal.bevoelkerung VALUES (341, 2017, 9677, '      Main-Spessart, Landkreis', '126412', '63022', '63391');
INSERT INTO kriminal.bevoelkerung VALUES (342, 2017, 9678, '      Schweinfurt, Landkreis', '114964', '57541', '57424');
INSERT INTO kriminal.bevoelkerung VALUES (343, 2017, 9679, '      W�rzburg, Landkreis', '161191', '80167', '81024');
INSERT INTO kriminal.bevoelkerung VALUES (344, 2017, 97, '    Schwaben, Regierungsbezirk', '1865680', '927504', '938176');
INSERT INTO kriminal.bevoelkerung VALUES (345, 2017, 9761, '      Augsburg', '291218', '142950', '148268');
INSERT INTO kriminal.bevoelkerung VALUES (346, 2017, 9762, '      Kaufbeuren', '43306', '21288', '22018');
INSERT INTO kriminal.bevoelkerung VALUES (347, 2017, 9763, '      Kempten (Allg�u)', '67930', '33613', '34317');
INSERT INTO kriminal.bevoelkerung VALUES (348, 2017, 9764, '      Memmingen', '43382', '21405', '21977');
INSERT INTO kriminal.bevoelkerung VALUES (349, 2017, 9771, '      Aichach-Friedberg, Landkreis', '131998', '65826', '66172');
INSERT INTO kriminal.bevoelkerung VALUES (350, 2017, 9772, '      Augsburg, Landkreis', '248689', '123430', '125259');
INSERT INTO kriminal.bevoelkerung VALUES (351, 2017, 9773, '      Dillingen a.d.Donau, Landkreis', '94858', '47462', '47396');
INSERT INTO kriminal.bevoelkerung VALUES (352, 2017, 9774, '      G�nzburg, Landkreis', '124009', '62576', '61433');
INSERT INTO kriminal.bevoelkerung VALUES (353, 2017, 9775, '      Neu-Ulm, Landkreis', '171779', '85182', '86597');
INSERT INTO kriminal.bevoelkerung VALUES (354, 2017, 9776, '      Lindau (Bodensee), Landkreis', '81055', '39917', '41138');
INSERT INTO kriminal.bevoelkerung VALUES (355, 2017, 9777, '      Ostallg�u, Landkreis', '138772', '69166', '69606');
INSERT INTO kriminal.bevoelkerung VALUES (356, 2017, 9778, '      Unterallg�u, Landkreis', '141855', '71343', '70512');
INSERT INTO kriminal.bevoelkerung VALUES (357, 2017, 9779, '      Donau-Ries, Landkreis', '132671', '67325', '65346');
INSERT INTO kriminal.bevoelkerung VALUES (358, 2017, 9780, '      Oberallg�u, Landkreis', '154164', '76025', '78139');
INSERT INTO kriminal.bevoelkerung VALUES (359, 2017, 10, '  Saarland', '995419', '488656', '506764');
INSERT INTO kriminal.bevoelkerung VALUES (360, 2017, 10041, '      Saarbr�cken, Regionalverband', '329872', '161952', '167920');
INSERT INTO kriminal.bevoelkerung VALUES (361, 2017, 10041100, '      Saarbr�cken, Landeshauptstadt', '180338', '89398', '90940');
INSERT INTO kriminal.bevoelkerung VALUES (362, 2017, 10042, '      Merzig-Wadern, Landkreis', '103782', '51312', '52470');
INSERT INTO kriminal.bevoelkerung VALUES (363, 2017, 10043, '      Neunkirchen, Landkreis', '133641', '65657', '67984');
INSERT INTO kriminal.bevoelkerung VALUES (364, 2017, 10044, '      Saarlouis, Landkreis', '196212', '96144', '100068');
INSERT INTO kriminal.bevoelkerung VALUES (365, 2017, 10045, '      Saarpfalz-Kreis', '143751', '69990', '73762');
INSERT INTO kriminal.bevoelkerung VALUES (366, 2017, 10046, '      St. Wendel, Landkreis', '88162', '43602', '44560');
INSERT INTO kriminal.bevoelkerung VALUES (367, 2017, 11000, '  Berlin', '3594163', '1765984', '1828179');
INSERT INTO kriminal.bevoelkerung VALUES (368, 2017, 11001001, '      Berlin-Mitte', '-', '-', '-');
INSERT INTO kriminal.bevoelkerung VALUES (369, 2017, 11002002, '      Berlin-Friedrichshain-Kreuzberg', '-', '-', '-');
INSERT INTO kriminal.bevoelkerung VALUES (370, 2017, 11003003, '      Berlin-Pankow', '-', '-', '-');
INSERT INTO kriminal.bevoelkerung VALUES (371, 2017, 11004004, '      Berlin-Charlottenburg-Wilmersdorf', '-', '-', '-');
INSERT INTO kriminal.bevoelkerung VALUES (372, 2017, 11005005, '      Berlin-Spandau', '-', '-', '-');
INSERT INTO kriminal.bevoelkerung VALUES (373, 2017, 11006006, '      Berlin-Steglitz-Zehlendorf', '-', '-', '-');
INSERT INTO kriminal.bevoelkerung VALUES (374, 2017, 11007007, '      Berlin-Tempelhof-Sch�neberg', '-', '-', '-');
INSERT INTO kriminal.bevoelkerung VALUES (375, 2017, 11008008, '      Berlin-Neuk�lln', '-', '-', '-');
INSERT INTO kriminal.bevoelkerung VALUES (376, 2017, 11009009, '      Berlin-Treptow-K�penick', '-', '-', '-');
INSERT INTO kriminal.bevoelkerung VALUES (377, 2017, 11010010, '      Berlin-Marzahn-Hellersdorf', '-', '-', '-');
INSERT INTO kriminal.bevoelkerung VALUES (378, 2017, 11011011, '      Berlin-Lichtenberg', '-', '-', '-');
INSERT INTO kriminal.bevoelkerung VALUES (379, 2017, 11012012, '      Berlin-Reinickendorf', '-', '-', '-');
INSERT INTO kriminal.bevoelkerung VALUES (380, 2017, 12, '  Brandenburg', '2499344', '1233827', '1265517');
INSERT INTO kriminal.bevoelkerung VALUES (381, 2017, 12051, '      Brandenburg an der Havel, Kreisfreie Stadt', '71775', '35451', '36324');
INSERT INTO kriminal.bevoelkerung VALUES (382, 2017, 12052, '      Cottbus, Kreisfreie Stadt', '100726', '49667', '51059');
INSERT INTO kriminal.bevoelkerung VALUES (383, 2017, 12053, '      Frankfurt (Oder), Kreisfreie Stadt', '58215', '28142', '30074');
INSERT INTO kriminal.bevoelkerung VALUES (384, 2017, 12054, '      Potsdam, Kreisfreie Stadt', '173760', '84053', '89707');
INSERT INTO kriminal.bevoelkerung VALUES (385, 2017, 12060, '      Barnim, Landkreis', '180115', '89321', '90794');
INSERT INTO kriminal.bevoelkerung VALUES (386, 2017, 12061, '      Dahme-Spreewald, Landkreis', '166697', '82709', '83988');
INSERT INTO kriminal.bevoelkerung VALUES (387, 2017, 12062, '      Elbe-Elster, Landkreis', '103926', '51530', '52397');
INSERT INTO kriminal.bevoelkerung VALUES (388, 2017, 12063, '      Havelland, Landkreis', '160198', '79258', '80940');
INSERT INTO kriminal.bevoelkerung VALUES (389, 2017, 12064, '      M�rkisch-Oderland, Landkreis', '192303', '95430', '96874');
INSERT INTO kriminal.bevoelkerung VALUES (390, 2017, 12065, '      Oberhavel, Landkreis', '209266', '103131', '106135');
INSERT INTO kriminal.bevoelkerung VALUES (391, 2017, 12066, '      Oberspreewald-Lausitz, Landkreis', '111542', '54825', '56718');
INSERT INTO kriminal.bevoelkerung VALUES (392, 2017, 12067, '      Oder-Spree, Landkreis', '178598', '87798', '90801');
INSERT INTO kriminal.bevoelkerung VALUES (393, 2017, 12068, '      Ostprignitz-Ruppin, Landkreis', '99391', '49511', '49880');
INSERT INTO kriminal.bevoelkerung VALUES (394, 2017, 12069, '      Potsdam-Mittelmark, Landkreis', '212711', '105468', '107243');
INSERT INTO kriminal.bevoelkerung VALUES (395, 2017, 12070, '      Prignitz, Landkreis', '77538', '38611', '38928');
INSERT INTO kriminal.bevoelkerung VALUES (396, 2017, 12071, '      Spree-Nei�e, Landkreis', '116141', '57220', '58921');
INSERT INTO kriminal.bevoelkerung VALUES (397, 2017, 12072, '      Teltow-Fl�ming, Landkreis', '165831', '82537', '83294');
INSERT INTO kriminal.bevoelkerung VALUES (398, 2017, 12073, '      Uckermark, Landkreis', '120614', '59170', '61444');
INSERT INTO kriminal.bevoelkerung VALUES (399, 2017, 13, '  Mecklenburg-Vorpommern', '1610897', '795170', '815727');
INSERT INTO kriminal.bevoelkerung VALUES (400, 2017, 13001, '      Kreisfreie Stadt Greifswald, Hansestadt', '-', '-', '-');
INSERT INTO kriminal.bevoelkerung VALUES (401, 2017, 13002, '      Kreisfreie Stadt Neubrandenburg, Stadt', '-', '-', '-');
INSERT INTO kriminal.bevoelkerung VALUES (402, 2017, 13003, '      Kreisfreie Stadt Rostock, Hansestadt', '207961', '102391', '105570');
INSERT INTO kriminal.bevoelkerung VALUES (403, 2017, 13004, '      Kreisfreie Stadt Schwerin, Landeshauptstadt', '95733', '46079', '49654');
INSERT INTO kriminal.bevoelkerung VALUES (404, 2017, 13005, '      Kreisfreie Stadt Stralsund, Hansestadt', '-', '-', '-');
INSERT INTO kriminal.bevoelkerung VALUES (405, 2017, 13006, '      Kreisfreie Stadt Wismar, Hansestadt', '-', '-', '-');
INSERT INTO kriminal.bevoelkerung VALUES (406, 2017, 13051, '      Landkreis Bad Doberan', '-', '-', '-');
INSERT INTO kriminal.bevoelkerung VALUES (407, 2017, 13052, '      Landkreis Demmin', '-', '-', '-');
INSERT INTO kriminal.bevoelkerung VALUES (408, 2017, 13053, '      Landkreis G�strow', '-', '-', '-');
INSERT INTO kriminal.bevoelkerung VALUES (409, 2017, 13054, '      Landkreis Ludwigslust', '-', '-', '-');
INSERT INTO kriminal.bevoelkerung VALUES (410, 2017, 13055, '      Landkreis Mecklenburg-Strelitz', '-', '-', '-');
INSERT INTO kriminal.bevoelkerung VALUES (411, 2017, 13056, '      Landkreis M�ritz', '-', '-', '-');
INSERT INTO kriminal.bevoelkerung VALUES (412, 2017, 13057, '      Landkreis Nordvorpommern', '-', '-', '-');
INSERT INTO kriminal.bevoelkerung VALUES (413, 2017, 13058, '      Landkreis Nordwestmecklenburg', '-', '-', '-');
INSERT INTO kriminal.bevoelkerung VALUES (414, 2017, 13059, '      Landkreis Ostvorpommern', '-', '-', '-');
INSERT INTO kriminal.bevoelkerung VALUES (415, 2017, 13060, '      Landkreis Parchim', '-', '-', '-');
INSERT INTO kriminal.bevoelkerung VALUES (416, 2017, 13061, '      Landkreis R�gen', '-', '-', '-');
INSERT INTO kriminal.bevoelkerung VALUES (417, 2017, 13062, '      Landkreis Uecker-Randow', '-', '-', '-');
INSERT INTO kriminal.bevoelkerung VALUES (418, 2017, 13071, '      Landkreis Mecklenburgische Seenplatte', '261195', '128386', '132809');
INSERT INTO kriminal.bevoelkerung VALUES (419, 2017, 13072, '      Landkreis Rostock', '214290', '106662', '107628');
INSERT INTO kriminal.bevoelkerung VALUES (420, 2017, 13073, '      Landkreis Vorpommern-R�gen', '225047', '110846', '114202');
INSERT INTO kriminal.bevoelkerung VALUES (421, 2017, 13074, '      Landkreis Nordwestmecklenburg', '156909', '78165', '78744');
INSERT INTO kriminal.bevoelkerung VALUES (422, 2017, 13075, '      Landkreis Vorpommern-Greifswald', '237220', '116561', '120659');
INSERT INTO kriminal.bevoelkerung VALUES (423, 2017, 13076, '      Landkreis Ludwigslust-Parchim', '212542', '106081', '106461');
INSERT INTO kriminal.bevoelkerung VALUES (424, 2017, 14, '  Sachsen', '4081546', '2010103', '2071443');
INSERT INTO kriminal.bevoelkerung VALUES (425, 2017, 141, '    Chemnitz, Regierungsbezirk', '-', '-', '-');
INSERT INTO kriminal.bevoelkerung VALUES (426, 2017, 14161, '      Chemnitz, Kreisfreie Stadt', '-', '-', '-');
INSERT INTO kriminal.bevoelkerung VALUES (427, 2017, 14166, '      Plauen, Kreisfreie Stadt', '-', '-', '-');
INSERT INTO kriminal.bevoelkerung VALUES (428, 2017, 14167, '      Zwickau, Kreisfreie Stadt', '-', '-', '-');
INSERT INTO kriminal.bevoelkerung VALUES (429, 2017, 14171, '      Annaberg, Landkreis', '-', '-', '-');
INSERT INTO kriminal.bevoelkerung VALUES (430, 2017, 14173, '      Chemnitzer Land, Landkreis', '-', '-', '-');
INSERT INTO kriminal.bevoelkerung VALUES (431, 2017, 14177, '      Freiberg, Landkreis', '-', '-', '-');
INSERT INTO kriminal.bevoelkerung VALUES (432, 2017, 14178, '      Vogtlandkreis', '-', '-', '-');
INSERT INTO kriminal.bevoelkerung VALUES (433, 2017, 14181, '      Mittlerer Erzgebirgskreis', '-', '-', '-');
INSERT INTO kriminal.bevoelkerung VALUES (434, 2017, 14182, '      Mittweida, Landkreis', '-', '-', '-');
INSERT INTO kriminal.bevoelkerung VALUES (435, 2017, 14188, '      Stollberg, Landkreis', '-', '-', '-');
INSERT INTO kriminal.bevoelkerung VALUES (436, 2017, 14191, '      Aue-Schwarzenberg, Landkreis', '-', '-', '-');
INSERT INTO kriminal.bevoelkerung VALUES (437, 2017, 14193, '      Zwickauer Land, Landkreis', '-', '-', '-');
INSERT INTO kriminal.bevoelkerung VALUES (438, 2017, 142, '    Dresden, Regierungsbezirk', '-', '-', '-');
INSERT INTO kriminal.bevoelkerung VALUES (439, 2017, 14262, '      Dresden, Kreisfreie Stadt', '-', '-', '-');
INSERT INTO kriminal.bevoelkerung VALUES (440, 2017, 14263, '      G�rlitz, Kreisfreie Stadt', '-', '-', '-');
INSERT INTO kriminal.bevoelkerung VALUES (441, 2017, 14264, '      Hoyerswerda, Kreisfreie Stadt', '-', '-', '-');
INSERT INTO kriminal.bevoelkerung VALUES (442, 2017, 14272, '      Bautzen, Landkreis', '-', '-', '-');
INSERT INTO kriminal.bevoelkerung VALUES (443, 2017, 14280, '      Mei�en, Landkreis', '-', '-', '-');
INSERT INTO kriminal.bevoelkerung VALUES (444, 2017, 14284, '      Niederschlesischer Oberlausitzkreis', '-', '-', '-');
INSERT INTO kriminal.bevoelkerung VALUES (445, 2017, 14285, '      Riesa-Gro�enhain, Landkreis', '-', '-', '-');
INSERT INTO kriminal.bevoelkerung VALUES (446, 2017, 14286, '      L�bau-Zittau, Landkreis', '-', '-', '-');
INSERT INTO kriminal.bevoelkerung VALUES (447, 2017, 14287, '      S�chsische Schweiz, Landkreis', '-', '-', '-');
INSERT INTO kriminal.bevoelkerung VALUES (449, 2017, 14292, '      Kamenz, Landkreis', '-', '-', '-');
INSERT INTO kriminal.bevoelkerung VALUES (450, 2017, 143, '    Leipzig, Regierungsbezirk', '-', '-', '-');
INSERT INTO kriminal.bevoelkerung VALUES (451, 2017, 14365, '      Leipzig, Kreisfreie Stadt', '-', '-', '-');
INSERT INTO kriminal.bevoelkerung VALUES (452, 2017, 14374, '      Delitzsch, Landkreis', '-', '-', '-');
INSERT INTO kriminal.bevoelkerung VALUES (453, 2017, 14375, '      D�beln, Landkreis', '-', '-', '-');
INSERT INTO kriminal.bevoelkerung VALUES (454, 2017, 14379, '      Leipziger Land, Landkreis', '-', '-', '-');
INSERT INTO kriminal.bevoelkerung VALUES (455, 2017, 14383, '      Muldentalkreis', '-', '-', '-');
INSERT INTO kriminal.bevoelkerung VALUES (456, 2017, 14389, '      Torgau-Oschatz, Landkreis', '-', '-', '-');
INSERT INTO kriminal.bevoelkerung VALUES (457, 2017, 145, '    Chemnitz, Stat. Region', '1449549', '711355', '738194');
INSERT INTO kriminal.bevoelkerung VALUES (458, 2017, 14511, '      Chemnitz, Stadt', '246604', '121525', '125079');
INSERT INTO kriminal.bevoelkerung VALUES (459, 2017, 14521, '      Erzgebirgskreis', '342255', '167497', '174758');
INSERT INTO kriminal.bevoelkerung VALUES (460, 2017, 14522, '      Mittelsachsen, Landkreis', '309329', '153686', '155644');
INSERT INTO kriminal.bevoelkerung VALUES (461, 2017, 14523, '      Vogtlandkreis', '230318', '112215', '118103');
INSERT INTO kriminal.bevoelkerung VALUES (462, 2017, 14524, '      Zwickau, Landkreis', '321044', '156433', '164611');
INSERT INTO kriminal.bevoelkerung VALUES (463, 2017, 146, '    Dresden, Stat. Region', '1599364', '790740', '808624');
INSERT INTO kriminal.bevoelkerung VALUES (464, 2017, 14612, '      Dresden, Stadt', '549122', '273456', '275667');
INSERT INTO kriminal.bevoelkerung VALUES (465, 2017, 14625, '      Bautzen, Landkreis', '303663', '149967', '153696');
INSERT INTO kriminal.bevoelkerung VALUES (466, 2017, 14626, '      G�rlitz, Landkreis', '257462', '126060', '131402');
INSERT INTO kriminal.bevoelkerung VALUES (467, 2017, 14627, '      Mei�en, Landkreis', '243376', '119982', '123394');
INSERT INTO kriminal.bevoelkerung VALUES (468, 2017, 14628, '      S�chsische Schweiz-Osterzgebirge, Landkreis', '245742', '121276', '124466');
INSERT INTO kriminal.bevoelkerung VALUES (469, 2017, 147, '    Leipzig, Stat. Region', '1032633', '508008', '524625');
INSERT INTO kriminal.bevoelkerung VALUES (470, 2017, 14713, '      Leipzig, Stadt', '576534', '283035', '293500');
INSERT INTO kriminal.bevoelkerung VALUES (471, 2017, 14729, '      Leipzig, Landkreis', '258171', '126616', '131555');
INSERT INTO kriminal.bevoelkerung VALUES (472, 2017, 14730, '      Nordsachsen, Landkreis', '197929', '98358', '99571');
INSERT INTO kriminal.bevoelkerung VALUES (473, 2017, 15, '  Sachsen-Anhalt', '2229667', '1098665', '1131002');
INSERT INTO kriminal.bevoelkerung VALUES (474, 2017, 15001, '      Dessau-Ro�lau, Kreisfreie Stadt', '82308', '40019', '42289');
INSERT INTO kriminal.bevoelkerung VALUES (475, 2017, 15002, '      Halle (Saale), Kreisfreie Stadt', '238589', '115485', '123105');
INSERT INTO kriminal.bevoelkerung VALUES (476, 2017, 15003, '      Magdeburg, Kreisfreie Stadt', '238307', '117602', '120705');
INSERT INTO kriminal.bevoelkerung VALUES (477, 2017, 15081, '      Altmarkkreis Salzwedel', '84847', '42366', '42481');
INSERT INTO kriminal.bevoelkerung VALUES (478, 2017, 15082, '      Anhalt-Bitterfeld, Landkreis', '162182', '79605', '82578');
INSERT INTO kriminal.bevoelkerung VALUES (479, 2017, 15083, '      B�rde, Landkreis', '172789', '86249', '86540');
INSERT INTO kriminal.bevoelkerung VALUES (480, 2017, 15084, '      Burgenlandkreis', '182571', '90687', '91884');
INSERT INTO kriminal.bevoelkerung VALUES (481, 2017, 15085, '      Harz, Landkreis', '217971', '107269', '110702');
INSERT INTO kriminal.bevoelkerung VALUES (482, 2017, 15086, '      Jerichower Land, Landkreis', '90905', '45341', '45564');
INSERT INTO kriminal.bevoelkerung VALUES (483, 2017, 15087, '      Mansfeld-S�dharz, Landkreis', '138897', '68413', '70485');
INSERT INTO kriminal.bevoelkerung VALUES (484, 2017, 15088, '      Saalekreis', '185723', '91995', '93728');
INSERT INTO kriminal.bevoelkerung VALUES (485, 2017, 15089, '      Salzlandkreis', '193657', '94707', '98951');
INSERT INTO kriminal.bevoelkerung VALUES (486, 2017, 15090, '      Stendal, Landkreis', '113790', '56297', '57493');
INSERT INTO kriminal.bevoelkerung VALUES (487, 2017, 15091, '      Wittenberg, Landkreis', '127132', '62633', '64499');
INSERT INTO kriminal.bevoelkerung VALUES (488, 2017, 151, '    Dessau, Stat. Region', '-', '-', '-');
INSERT INTO kriminal.bevoelkerung VALUES (489, 2017, 15101, '      Dessau, Kreisfreie Stadt', '-', '-', '-');
INSERT INTO kriminal.bevoelkerung VALUES (490, 2017, 15151, '      Anhalt-Zerbst, Kreis', '-', '-', '-');
INSERT INTO kriminal.bevoelkerung VALUES (491, 2017, 15153, '      Bernburg, Kreis', '-', '-', '-');
INSERT INTO kriminal.bevoelkerung VALUES (492, 2017, 15154, '      Bitterfeld, Kreis', '-', '-', '-');
INSERT INTO kriminal.bevoelkerung VALUES (493, 2017, 15159, '      K�then, Kreis', '-', '-', '-');
INSERT INTO kriminal.bevoelkerung VALUES (494, 2017, 15171, '      Wittenberg, Kreis', '-', '-', '-');
INSERT INTO kriminal.bevoelkerung VALUES (495, 2017, 152, '    Halle, Stat. Region', '-', '-', '-');
INSERT INTO kriminal.bevoelkerung VALUES (496, 2017, 15202, '      Halle (Saale), Kreisfreie Stadt', '-', '-', '-');
INSERT INTO kriminal.bevoelkerung VALUES (497, 2017, 15256, '      Burgenlandkreis', '-', '-', '-');
INSERT INTO kriminal.bevoelkerung VALUES (498, 2017, 15260, '      Mansfelder Land, Kreis', '-', '-', '-');
INSERT INTO kriminal.bevoelkerung VALUES (499, 2017, 15261, '      Merseburg-Querfurt, Kreis', '-', '-', '-');
INSERT INTO kriminal.bevoelkerung VALUES (500, 2017, 15265, '      Saalkreis', '-', '-', '-');
INSERT INTO kriminal.bevoelkerung VALUES (501, 2017, 15266, '      Sangerhausen, Kreis', '-', '-', '-');
INSERT INTO kriminal.bevoelkerung VALUES (502, 2017, 15268, '      Wei�enfels, Kreis', '-', '-', '-');
INSERT INTO kriminal.bevoelkerung VALUES (503, 2017, 153, '    Magdeburg, Stat. Region', '-', '-', '-');
INSERT INTO kriminal.bevoelkerung VALUES (504, 2017, 15303, '      Magdeburg, Kreisfreie Stadt', '-', '-', '-');
INSERT INTO kriminal.bevoelkerung VALUES (505, 2017, 15352, '      Aschersleben-Sta�furt, Kreis', '-', '-', '-');
INSERT INTO kriminal.bevoelkerung VALUES (506, 2017, 15355, '      B�rdekreis', '-', '-', '-');
INSERT INTO kriminal.bevoelkerung VALUES (507, 2017, 15357, '      Halberstadt, Kreis', '-', '-', '-');
INSERT INTO kriminal.bevoelkerung VALUES (508, 2017, 15358, '      Jerichower Land, Kreis', '-', '-', '-');
INSERT INTO kriminal.bevoelkerung VALUES (509, 2017, 15362, '      Ohrekreis', '-', '-', '-');
INSERT INTO kriminal.bevoelkerung VALUES (510, 2017, 15363, '      Stendal, Kreis', '-', '-', '-');
INSERT INTO kriminal.bevoelkerung VALUES (511, 2017, 15364, '      Quedlinburg, Kreis', '-', '-', '-');
INSERT INTO kriminal.bevoelkerung VALUES (512, 2017, 15367, '      Sch�nebeck, Kreis', '-', '-', '-');
INSERT INTO kriminal.bevoelkerung VALUES (513, 2017, 15369, '      Wernigerode, Kreis', '-', '-', '-');
INSERT INTO kriminal.bevoelkerung VALUES (514, 2017, 15370, '      Altmarkkreis Salzwedel, Kreis', '-', '-', '-');
INSERT INTO kriminal.bevoelkerung VALUES (515, 2017, 16, '  Th�ringen', '2154667', '1066441', '1088226');
INSERT INTO kriminal.bevoelkerung VALUES (516, 2017, 16051, '      Erfurt, krsfr. Stadt', '212051', '103130', '108921');
INSERT INTO kriminal.bevoelkerung VALUES (517, 2017, 16052, '      Gera, krsfr. Stadt', '94805', '45824', '48981');
INSERT INTO kriminal.bevoelkerung VALUES (518, 2017, 16053, '      Jena, krsfr. Stadt', '110710', '55220', '55490');
INSERT INTO kriminal.bevoelkerung VALUES (519, 2017, 16054, '      Suhl, krsfr. Stadt', '35387', '17234', '18153');
INSERT INTO kriminal.bevoelkerung VALUES (520, 2017, 16055, '      Weimar, krsfr. Stadt', '64391', '31192', '33199');
INSERT INTO kriminal.bevoelkerung VALUES (521, 2017, 16056, '      Eisenach, krsfr. Stadt', '42649', '21015', '21635');
INSERT INTO kriminal.bevoelkerung VALUES (522, 2017, 16061, '      Eichsfeld, Kreis', '100839', '50677', '50162');
INSERT INTO kriminal.bevoelkerung VALUES (523, 2017, 16062, '      Nordhausen, Kreis', '84898', '42063', '42835');
INSERT INTO kriminal.bevoelkerung VALUES (524, 2017, 16063, '      Wartburgkreis', '124247', '62151', '62096');
INSERT INTO kriminal.bevoelkerung VALUES (525, 2017, 16064, '      Unstrut-Hainich-Kreis', '103726', '51462', '52265');
INSERT INTO kriminal.bevoelkerung VALUES (526, 2017, 16065, '      Kyffh�userkreis', '76252', '37932', '38320');
INSERT INTO kriminal.bevoelkerung VALUES (527, 2017, 16066, '      Schmalkalden-Meiningen, Kreis', '123229', '61589', '61640');
INSERT INTO kriminal.bevoelkerung VALUES (528, 2017, 16067, '      Gotha, Kreis', '135476', '67455', '68021');
INSERT INTO kriminal.bevoelkerung VALUES (529, 2017, 16068, '      S�mmerda, Kreis', '70073', '35029', '35044');
INSERT INTO kriminal.bevoelkerung VALUES (530, 2017, 16069, '      Hildburghausen, Kreis', '64127', '32091', '32036');
INSERT INTO kriminal.bevoelkerung VALUES (531, 2017, 16070, '      Ilm-Kreis', '108999', '54744', '54255');
INSERT INTO kriminal.bevoelkerung VALUES (532, 2017, 16071, '      Weimarer Land, Kreis', '82224', '40819', '41405');
INSERT INTO kriminal.bevoelkerung VALUES (533, 2017, 16072, '      Sonneberg, Kreis', '56434', '27742', '28693');
INSERT INTO kriminal.bevoelkerung VALUES (534, 2017, 16073, '      Saalfeld-Rudolstadt, Kreis', '107842', '53082', '54760');
INSERT INTO kriminal.bevoelkerung VALUES (535, 2017, 16074, '      Saale-Holzland-Kreis', '83758', '41765', '41993');
INSERT INTO kriminal.bevoelkerung VALUES (536, 2017, 16075, '      Saale-Orla-Kreis', '81932', '40570', '41362');
INSERT INTO kriminal.bevoelkerung VALUES (537, 2017, 16076, '      Greiz, Kreis', '99496', '49004', '50492');
INSERT INTO kriminal.bevoelkerung VALUES (538, 2017, 16077, '      Altenburger Land, Kreis', '91129', '44657', '46472');


--
-- TOC entry 4271 (class 0 OID 0)
-- Dependencies: 218
-- Name: bevoelkerung_id_seq; Type: SEQUENCE SET; Schema: kriminal; Owner: postgres
--

SELECT pg_catalog.setval('kriminal.bevoelkerung_id_seq', 538, true);


--
-- TOC entry 4133 (class 2606 OID 32782)
-- Name: bevoelkerung bevoelkerung_pkey; Type: CONSTRAINT; Schema: kriminal; Owner: postgres
--

ALTER TABLE ONLY kriminal.bevoelkerung
    ADD CONSTRAINT bevoelkerung_pkey PRIMARY KEY (id);


-- Completed on 2019-08-10 14:05:23

--
-- PostgreSQL database dump complete
--
Alter table kriminal."vg2500 vg2500_krs" 
ADD column "BevoelkerungInsgesamt" integer, 
ADD column "m�nnlich" integer, 
ADD column "weiblich" integer; 

Update kriminal."vg2500 vg2500_krs" as vg
Set "BevoelkerungInsgesamt" = cast(b."BevoelkerungInsgesamt" as INTEGER), "m�nnlich" = cast(b."m�nnlich" as INTEGER), "weiblich" = cast(b."weiblich" as INTEGER) from kriminal."bevoelkerung" as b where b."GS" = cast(vg."rs" AS INTEGER);


