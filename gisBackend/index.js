const express = require("express");
const cors = require("cors");
const { Pool } = require("pg");
const bodyParser = require("body-parser");
const StringUtils = require("./utils/stringUtils");

const dropPreviousStatistik = 'Drop table if exists kriminal."BKA-LKS-%s1"';
const createNewStatistik = 'Create table kriminal."BKA-LKS-%s1" as Select * from kriminal."BKA-LKS"';
const insertNewStatistik = 'Insert into kriminal."BKA-LKS-%s1" VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19)';
const dropPreviousGeoLayer = "Drop table if exists kriminal.vg_bka_%s1";
const createGeoLayer =
  'Create Table kriminal.vg_bka_%s1 as Select * from kriminal."BKA-LKS-%s1" as s, kriminal."vg2500 vg2500_krs" as kreis where s."Gemeindeschluessel" = CAST(kreis.rs as Integer)';

const app = express();

const dbuser = 'postgres'
const dbpass = 'admin';

const pool = new Pool({
  user: dbuser,
  host: "localhost",
  database: "kstatistiken",
  password: dbpass,
  port: 5432
});

app.use(cors());
app.use(bodyParser.json({ limit: "10mb" }));
app.use(bodyParser.urlencoded({ extended: true, limit: "10mb" }));

app.listen(8000, () => console.log("Example app listening on port 3000!"));

app.get("/", (req, res) => {
  res.send("Hello World!");
});

app.post("/upload/:year", async (req, res) => {

  if (!req.headers.authorization) {
    return res.status(403).send("Missing Credentials");
  }

  const auth = req.headers.authorization.split(' ')[1];

  if (!auth) {
    return res.status(403).send("Credentials Error");
  }

  let buff = new Buffer(auth, 'base64');
  let text = buff.toString('ascii');
  const authorizationData = text.split(':');

  if (authorizationData.length !== 2 || authorizationData[0] !== dbuser || authorizationData[1] !== dbpass) {
      return res.status(403).send("Invalid Login");
  }

  const year = Number.parseInt(req.params.year, 10);

  if (!year) {
    return res.status(400).send("Invalid year");
  }

  const client = await pool.connect();
  try {
    await client.query("BEGIN");
    await client.query(dropPreviousStatistik.replace("%s1", year.toString()));
    await client.query(createNewStatistik.replace("%s1", year.toString()));
    const data = req.body.data;

    data.forEach(async (element, index) => {
      if (element.length >= 18) {
        const values = [
          Number.parseInt(index.toString(), 10),
          element[0],
          element[1],
          Number.parseInt(element[2], 10),
          element[3],
          element[4],
          element[5],
          element[6],
          element[7],
          Number.parseFloat(element[8]),
          Number.parseInt(element[9], 10),
          Number.parseInt(element[10], 10),
          element[11],
          Number.parseFloat(element[12]),
          element[13],
          element[14],
          element[15],
          element[16],
          Number.parseFloat(element[17])
        ];

        if (element && element.toString() && element.toString() !== "") {
          await client.query(insertNewStatistik.replace("%s1", year.toString()), values);
        }
      }
    });
    await client.query(StringUtils.replaceAll(dropPreviousGeoLayer, "%s1", year.toString()));
    await client.query(StringUtils.replaceAll(createGeoLayer, "%s1", year.toString()));
    await client.query("COMMIT");
  } catch (e) {
    await client.query("ROLLBACK");
    console.log("Wrong");
    return res.status(400).send("Something went wrong, contact Admin");
  } finally {
    client.release();
  }
  res.status(200).send("Completed");
});
